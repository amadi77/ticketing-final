package com.rajaee.ticketing.config;

import com.rajaee.ticketing.utility.model.object.ErrorResult;
import com.rajaee.ticketing.utility.model.object.SystemError;
import com.rajaee.ticketing.utility.model.object.SystemException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(SystemException.class)
    public List<ErrorResult> handleSystemException(SystemException exception, HttpServletResponse response) {
        response.setStatus(exception.getError().getValue());
        if (exception.getErrorResults() != null) {
            return exception.getErrorResults();
        }
        return Collections.singletonList(new ErrorResult(exception));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public List<ErrorResult> constraintViolationException(ConstraintViolationException exception, HttpServletResponse response) {
        List<ErrorResult> validationList = new ArrayList<>();

        for (ConstraintViolation violation : exception.getConstraintViolations()) {
            String argument = "";
            if (violation.getPropertyPath() != null) {
                for (Path.Node node : violation.getPropertyPath()) argument = node.getName();
                argument += " ";
            }
            argument += violation.getMessage();
            validationList.add(new ErrorResult(SystemError.VALIDATION_EXCEPTION, 5050, argument));
        }

        response.setStatus(SystemError.VALIDATION_EXCEPTION.getValue());
        return validationList;
    }

    @ExceptionHandler(Exception.class)
    public ErrorResult handleUnHandledException(Exception exception, HttpServletResponse response) {
        exception.printStackTrace();
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        return new ErrorResult(SystemError.SERVER_ERROR, 6060, exception.getMessage());
    }

//    @Override
//    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
//                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
//        List<String> validationList = ex.getBindingResult().getFieldErrors().stream().map(fieldError->fieldError.getField()).collect(Collectors.toList());
//        String fields = String.join(",", validationList);
//        ErrorResult errorResult = new ErrorResult(SystemError.VALIDATION_EXCEPTION, 15312, fields);
//        return new ResponseEntity<>(errorResult, status);
//    }

}