package com.rajaee.ticketing.config;

import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
@EnableWebMvc
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig implements WebMvcConfigurer{


        @Bean
        public Docket adminApi() {

            return new Docket(DocumentationType.SWAGGER_2)
                    .groupName("Admin")
                    .select()
                    .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                    .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                    .paths(PathSelectors.ant("/admin/**"))
                    .build()
                    .useDefaultResponseMessages(false)
                    .apiInfo(apiInfo())
                    .securityContexts(Lists.newArrayList(securityContext()))
                    .securitySchemes(Collections.singletonList(apiKey()));
        }

        @Bean
        public Docket pubApi() {

            return new Docket(DocumentationType.SWAGGER_2)
                    .groupName("Public")
                    .select()
                    .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                    .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                    .paths(PathSelectors.ant("/pub/**"))
                    .build()
                    .useDefaultResponseMessages(false)
                    .apiInfo(apiInfo())
                    .securityContexts(Lists.newArrayList(securityContext()))
                    .securitySchemes(Collections.singletonList(apiKey()));
        }

        @Bean
        public Docket identifiedApi() {

            return new Docket(DocumentationType.SWAGGER_2)
                    .groupName("Identified")
                    .select()
                    .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                    .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                    .paths(PathSelectors.ant("/idn/**"))
                    .build()
                    .useDefaultResponseMessages(false)
                    .apiInfo(apiInfo())
                    .securityContexts(Lists.newArrayList(securityContext()))
                    .securitySchemes(Collections.singletonList(apiKey()));
        }

        @Bean
        public Docket allApi() {

            return new Docket(DocumentationType.SWAGGER_2)
                    .groupName(" All Apis")
                    .select()
                    .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                    .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                    .paths(PathSelectors.any())
                    .build()
                    .useDefaultResponseMessages(false)
                    .apiInfo(apiInfo())
                    .securityContexts(Lists.newArrayList(securityContext()))
                    .securitySchemes(Collections.singletonList(apiKey()));
        }
//
//    @Bean
//    public SecurityConfiguration securityInfo() {
//        return new SecurityConfiguration(null, null, null, null, "", ApiKeyVehicle.HEADER, "Authorization", "");
//    }

        private ApiInfo apiInfo() {
            return new ApiInfo(
                    "Ticketing API",
                    "This is ticketing webservice API",
                    "API TOS",
                    "Terms of service",
                    new Contact("Ahmad Reza Arab Mokhtari", "", "ahmadmokhtari7744@gmail.com"),
                    "License of API", "API license URL", Collections.emptyList());
        }

        @Override
        public void addViewControllers(ViewControllerRegistry registry) {
            registry.addRedirectViewController("/doc", "/swagger-ui.html");
        }

        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.addResourceHandler("/swagger-ui.html**").addResourceLocations("classpath:/META-INF/resources/swagger-ui.html");
            registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
        }

        private ApiKey apiKey() {
            return new ApiKey("JWT", "Authorization", "header");
        }

        private SecurityContext securityContext() {
            return SecurityContext.builder()
                    .securityReferences(defaultAuth())
                    .forPaths(PathSelectors.regex("/((sec)|(idn))/.*"))
                    .build();
        }

        List<SecurityReference> defaultAuth() {
            AuthorizationScope authorizationScope
                    = new AuthorizationScope("global", "accessEverything");
            AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
            authorizationScopes[0] = authorizationScope;
            return Lists.newArrayList(
                    new SecurityReference("JWT", authorizationScopes));
        }

    }
