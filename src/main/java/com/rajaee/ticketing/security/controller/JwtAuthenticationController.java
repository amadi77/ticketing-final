package com.rajaee.ticketing.security.controller;

import com.rajaee.ticketing.databse.security.entity.UserEntity;
import com.rajaee.ticketing.security.auterization.JwtUser;
import com.rajaee.ticketing.security.model.ChangePasswordIn;
import com.rajaee.ticketing.security.model.LoginIn;
import com.rajaee.ticketing.security.model.LoginOut;
import com.rajaee.ticketing.security.model.Register;
import com.rajaee.ticketing.security.repository.service.AccountService;
import com.rajaee.ticketing.security.statics.SecurityConstant;
import com.rajaee.ticketing.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@Api(tags = "Account")
@RequestMapping
@Validated
public class JwtAuthenticationController {
    private final AccountService accountService;

    @Autowired
    public JwtAuthenticationController(AccountService accountService) {
        this.accountService = accountService;
    }

    @ApiOperation(value = "register user and student", response = LoginOut.class)
    @PostMapping(value = RestApi.REST_PUBLIC + SecurityConstant.REGISTER, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> register(@RequestBody Register register) throws Exception {
        return new ResponseEntity<>(accountService.register(register), HttpStatus.ACCEPTED);
    }

    @ApiOperation(value = "login user", response = LoginOut.class)
    @PostMapping(value = RestApi.REST_PUBLIC + SecurityConstant.LOGIN, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> login(@RequestBody LoginIn loginIn) throws Exception {
        return new ResponseEntity<>(accountService.login(loginIn), HttpStatus.ACCEPTED);
    }

    @ApiOperation(value = "change password", response = LoginOut.class)
    @PostMapping(value = RestApi.REST_PUBLIC + SecurityConstant.PASSWORD_CHANGE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> login(@Valid @RequestBody ChangePasswordIn password, BindingResult bindingResult, HttpServletRequest servletRequest) throws Exception {
        UserEntity user = JwtUser.getUser();
        return new ResponseEntity<>(accountService.changePassword(password, user.getUsername()), HttpStatus.ACCEPTED);
    }

    @ApiOperation(value = "change password", response = LoginOut.class)
    @GetMapping(value = RestApi.REST_PUBLIC + "/test", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> test(HttpServletRequest servletRequest) throws Exception {
        return new ResponseEntity<>("test is ok", HttpStatus.ACCEPTED);
    }

}