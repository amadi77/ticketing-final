package com.rajaee.ticketing.security.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginOut {
    private String token;
    private Integer id;
    private String fullName;
}
