package com.rajaee.ticketing.security.model;

import com.rajaee.ticketing.utility.model.object.IValidation;
import com.rajaee.ticketing.utility.model.object.SystemError;
import com.rajaee.ticketing.utility.model.object.SystemException;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ChangePasswordIn implements IValidation {
    private String oldPassword;
    private String newPassword;
    private String confirmPassword;

    @Override
    public void validate() throws SystemException {
        if (newPassword.length()<6 )
            throw new SystemException(SystemError.VALIDATION_EXCEPTION,"password length must bigger than 6",1401);
        if(!newPassword.equals(confirmPassword))
            throw new SystemException(SystemError.VALIDATION_EXCEPTION,"password not match!",1402);
    }
}
