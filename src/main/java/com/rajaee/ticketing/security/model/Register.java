package com.rajaee.ticketing.security.model;

import com.rajaee.ticketing.core.pub.model.student.StudentIn;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Register {
    private StudentIn studentIn;
    private UserIn user;
    private boolean student;
}
