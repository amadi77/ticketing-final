package com.rajaee.ticketing.security.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginIn {
    private String username;
    private String password;
}
