package com.rajaee.ticketing.security.model;

import com.rajaee.ticketing.utility.model.dto.FilterBase;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserFilter extends FilterBase {
    private String firstName;
    private String lastName;
    private String fullName;
    private String phoneNumber;
    private String email;
    private String nationalCode;
    private String username;
    private Boolean active;
    private Integer id;
}
