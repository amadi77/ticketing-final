package com.rajaee.ticketing.security.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserIn {
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private String nationalCode;
}
