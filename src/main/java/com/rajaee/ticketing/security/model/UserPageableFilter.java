package com.rajaee.ticketing.security.model;

import com.rajaee.ticketing.utility.model.object.SortOption;
import com.rajaee.ticketing.utility.statics.enums.SortType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class UserPageableFilter extends UserFilter{
    @Min(1)
    private Integer size;
    @Min(1)
    private Integer page;
    private List<SortOption> sorts;

    public UserPageableFilter() {
        size = 10;
        page = 1;
        sorts = new ArrayList<>();
        sorts.add(new SortOption("id", SortType.ASCENDING));
    }

}
