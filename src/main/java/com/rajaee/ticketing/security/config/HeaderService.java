package com.rajaee.ticketing.security.config;

import javax.servlet.http.HttpServletRequest;

public class HeaderService {

    public static String extractTokenFromHeader(HttpServletRequest servletRequest) {
        String token = servletRequest.getHeader("Authentication");
        if (token.length() < 7 || !token.startsWith("Bearer ")) {
            return null;
        }
        return token;
    }
}
