package com.rajaee.ticketing.security.auterization;

import com.rajaee.ticketing.databse.security.entity.UserEntity;
import com.rajaee.ticketing.security.model.UserPrincipal;
import com.rajaee.ticketing.utility.model.object.SystemError;
import com.rajaee.ticketing.utility.model.object.SystemException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class JwtUser {
    public static UserEntity getUser() throws SystemException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        if (principal.getUserEntity() != null) {
            return principal.getUserEntity();
        }
        throw new SystemException(SystemError.USER_NOT_FOUND, "authentication", 2233);
    }
}
