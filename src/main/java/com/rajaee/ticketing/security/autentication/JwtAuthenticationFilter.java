package com.rajaee.ticketing.security.autentication;

import com.rajaee.ticketing.databse.security.entity.UserEntity;
import com.rajaee.ticketing.security.auterization.JwtTokenUtil;
import com.rajaee.ticketing.security.config.HeaderService;
import com.rajaee.ticketing.security.model.JwtAuthentication;
import com.rajaee.ticketing.security.statics.SecurityConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    @Value("jwt.header")
    private static String HEADER_TOKEN;
    @Value("jwt.token.prefix")
    private static String TOKEN_PREFIX;

    private AuthenticationManager authenticationManager;
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    protected JwtAuthenticationFilter(JwtRequestMatcher jwtRequestMatcher) {
        super(jwtRequestMatcher);
    }

    @Autowired
    public void init(AuthenticationManager authenticationManager) {
        super.setAuthenticationManager(authenticationManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        JwtAuthentication possibleToken = new JwtAuthentication(HeaderService.extractTokenFromHeader(request));
//            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
//                    loginIn.getUsername(),
//                    loginIn.getPassword(),
//                    new ArrayList<>()
//            );
        Authentication auth = authenticationManager.authenticate(possibleToken);
        return auth;

    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        request.setAttribute(SecurityConstant.REQUEST_EXTENDED_ATTRIBUTE, authResult.getPrincipal());
        SecurityContextHolder.getContext().setAuthentication(authResult);
        response.addHeader(HEADER_TOKEN, jwtTokenUtil.generateToken((UserEntity) authResult.getPrincipal()));
        chain.doFilter(request, response);
    }
}
