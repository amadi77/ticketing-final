package com.rajaee.ticketing.security.autentication;

import com.rajaee.ticketing.utility.statics.constants.RestApi;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class JwtRequestMatcher implements RequestMatcher {

    @Override
    public boolean matches(HttpServletRequest request) {
        RequestMatcher adminMatcher = new AntPathRequestMatcher(RestApi.MATCHER_ADMIN);
        RequestMatcher sellerMatcher = new AntPathRequestMatcher(RestApi.MATCHER_SELLER);
        RequestMatcher identifiedMatcher = new AntPathRequestMatcher(RestApi.MATCHER_IDENTIFIED);
        return identifiedMatcher.matches(request) || adminMatcher.matches(request) || sellerMatcher.matches(request);
    }
}
