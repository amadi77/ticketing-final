package com.rajaee.ticketing.security.repository.service;

import com.rajaee.ticketing.databse.security.entity.UserEntity;
import com.rajaee.ticketing.security.model.UserPrincipal;
import com.rajaee.ticketing.security.repository.dao.JwtUserDetailDao;
import com.rajaee.ticketing.utility.model.dto.FilterBase;
import com.rajaee.ticketing.utility.model.object.Pair;
import com.rajaee.ticketing.utility.model.object.SystemError;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.model.object.report.ReportCondition;
import com.rajaee.ticketing.utility.model.object.report.ReportOption;
import com.rajaee.ticketing.utility.repository.dao.base.GenericDao;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService extends GenericDao implements UserDetailsService {
    private final JwtUserDetailDao userDetailDao;
    private final ModelMapper modelMapper;

    public JwtUserDetailsService(JwtUserDetailDao userDetailDao, ModelMapper modelMapper) {
        this.userDetailDao = userDetailDao;
        this.modelMapper = modelMapper;
    }

    public UserEntity getEntityById(int id, String[] include) {
        return getEntityById(id, include);
    }

    public UserDetails loadUserByUsername(String username) {
        UserEntity userEntity = userDetailDao.getUserByUserName(username, null);
        return new UserPrincipal(userEntity);
    }

    public UserEntity getUserByUsername(String username, String[] include) throws SystemException {
        UserEntity user = userDetailDao.getUserByUserName(username, include);
        if (user == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, "username : " + username, 1501);
        else
            return user;
    }

    public UserEntity getDuplicateUser(String username, String email, String nationalCode) {
        return userDetailDao.getUserByUniqFiled(username, email, nationalCode);
    }


    @Override
    public Pair<ReportCondition, ReportOption> filterChain(FilterBase filterBase) {
        return null;
    }

}
