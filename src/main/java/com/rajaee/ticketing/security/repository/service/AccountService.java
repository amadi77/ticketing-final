package com.rajaee.ticketing.security.repository.service;

import com.rajaee.ticketing.core.pub.repository.service.PubStudentService;
import com.rajaee.ticketing.databse.security.entity.UserEntity;
import com.rajaee.ticketing.security.auterization.JwtTokenUtil;
import com.rajaee.ticketing.security.model.*;
import com.rajaee.ticketing.utility.bl.HashService;
import com.rajaee.ticketing.utility.model.dto.FilterBase;
import com.rajaee.ticketing.utility.model.object.Pair;
import com.rajaee.ticketing.utility.model.object.SystemError;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.model.object.report.ReportCondition;
import com.rajaee.ticketing.utility.model.object.report.ReportOption;
import com.rajaee.ticketing.utility.repository.dao.base.GenericDao;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService extends GenericDao {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final JwtUserDetailsService userDetailsService;
    private final PubStudentService studentService;
    private final ModelMapper modelMapper;
    private final HashService hashService;

    @Autowired
    public AccountService(AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil, JwtUserDetailsService userDetailsService, PubStudentService studentService, ModelMapper modelMapper, HashService hashService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userDetailsService = userDetailsService;
        this.studentService = studentService;
        this.modelMapper = modelMapper;
        this.hashService = hashService;
    }

    public UserEntity getEntityById(int id, String[] include) throws SystemException {
        return getEntityById(UserEntity.class, id, include);
    }

    public List<UserEntity> getAllEntities(UserPageableFilter filter, String[] include) {
        return getAllEntities(UserEntity.class, filter, include);
    }

    public LoginOut register(Register register) throws SystemException {
        UserEntity duplicateUser = userDetailsService.
                getDuplicateUser(register.getUser().getUsername(),
                        register.getUser().getEmail(),
                        register.getUser().getNationalCode());
        if (duplicateUser != null) {
            throw new SystemException(SystemError.USERNAME_ALREADY_EXIST, register.getUser().toString(), 1231);
        }
        if (register.isStudent() && studentService.studentExist(register.getStudentIn())) {
            throw new SystemException(SystemError.USERNAME_ALREADY_EXIST, register.getStudentIn().toString(), 1232);
        }
        UserEntity user = modelMapper.map(register.getUser(), UserEntity.class);
        String hashedPassword = hashService.hash(register.getUser().getPassword());
        user.setHashedPassword(hashedPassword);
        createEntity(user);
        if (register.isStudent())
            studentService.create(register.getStudentIn(), user.getId());
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getHashedPassword()));
        return createLoginOut(user);
    }

    public LoginOut login(LoginIn in) throws SystemException {
        UserPrincipal userPrincipal = (UserPrincipal) userDetailsService.loadUserByUsername(in.getUsername());
        if (userPrincipal.getUserEntity() == null) {
            throw new SystemException(SystemError.USER_NOT_FOUND, "username: " + in.getUsername(), 1233);
        }
        String hashPassword = hashService.hash(in.getPassword());
        if (!hashPassword.equals(userPrincipal.getUserEntity().getHashedPassword()))
            throw new SystemException(SystemError.USERNAME_PASSWORD_NOT_MATCH, "username: " + in.getUsername() + " password: " + in.getPassword(), 1234);
        return createLoginOut(userPrincipal.getUserEntity());
    }

    private LoginOut createLoginOut(UserEntity user) {
        String token = jwtTokenUtil.generateToken(user);
        LoginOut loginOut = new LoginOut();
        loginOut.setFullName(user.getFullName());
        loginOut.setId(user.getId());
        loginOut.setToken(token);
        return loginOut;
    }

    public boolean changePassword(ChangePasswordIn password, String username) throws SystemException {
        UserEntity userEntity = userDetailsService.getUserByUsername(username, null);
        String oldHashedPassword = hashService.hash(password.getOldPassword());
        if (oldHashedPassword.equals(userEntity.getHashedPassword()))
            throw new SystemException(SystemError.USERNAME_PASSWORD_NOT_MATCH, "user : " + username, 1235);
        String newHashedPassword = hashService.hash(password.getNewPassword());
        userEntity.setHashedPassword(newHashedPassword);
        updateEntity(userEntity);
        return true;
    }

    @Override
    public Pair<ReportCondition, ReportOption> filterChain(FilterBase filterBase) {
        ReportOption reportOption = new ReportOption();

        if (filterBase instanceof UserPageableFilter) {
            UserPageableFilter pageableFilter = (UserPageableFilter) filterBase;
            reportOption.setPageSize(pageableFilter.getSize());
            reportOption.setPageNumber(pageableFilter.getPage());
            reportOption.setSortOptions(pageableFilter.getSorts());
        }

        UserFilter userFilter = (UserFilter) filterBase;
        ReportCondition condition = new ReportCondition();
        condition.addEqualCondition("id", userFilter.getId());
        condition.addLikeCondition("emil", userFilter.getEmail());
        condition.addEqualCondition("active", userFilter.getActive());
        condition.addLikeCondition("lastName", userFilter.getLastName());
        condition.addLikeCondition("fullName", userFilter.getFullName());
        condition.addLikeCondition("lastName", userFilter.getFirstName());
        condition.addLikeCondition("firstName", userFilter.getFirstName());
        condition.addLikeCondition("phoneNumber", userFilter.getPhoneNumber());
        condition.addLikeCondition("nationalCode", userFilter.getNationalCode());
        return new Pair<>(condition, reportOption);
    }
}
