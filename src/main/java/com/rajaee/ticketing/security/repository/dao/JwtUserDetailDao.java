package com.rajaee.ticketing.security.repository.dao;

import com.rajaee.ticketing.databse.security.entity.UserEntity;
import com.rajaee.ticketing.utility.repository.dao.base.BaseDaoInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class JwtUserDetailDao {
    private final BaseDaoInterface baseDaoInterface;

    @Autowired
    public JwtUserDetailDao(BaseDaoInterface baseDaoInterface) {
        this.baseDaoInterface = baseDaoInterface;
    }

    public UserEntity getUserByUserName(String username, String[] include) {
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("username", username);
        return baseDaoInterface.getByAndConditions(UserEntity.class, objectMap, include);
    }

    public UserEntity getUserByUniqFiled(String username,String email,String nationalCode){
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("username", username);
        objectMap.put("email", email);
        objectMap.put("nationalCode", nationalCode);
        return baseDaoInterface.getByAndConditions(UserEntity.class, objectMap);
    }
}
