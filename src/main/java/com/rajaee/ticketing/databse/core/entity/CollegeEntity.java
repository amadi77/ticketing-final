package com.rajaee.ticketing.databse.core.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "COLLEGE",schema = "rajaee")
@Setter
@Getter
public class CollegeEntity {
    @Id
    @Column(name = "ID_PK" , nullable = false , scale = 0 , precision = 10)
    @GeneratedValue(strategy = GenerationType.SEQUENCE , generator = "College_Generator")
    @SequenceGenerator(name = "College_Generator"  , allocationSize = 1 , sequenceName = "COLLEGE_GENERATOR_SEQ")
    private int id;
    @Basic
    @Column(name = "COLLEGE_NAME",nullable = false , length = 80)
    private String collegeName;
    @Basic
    @Column(name = "COLLEGE_ENGLISH_NAME",nullable = false , length = 80)
    private String collegeEngName;
    @OneToMany(fetch = FetchType.LAZY , mappedBy = "college" ,cascade = CascadeType.ALL)
    private List<FieldEntity> fields;
}
