package com.rajaee.ticketing.databse.core.entity;

import com.rajaee.ticketing.databse.core.statics.Gender;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "DORM", schema = "rajaee")
@Setter
@Getter
public class DormEntity {
    @Id
    @Column(name = "ID_PK")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Dorm_Generator")
    @SequenceGenerator(name = "Dorm_Generator", allocationSize = 1, sequenceName = "DORM_SEQ")
    private int id;
    @Basic
    @Column(name = "NAME", nullable = false, length = 100)
    private String name;
    @Enumerated(EnumType.STRING)
    @Basic
    @Column(name = "GENDER", nullable = false, length = 10)
    private Gender gender;

}
