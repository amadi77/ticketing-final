package com.rajaee.ticketing.databse.core.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "CANDIDATE", schema = "rajaee")
@Setter
@Getter
public class CandidateEntity {
    @Id
    @Column(name = "ID_PK", scale = 0, precision = 10, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Candidate_Generator")
    @SequenceGenerator(name = "Candidate_Generator", allocationSize = 1, sequenceName = "CANDIDATE_GENERATOR_SEQ")
    private int id;
    @Basic
    @Column(name = "ESCRIPTION", length = 120)
    private String description;
    @Basic
    @Column(name = "STUDENT_ID_FK", nullable = false, scale = 0, precision = 10)
    private Integer studentId;
    @Basic
    @Column(name = "SELECTION_ID_FK", nullable = false, scale = 0, precision = 10)
    private Integer selectionId;
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STUDENT_ID_FK", insertable = false, updatable = false)
    private StudentEntity student;
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SELECTION_ID_FK", insertable = false, updatable = false)
    private SelectionEntity selection;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "candidate", cascade = CascadeType.ALL)
    private List<VoteEntity> votes;
}
