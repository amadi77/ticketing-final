package com.rajaee.ticketing.databse.core.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "FIELD", schema = "rajaee")
@Setter
@Getter
public class FieldEntity {
    @Id
    @Column(name = "ID_PK", nullable = false, scale = 0, precision = 10)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Field_Generator")
    @SequenceGenerator(name = "Field_Generator", allocationSize = 1, sequenceName = "FIELD_GENERATOR_SEQ")
    private int id;
    @Column(name = "FIELD_NAME", nullable = false)
    @Basic
    private String fieldName;
    @Column(name = "ORIENTATION", nullable = false)
    @Basic
    private String orientation;
    @Basic
    @Column(name = "COLLEGE_ID_PK", nullable = false)
    private Integer collegeId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COLLEGE_ID_PK", referencedColumnName = "ID_PK", insertable = false, updatable = false)
    private CollegeEntity college;
}
