package com.rajaee.ticketing.databse.core.entity;

import com.rajaee.ticketing.databse.core.statics.Gender;
import com.rajaee.ticketing.databse.core.statics.Grade;
import com.rajaee.ticketing.databse.security.entity.UserEntity;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

@Entity
@Table(name = "STUDENT", schema = "rajaee")
@Setter
@Getter
public class StudentEntity {
    @Id
    @Column(name = "ID_PK", scale = 0, precision = 10)
    private int id;
    @Basic
    @Column(name = "STUDENT_ID", nullable = false, length = 20)
    private String studentId;
    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "GENDER", nullable = false, length = 10)
    private Gender gender;
    @Basic
    @Column(name = "INPUT_YEAR", length = 4)
    private Integer inputYear;
    @Basic
    @Column(name = "COLLEGE_ID_FK", length = 40)
    private Integer collegeId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COLLEGE_ID_FK", updatable = false, insertable = false, referencedColumnName = "ID_PK")
    private CollegeEntity college;
    @Basic
    @Column(name = "ACTIVE", nullable = false)
    private boolean active;
    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "GRADE", nullable = false)
    private Grade grade;
    @Basic
    @Column(name = "FIELD_ID_FK", nullable = false)
    private Integer fieldId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FIELD_ID_FK", referencedColumnName = "ID_PK", insertable = false, updatable = false)
    private FieldEntity field;
    @Basic
    @Column(name = "DORM_ID_FK")
    private Integer dormId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DORM_ID_FK", referencedColumnName = "ID_PK", insertable = false, updatable = false)
    private DormEntity dorm;
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    @JoinColumn(name = "ID_PK")
    private UserEntity user;

}
