package com.rajaee.ticketing.databse.core.statics;


import lombok.Getter;
import lombok.Setter;

public enum Grade {
    BH(0),
    MA(1),
    PHD(2);

    @Setter
    @Getter
    private int value;

    Grade(int value) {
        this.value = value;
    }

    public static Grade getGrade(int value) {
        switch (value) {
            case 0:
                return Grade.BH;
            case 1:
                return Grade.MA;
            case 2:
                return Grade.PHD;
            default:
                return null;
        }
    }
}
