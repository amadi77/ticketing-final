package com.rajaee.ticketing.databse.core.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "VOTE", schema = "rajaee")
@Setter
@Getter
public class VoteEntity {
    @Id
    @Column(name = "ID_PK",scale = 0 , precision = 10)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "Vote_Generator")
    @SequenceGenerator(name = "Vote_Generator",allocationSize = 1,sequenceName = "VOTE_GENERATOR_SEQ")
    private int id;
    @Basic
    @Column(name = "STUDENT_FK_ID", scale = 0, precision = 10, nullable = false)
    private int studentId;
    @Basic
    @Column(name = "CANDIDATE_FK_ID", scale = 0, precision = 10, nullable = false)
    private int candidateId;
    @Basic
    @Column(name = "SELECTION_FK_ID", scale = 0, precision = 10, nullable = false)
    private int selectionId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STUDENT_ID_FK", updatable = false, insertable = false)
    private StudentEntity student;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CANDIDATE_ID_FK", updatable = false, insertable = false)
    private CandidateEntity candidate;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SELECTION_ID_FK", updatable = false, insertable = false)
    private SelectionEntity selection;
}
