package com.rajaee.ticketing.databse.core.statics;

import lombok.Getter;
import lombok.Setter;

public enum Gender {
    MAN(0),
    WOMAN(1);

    @Setter
    @Getter
    private int value;

    Gender(int value) {
        this.value = value;
    }

    public static Gender getGender(int value){
        if (value==0)
            return Gender.MAN;
        else if (value==1)
            return Gender.WOMAN;
        return null;
    }
}
