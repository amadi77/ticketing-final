package com.rajaee.ticketing.databse.core.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@Entity
@Table(name = "SELECTION", schema = "rajaee")
@Setter
@Getter
public class SelectionEntity {
    @Id
    @Column(name = "ID_PK", nullable = false, scale = 0, precision = 10)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Selection_generator")
    @SequenceGenerator(name = "Selection_generator", allocationSize = 1, sequenceName = "SELECTION_GENERATOR_SEQ")
    private int id;
    @Basic
    @Column(name = "NAME", nullable = false, length = 80)
    private String name;
    @Basic
    @Column(name = "CREATED", nullable = false)
    private ZonedDateTime created = ZonedDateTime.now();
    @Basic
    @Column(name = "COLLEGE_IDS", scale = 0, precision = 10, length = 30)
    private String collegeIds;
    @Basic
    @Column(name = "INPUT_YEARS", scale = 0, precision = 10, length = 30)
    private String inputYears;
    @Basic
    @Column(name = "DORM_IDS", scale = 0, precision = 10, length = 30)
    private String dormIds;
    @Basic
    @Column(name = "GRADES", scale = 0, precision = 10, length = 30)
    private String grades;
    @Basic
    @Column(name = "GENDERS", scale = 0, precision = 10, length = 30)
    private String gender;
    @Basic
    @Column(name = "FIELD_IDS", scale = 0, precision = 10, length = 30)
    private String fieldIds;
    @Basic
    @Column(name = "LIMIT_VOTE", nullable = false, scale = 0, precision = 10)
    private Integer limitVote;
    @Basic
    @Column(name = "FROM_DATE", nullable = false)
    private ZonedDateTime fromDate;
    @Basic
    @Column(name = "TO_DATE", nullable = false)
    private ZonedDateTime toDate;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "selection")
    private List<CandidateEntity> candidates;
}
