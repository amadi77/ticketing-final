package com.rajaee.ticketing.databse.security.entity;

import com.rajaee.ticketing.databse.core.entity.StudentEntity;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "USER", schema = "rajaee")
@Setter
@Getter
public class UserEntity {

    @Id
    @Column(name = "ID_PK", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "User_Generator")
    @SequenceGenerator(name = "User_Generator", allocationSize = 1, sequenceName = "USER_SEQ")
    private int id;
    @Basic
    @Column(name = "FIRST_NAME", nullable = false, length = 40)
    private String firstName;
    @Basic
    @Column(name = "LAST_NAME", nullable = false, length = 80)
    private String lastName;
    @Basic
    @Column(name = "FULL_NAME", nullable = false, length = 120)
    private String fullName;
    @Basic
    @Column(name = "PHONE_NUMBER", length = 15)
    private String phoneNumber;
    @Basic
    @Column(name = "EMAIL", length = 254)
    private String email;
    @Basic
    @Column(name = "NATIONAL_CODE", length = 10)
    private String nationalCode;
    @Basic
    @Column(name = "USERNAME", length = 50)
    private String username;
    @Basic
    @Column(name = "ACTIVE", nullable = false)
    private boolean active = true;
    @Basic
    @Column(name = "HASHED_PASSWORD", length = 150)
    private String hashedPassword;
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "USER_ROLE", schema = "rajaee",
            joinColumns = @JoinColumn(name = "USER_ID_FK", nullable = false, referencedColumnName = "ID_PK"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID_FK", nullable = false, referencedColumnName = "ID_PK")
    )
    private List<UserRoleEntity> roles;
    @OneToOne(optional = false, fetch = FetchType.LAZY, mappedBy = "user")
    private StudentEntity student;

    public void setUsername(String username) {
        if (username != null) {
            this.username = username;
        } else {
            this.username = this.nationalCode;
        }
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
        setUsername(this.username);
    }

    public void setFullName(String fullName) {
        if (fullName == null || fullName.equals(""))
            this.fullName = this.firstName + " " + this.lastName;
        else
            this.fullName = fullName;
    }
}
