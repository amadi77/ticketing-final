package com.rajaee.ticketing.databse.security.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpMethod;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "SECURITY_REST", schema = "rajaee")
@Setter
@Getter
public class SecurityRestEntity {
    @Id
    @Column(name = "ID_PK", scale = 0, precision = 10, nullable = false)
    private int id;
    @Basic
    @Column(name = "REST_URL", nullable = false)
    private String url;
    @Basic
    @Column(name = "METHOD_TYPE", nullable = false)
    private HttpMethod method;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "REST_IN_PERMISSION",
            schema = "rajaee",
            joinColumns = @JoinColumn(name = "REST_ID_FK", nullable = false, referencedColumnName = "ID_PK"),
            inverseJoinColumns = @JoinColumn(name = "PERMISSION_ID_FK", nullable = false, referencedColumnName = "ID_PK"))
    private List<SecurityPermissionEntity> permissions;
}
