package com.rajaee.ticketing.databse.security.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "SECURITY_PERMISSION", schema = "rajaee")
@Setter
@Getter
public class SecurityPermissionEntity {
    @Id
    @Column(name = "ID_PK", scale = 0, precision = 10, nullable = false)
    private int id;
    @Basic
    @Column(name = "PARENT_ID_FK", scale = 0, precision = 10)
    private Integer parentId;
    @Basic
    @Column(name = "NODE_TYPE", scale = 0, precision = 10, nullable = false)
    private int nodeType;

    @Basic
    @Column(name = "TRAVERSAL", nullable = false)
    private boolean traversal;
    @Basic
    @Column(name = "NAME", nullable = false, unique = true)
    private String name;
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ROLE_IN_PERMISSION",
            schema = "rajaee",
            joinColumns = @JoinColumn(name = "PERMISSION_ID_FK", nullable = false, referencedColumnName = "ID_PK"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID_FK", nullable = false, referencedColumnName = "ID_PK"))
    private List<UserRoleEntity> roles;
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "REST_IN_PERMISSION",
            schema = "rajaee",
            joinColumns = @JoinColumn(name = "PERMISSION_ID_FK", nullable = false, referencedColumnName = "ID_PK"),
            inverseJoinColumns = @JoinColumn(name = "REST_ID_FK", nullable = false, referencedColumnName = "ID_PK"))
    private List<SecurityRestEntity> rests;
}
