package com.rajaee.ticketing.databse.security.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ROLE", schema = "rajaee")
@Setter
@Getter
public class UserRoleEntity {
    @Id
    @Column(name = "ID_PK", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "User_Role_Generator")
    @SequenceGenerator(name = "User_Role_Generator", sequenceName = "USER_ROLE_SEQ", allocationSize = 1)
    private int id;
    @Basic
    @Column(name = "ROLE", nullable = false, length = 100)
    private String role;
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "USER_ROLE",
            schema = "rajaee",
            joinColumns = @JoinColumn(name = "ROLE_ID_FK", nullable = false, referencedColumnName = "ID_PK"),
            inverseJoinColumns = @JoinColumn(name = "USER_ID_FK", nullable = false, referencedColumnName = "ID_PK")
    )
    private List<UserEntity> users;
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "ROLE_IN_PERMISSION",
            schema = "rajaee",
            joinColumns = @JoinColumn(name = "ROLE_ID_FK", nullable = false, referencedColumnName = "ID_PK"),
            inverseJoinColumns = @JoinColumn(name = "PERMISSION_ID_FK", nullable = false, referencedColumnName = "ID_PK")
    )

    private List<SecurityPermissionEntity> permissions;
}
