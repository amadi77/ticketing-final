package com.rajaee.ticketing.utility.generic;

import java.util.Collection;

/**
 * @author imax on 12/12/19
 */
public class GenericUtil {

    public static boolean checkListSizeAndNullable(Collection<?>... t) {
        for (Collection<?> ts : t) {
            if (ts != null && !ts.isEmpty())
                return false;
        }
        return true;
    }


//    public static void main(String[] args) {
//        List<MellatConfig> z = new ArrayList<>();
//        List<EmailConfig> p = new ArrayList<>();
//        MellatConfig mellatConfig=new MellatConfig();
//        EmailConfig emailConfig=new EmailConfig();
//        z.add(mellatConfig);
//        p.add(emailConfig);
//        System.out.print("value is " + checkListSizeAndNullable(z, p));
//    }

}
