package com.rajaee.ticketing.utility.model.object;

import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

@Getter
@Setter
public class TokenInfo {
    private String accessToken;
    private String refreshToken;
    private Long ttl;
    private ZonedDateTime creationTime;

    public TokenInfo() {
    }

    public TokenInfo(String accessToken, String refreshToken, Long ttl) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.ttl = ttl;
        this.creationTime = ZonedDateTime.now();
    }
}
