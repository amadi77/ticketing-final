package com.rajaee.ticketing.utility.model.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ReportDto<Result, Aggregation> implements Serializable {
    private List<Result> result;
    private Aggregation aggregation;
    private Integer countAll;
    private Integer pageSize;
    private Integer pageNumber;
    private Map<String, ?> extraColumns;

    public ReportDto() {
    }

    public ReportDto(Integer countAll, Integer pageSize, Integer pageNumber) {
        this.result = null;
        this.countAll = countAll;
        this.pageSize = pageSize;
        this.pageNumber = pageNumber;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Aggregation getAggregation() {
        return aggregation;
    }

    public void setAggregation(Aggregation aggregation) {
        this.aggregation = aggregation;
    }

    public Integer getCountAll() {
        return countAll;
    }

    public void setCountAll(Integer countAll) {
        this.countAll = countAll;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Map<String, ?> getExtraColumns() {
        return extraColumns;
    }

    public void setExtraColumns(Map<String, ?> extraColumns) {
        this.extraColumns = extraColumns;
    }
}
