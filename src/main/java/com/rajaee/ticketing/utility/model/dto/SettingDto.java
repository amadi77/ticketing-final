package com.rajaee.ticketing.utility.model.dto;

import com.rajaee.ticketing.utility.config.property.Hosts;
import com.rajaee.ticketing.utility.config.property.identity.IdentitySettings;

public class SettingDto {
    private IdentitySettings identitySettings;
    private Hosts hosts;

    public IdentitySettings getIdentitySettings() {
        return identitySettings;
    }

    public void setIdentitySettings(IdentitySettings identitySettings) {
        this.identitySettings = identitySettings;
    }

    public Hosts getHosts() {
        return hosts;
    }

    public void setHosts(Hosts hosts) {
        this.hosts = hosts;
    }
}
