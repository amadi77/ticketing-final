package com.rajaee.ticketing.utility.model.dto.sms;

import com.rajaee.ticketing.utility.config.property.sms.SmsConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * @author imax on 6/30/19
 */
public class Magfa {

    private int priority;
    private List<SmsConfig> configs=new ArrayList<>();

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public List<SmsConfig> getConfigs() {
        return configs;
    }

    public void setConfigs(List<SmsConfig> configs) {
        this.configs = configs;
    }
}
