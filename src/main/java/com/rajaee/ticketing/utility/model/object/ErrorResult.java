package com.rajaee.ticketing.utility.model.object;

import javax.servlet.http.HttpServletResponse;

public class ErrorResult {
    private SystemError error;
    private Integer errorCode;
    private String description;

    public ErrorResult(SystemError error, Integer errorCode, String description) {
        this.error = error;
        this.errorCode = errorCode;
        this.description = description;
    }

    public ErrorResult(SystemException exception) {
        this.error = exception.getError();
        this.errorCode = exception.getErrorCode();
        this.description=exception.getArgument();
    }

    public ErrorResult(SystemException exception, HttpServletResponse response) {
        this.error = exception.getError();
        this.errorCode = exception.getErrorCode();
        response.setStatus(this.error.getValue());
    }

    public SystemError getError() {
        return error;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public String getDescription() {
        return description;
    }


}
