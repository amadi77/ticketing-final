package com.rajaee.ticketing.utility.model.dto.sms;

/**
 * @author imax on 6/24/19
 */
public class SmsIn {

    private String message;
    private String to;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
