package com.rajaee.ticketing.utility.model.dto;

import com.rajaee.ticketing.utility.model.object.SortOption;
import com.rajaee.ticketing.utility.statics.enums.SortType;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author imax on 1/4/20
 */
@Getter
@Setter
public class PageableInfoFilter {
    private Integer page;
    private Integer size;
    private List<SortOption> sort;
    private String name;

    public PageableInfoFilter() {
        this.page = 1;
        this.size = 50;
        this.sort = new ArrayList<>();
        this.sort.add(new SortOption("id", SortType.DESCENDING));
    }
}
