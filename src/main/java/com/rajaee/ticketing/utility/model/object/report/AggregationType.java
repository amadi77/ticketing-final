package com.rajaee.ticketing.utility.model.object.report;

public enum AggregationType {
    SUM,
    AVG,
    MAX,
    MIN
}
