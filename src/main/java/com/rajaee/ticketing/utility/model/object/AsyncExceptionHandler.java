package com.rajaee.ticketing.utility.model.object;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;

import java.lang.reflect.Method;



public class AsyncExceptionHandler extends SimpleAsyncUncaughtExceptionHandler {

    private static final Logger devLogger = LogManager.getLogger(AsyncExceptionHandler.class);

    @Override
    public void handleUncaughtException(Throwable throwable, Method method, Object... objects) {
        SystemException systemException = (SystemException) throwable;
    }

}
