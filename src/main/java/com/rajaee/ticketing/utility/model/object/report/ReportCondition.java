package com.rajaee.ticketing.utility.model.object.report;

import com.rajaee.ticketing.utility.model.dto.ConditionParameters;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class ReportCondition {
    private List<ConditionParameters> controlFlag;

    private List<String> nullCondition;
    private List<String> notNullCondition;

    private List<ConditionParameters> equalCondition;
    private List<ConditionParameters> notEqualCondition;

    private List<ConditionParameters> likeCondition;

    private List<ConditionParameters> inCondition;

    private List<ConditionParameters> minNumberCondition;
    private List<ConditionParameters> maxNumberCondition;

    private List<ConditionParameters> minDateCondition;
    private List<ConditionParameters> maxDateCondition;

    private List<ConditionParameters> minTimeCondition;
    private List<ConditionParameters> maxTimeCondition;

    private List<ConditionParameters> minZonedTimeCondition;
    private List<ConditionParameters> maxZonedTimeCondition;

    private List<ReportCondition> orConditions;
    private List<ReportCriteriaJoinCondition> joinCondition;

    public ReportCondition() {
        this.controlFlag = new ArrayList<>();
        this.nullCondition = new ArrayList<>();
        this.notNullCondition = new ArrayList<>();
        this.equalCondition = new ArrayList<>();
        this.notEqualCondition = new ArrayList<>();
        this.likeCondition = new ArrayList<>();
        this.inCondition = new ArrayList<>();
        this.minNumberCondition = new ArrayList<>();
        this.maxNumberCondition = new ArrayList<>();
        this.minDateCondition = new ArrayList<>();
        this.maxDateCondition = new ArrayList<>();
        this.minTimeCondition = new ArrayList<>();
        this.maxTimeCondition = new ArrayList<>();
        this.minZonedTimeCondition = new ArrayList<>();
        this.maxZonedTimeCondition = new ArrayList<>();
        this.joinCondition = new ArrayList<>();
        this.orConditions = new ArrayList<>();
    }

    public void addControlFlag(String key, Boolean value) {
        this.controlFlag.add(new ConditionParameters(key, value));
    }

    public void addNullCondition(String value) {
        this.nullCondition.add(value);
    }

    public void addNotNullCondition(String value) {
        this.notNullCondition.add(value);
    }

    public void addEqualCondition(String key, Object value) {
        this.equalCondition.add(new ConditionParameters(key, value));
    }

    public void addNotEqualCondition(String key, Object value) {
        this.notEqualCondition.add(new ConditionParameters(key, value));
    }

    public void addLikeCondition(String key, String value) {
        this.likeCondition.add(new ConditionParameters(key, value));
    }

    public void addInCondition(String key, List<?> value) {
        this.inCondition.add(new ConditionParameters(key, value));
    }

    public void addMinNumberCondition(String key, Number value) {
        this.minNumberCondition.add(new ConditionParameters(key, value));
    }

    public void addMaxNumberCondition(String key, Number value) {
        this.maxNumberCondition.add(new ConditionParameters(key, value));
    }

    public void addMinTimeCondition(String key, LocalTime value) {
        this.minTimeCondition.add(new ConditionParameters(key, value));
    }

    public void addMaxTimeCondition(String key, LocalTime value) {
        this.maxTimeCondition.add(new ConditionParameters(key, value));
    }

    public void addMinZonedTimeCondition(String key, ZonedDateTime value) {
        this.minZonedTimeCondition.add(new ConditionParameters(key, value));
    }

    public void addMaxZonedTimeCondition(String key, ZonedDateTime value) {
        this.maxZonedTimeCondition.add(new ConditionParameters(key, value));
    }

    public void addOrCondition(ReportCondition condition) {
        this.orConditions.add(condition);
    }

    public void addJoinCondition(ReportCriteriaJoinCondition value) {
        this.joinCondition.add(value);
    }

}
