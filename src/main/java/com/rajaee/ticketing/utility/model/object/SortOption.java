package com.rajaee.ticketing.utility.model.object;


import com.rajaee.ticketing.utility.statics.enums.SortType;

public class SortOption {

    private String column;
    private SortType type;

    public SortOption() {
    }

    public SortOption(String column, SortType type) {
        this.column = column;
        this.type = type;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public SortType getType() {
        return type;
    }

    public void setType(SortType type) {
        this.type = type;
    }
}
