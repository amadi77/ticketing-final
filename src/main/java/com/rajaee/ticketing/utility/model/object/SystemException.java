package com.rajaee.ticketing.utility.model.object;

import java.util.List;

/**
 * System Exception Class,
 * A Customized {@link RuntimeException} for Whole System with a proper {@link SystemError} Type and Error Code
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
public class SystemException extends Exception {
    private final SystemError error;
    private final Integer errorCode;
    private final String argument;
    private final List<ErrorResult> errorResults;

    public SystemException(SystemError error, String argument, Integer errorCode) {
        this.error = error;
        this.argument = argument;
        this.errorCode = errorCode;
        this.errorResults = null;
    }

    public SystemException(SystemError error, String argument, Integer errorCode, List<ErrorResult> errorResults) {
        this.error = error;
        this.argument = argument;
        this.errorCode = errorCode;
        this.errorResults = errorResults;
    }

    public SystemError getError() {
        return error;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public String getArgument() {
        return argument;
    }

    public List<ErrorResult> getErrorResults() {
        return errorResults;
    }
}
