package com.rajaee.ticketing.utility.model.object.report;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.criteria.JoinType;
import java.time.ZonedDateTime;
import java.util.*;

@Data
@EqualsAndHashCode(callSuper = false)
public class ReportCriteriaJoinCondition {
    private String name;
    private JoinType joinType;
    private boolean fetch = false;
    private Map<String, Object> equalCondition;
    private Map<String, Object> likeCondition;
    private Map<String, Date> minDateCondition;
    private Map<String, Date> maxDateCondition;
    private Map<String, ZonedDateTime> minZonedTimeCondition;
    private Map<String, ZonedDateTime> maxZonedTimeCondition;
    private Map<String, List<? extends Number>> inCondition;
    private List<ReportCriteriaJoinCondition> children;


    public ReportCriteriaJoinCondition(String name, JoinType joinType) {
        this.name = name;
        this.joinType = joinType;
        this.equalCondition = new HashMap<>();
        this.likeCondition = new HashMap<>();
        this.minDateCondition = new HashMap<>();
        this.maxDateCondition = new HashMap<>();
        this.minZonedTimeCondition = new HashMap<>();
        this.maxZonedTimeCondition = new HashMap<>();
        this.inCondition = new HashMap<>();
        this.children = new ArrayList<>();
    }

    public ReportCriteriaJoinCondition(String name, JoinType joinType, boolean fetch) {
        this.name = name;
        this.joinType = joinType;
        this.equalCondition = new HashMap<>();
        this.likeCondition = new HashMap<>();
        this.minDateCondition = new HashMap<>();
        this.maxDateCondition = new HashMap<>();
        this.minZonedTimeCondition = new HashMap<>();
        this.maxZonedTimeCondition = new HashMap<>();
        this.inCondition = new HashMap<>();
        this.children = new ArrayList<>();
        this.fetch = fetch;
    }

    public void addEqualCondition(String key, Object value) {
        this.equalCondition.put(key, value);
    }

    public void addMinTimestampCondition(String key, Date value) {
        this.minDateCondition.put(key, value);
    }

    public void addMaxTimestampCondition(String key, Date value) {
        this.maxDateCondition.put(key, value);
    }

    public void addMinZonedTimeCondition(String key, ZonedDateTime value) {
        this.minZonedTimeCondition.put(key, value);
    }

    public void addMaxZonedTimeCondition(String key, ZonedDateTime value) {
        this.maxZonedTimeCondition.put(key, value);
    }

    public void addInCondition(String key, List<? extends Number> value) {
        this.inCondition.put(key, value);
    }

    public void addLikeCondition(String key, String value) {
        this.likeCondition.put(key, value);
    }

    public void addChild(ReportCriteriaJoinCondition join) {
        this.children.add(join);
    }

}
