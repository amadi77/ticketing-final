package com.rajaee.ticketing.utility.model.dto.email;

import com.rajaee.ticketing.utility.config.property.email.EmailConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * @author imax on 8/29/19
 */
public class Gmail {

    private int priority;
    private List<EmailConfig> configs=new ArrayList<>();

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public List<EmailConfig> getConfigs() {
        return configs;
    }

    public void setConfigs(List<EmailConfig> configs) {
        this.configs = configs;
    }
}
