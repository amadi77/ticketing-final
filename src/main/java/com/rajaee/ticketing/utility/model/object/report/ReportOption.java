package com.rajaee.ticketing.utility.model.object.report;


import com.rajaee.ticketing.utility.model.object.SortOption;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class ReportOption {
    private boolean distinct = false;
    private Integer pageSize;
    private Integer pageNumber; // start from 1
    private List<SortOption> sortOptions;

    public ReportOption() {
        this.pageSize = 10;
        this.pageNumber = 1;
    }

    public ReportOption(Integer pageSize, Integer pageNumber, List<SortOption> sortOptions) {
        this.pageSize = pageSize;
        this.pageNumber = pageNumber;
        this.sortOptions = sortOptions;
    }

}
