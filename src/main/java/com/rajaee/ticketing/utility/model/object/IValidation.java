package com.rajaee.ticketing.utility.model.object;

public interface IValidation {
    void validate() throws SystemException;
}
