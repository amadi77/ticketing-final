package com.rajaee.ticketing.utility.config.property.payment.pasargad;

import com.rajaee.ticketing.utility.config.property.payment.PayGatewayConfigBase;
import org.springframework.stereotype.Component;

/**
 * @author imax on 7/15/19
 */
@Component
public class PasargadConfig extends PayGatewayConfigBase {

    private String Sign;
    private String terminalCode;
    private String merchantCode;
    private String redirectAddress;
    private int action;
    private String merchantName;
    private String tokenUrl;
    private String paymentUrl;
    private String verifyUrl;
    private String checkTrUrl;
    private String revertUrl;
    private String priority;


    public String getSign() {
        return Sign;
    }

    public void setSign(String sign) {
        Sign = sign;
    }

    public String getTerminalCode() {
        return terminalCode;
    }

    public void setTerminalCode(String terminalCode) {
        this.terminalCode = terminalCode;
    }

    public String getMerchantCode() {
        return merchantCode;
    }

    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    public String getRedirectAddress() {
        return redirectAddress;
    }

    public void setRedirectAddress(String redirectAddress) {
        this.redirectAddress = redirectAddress;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getPaymentUrl() {
        return paymentUrl;
    }

    public void setPaymentUrl(String paymentUrl) {
        this.paymentUrl = paymentUrl;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getVerifyUrl() {
        return verifyUrl;
    }

    public void setVerifyUrl(String verifyUrl) {
        this.verifyUrl = verifyUrl;
    }

    public String getCheckTrUrl() {
        return checkTrUrl;
    }

    public void setCheckTrUrl(String checkTrUrl) {
        this.checkTrUrl = checkTrUrl;
    }

    public String getRevertUrl() {
        return revertUrl;
    }

    public void setRevertUrl(String revertUrl) {
        this.revertUrl = revertUrl;
    }

    public String getTokenUrl() {
        return tokenUrl;
    }

    public void setTokenUrl(String tokenUrl) {
        this.tokenUrl = tokenUrl;
    }
}
