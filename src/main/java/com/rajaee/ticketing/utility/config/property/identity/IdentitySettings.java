package com.rajaee.ticketing.utility.config.property.identity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdentitySettings {
    private SignInOptions signIn;
    private RegistrationOptions registration;
    private ProfileOptions profile;
    private PasswordOptions password;
    private LockoutOptions lockout;
    private JwtOptions jwt;

}
