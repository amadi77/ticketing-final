package com.rajaee.ticketing.utility.config.property.sms;

import com.rajaee.ticketing.utility.model.dto.sms.Magfa;

/**
 * @author imax on 6/30/19
 */
public class SmsProvider {

    private Magfa magfa;

    public Magfa getMagfa() {
        return magfa;
    }

    public void setMagfa(Magfa magfa) {
        this.magfa = magfa;
    }
}
