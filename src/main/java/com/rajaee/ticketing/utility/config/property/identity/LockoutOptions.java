package com.rajaee.ticketing.utility.config.property.identity;

public class LockoutOptions {
    private boolean allowedForNewUsers = true;
    private int maxFailedAccessAttempts = 5;

    public boolean isAllowedForNewUsers() {
        return allowedForNewUsers;
    }

    public void setAllowedForNewUsers(boolean allowedForNewUsers) {
        this.allowedForNewUsers = allowedForNewUsers;
    }

    public int getMaxFailedAccessAttempts() {
        return maxFailedAccessAttempts;
    }

    public void setMaxFailedAccessAttempts(int maxFailedAccessAttempts) {
        this.maxFailedAccessAttempts = maxFailedAccessAttempts;
    }
}
