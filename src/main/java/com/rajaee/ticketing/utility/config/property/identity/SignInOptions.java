package com.rajaee.ticketing.utility.config.property.identity;

public class SignInOptions {
    private boolean loginByMobile = true;
    private boolean loginByEmail = true;
    private boolean loginByOtp = true;
    private boolean loginByGoogle = false;
    private boolean loginByPod = true;
    private boolean requireConfirmedMobile = false;
    private boolean requireConfirmedEmail = false;

    public boolean isLoginByMobile() {
        return loginByMobile;
    }

    public void setLoginByMobile(boolean loginByMobile) {
        this.loginByMobile = loginByMobile;
    }

    public boolean isLoginByEmail() {
        return loginByEmail;
    }

    public void setLoginByEmail(boolean loginByEmail) {
        this.loginByEmail = loginByEmail;
    }

    public boolean isLoginByOtp() {
        return loginByOtp;
    }

    public void setLoginByOtp(boolean loginByOtp) {
        this.loginByOtp = loginByOtp;
    }

    public boolean isLoginByGoogle() {
        return loginByGoogle;
    }

    public void setLoginByGoogle(boolean loginByGoogle) {
        this.loginByGoogle = loginByGoogle;
    }

    public boolean isLoginByPod() {
        return loginByPod;
    }

    public void setLoginByPod(boolean loginByPod) {
        this.loginByPod = loginByPod;
    }

    public boolean isRequireConfirmedMobile() {
        return requireConfirmedMobile;
    }

    public void setRequireConfirmedMobile(boolean requireConfirmedMobile) {
        this.requireConfirmedMobile = requireConfirmedMobile;
    }

    public boolean isRequireConfirmedEmail() {
        return requireConfirmedEmail;
    }

    public void setRequireConfirmedEmail(boolean requireConfirmedEmail) {
        this.requireConfirmedEmail = requireConfirmedEmail;
    }
}
