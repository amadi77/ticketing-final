package com.rajaee.ticketing.utility.config.property;

import com.rajaee.ticketing.utility.config.property.email.EmailProvider;
import com.rajaee.ticketing.utility.config.property.file.FileCrud;
import com.rajaee.ticketing.utility.config.property.identity.IdentitySettings;
import com.rajaee.ticketing.utility.config.property.payment.PaymentProvider;
import com.rajaee.ticketing.utility.config.property.sms.SmsProvider;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "")
@Data @AllArgsConstructor
@NoArgsConstructor
public class ApplicationProperties {
    private boolean initDB;
    private IdentitySettings identitySettings;
    private Hosts hosts;
    private String tokenSecretKey;
    private Integer delayMinutes;
    private Integer accessLockLimit;
    private SmsProvider smsProviders;
    private EmailProvider emailProvider;
    private PaymentProvider paymentProviders;
    private Boolean smsOtpSandbox;
    private FileCrud fileCrud;
    private currency currency;
    private List<String> menu = new ArrayList<>(0);
    private List<String> widgetAreas = new ArrayList<>();
}
