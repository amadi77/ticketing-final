package com.rajaee.ticketing.utility.config.property.payment.mellat;

import java.util.List;

public class MellatPayment {

    List<MellatConfig> configs;

    public List<MellatConfig> getConfigs() {
        return configs;
    }

    public void setConfigs(List<MellatConfig> configs) {
        this.configs = configs;
    }

}
