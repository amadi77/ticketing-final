package com.rajaee.ticketing.utility.config.property.payment.zarrinpal;

import java.util.List;

/**
 * @author imax on 7/3/19
 */
public class ZarrinpalPayment {

    private List<ZrConfig> configs;

    public List<ZrConfig> getConfigs() {
        return configs;
    }

    public void setConfigs(List<ZrConfig> configs) {
        this.configs = configs;
    }
}
