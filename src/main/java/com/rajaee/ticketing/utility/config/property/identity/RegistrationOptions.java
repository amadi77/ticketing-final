package com.rajaee.ticketing.utility.config.property.identity;

public class RegistrationOptions {
    private boolean registerEnabled = true;
    private boolean registerByOtp = true;
    private boolean showFirstName = true;
    private boolean showLastName = true;
    private boolean showEmail = false;
    private boolean showMobile = true;
    private boolean showNationalId = false;
    private boolean showEconomicCode = false;
    private boolean showBirthDate = false;
    private boolean showGender = false;
    private boolean showReferrerMobile = true;
    private boolean requireFirstName = true;
    private boolean requireLastName = true;
    private boolean requireEmail = false;
    private boolean requireMobile = true;
    private boolean requireNationalId = false;
    private boolean requireEconomicCode = false;
    private boolean requireBirthDate = false;
    private boolean requireGender = false;
    private boolean requireReferrerMobile = true;
    private boolean validateNationalId = false;

    public boolean isRegisterEnabled() {
        return registerEnabled;
    }

    public void setRegisterEnabled(boolean registerEnabled) {
        this.registerEnabled = registerEnabled;
    }

    public boolean isRegisterByOtp() {
        return registerByOtp;
    }

    public void setRegisterByOtp(boolean registerByOtp) {
        this.registerByOtp = registerByOtp;
    }

    public boolean isShowFirstName() {
        return showFirstName;
    }

    public void setShowFirstName(boolean showFirstName) {
        this.showFirstName = showFirstName;
    }

    public boolean isShowLastName() {
        return showLastName;
    }

    public void setShowLastName(boolean showLastName) {
        this.showLastName = showLastName;
    }

    public boolean isShowEmail() {
        return showEmail;
    }

    public void setShowEmail(boolean showEmail) {
        this.showEmail = showEmail;
    }

    public boolean isShowMobile() {
        return showMobile;
    }

    public void setShowMobile(boolean showMobile) {
        this.showMobile = showMobile;
    }

    public boolean isShowNationalId() {
        return showNationalId;
    }

    public void setShowNationalId(boolean showNationalId) {
        this.showNationalId = showNationalId;
    }

    public boolean isShowEconomicCode() {
        return showEconomicCode;
    }

    public void setShowEconomicCode(boolean showEconomicCode) {
        this.showEconomicCode = showEconomicCode;
    }

    public boolean isShowBirthDate() {
        return showBirthDate;
    }

    public void setShowBirthDate(boolean showBirthDate) {
        this.showBirthDate = showBirthDate;
    }

    public boolean isShowGender() {
        return showGender;
    }

    public void setShowGender(boolean showGender) {
        this.showGender = showGender;
    }

    public boolean isShowReferrerMobile() {
        return showReferrerMobile;
    }

    public void setShowReferrerMobile(boolean showReferrerMobile) {
        this.showReferrerMobile = showReferrerMobile;
    }

    public boolean isRequireFirstName() {
        return requireFirstName;
    }

    public void setRequireFirstName(boolean requireFirstName) {
        this.requireFirstName = requireFirstName;
    }

    public boolean isRequireLastName() {
        return requireLastName;
    }

    public void setRequireLastName(boolean requireLastName) {
        this.requireLastName = requireLastName;
    }

    public boolean isRequireEmail() {
        return requireEmail;
    }

    public void setRequireEmail(boolean requireEmail) {
        this.requireEmail = requireEmail;
    }

    public boolean isRequireMobile() {
        return requireMobile;
    }

    public void setRequireMobile(boolean requireMobile) {
        this.requireMobile = requireMobile;
    }

    public boolean isRequireNationalId() {
        return requireNationalId;
    }

    public void setRequireNationalId(boolean requireNationalId) {
        this.requireNationalId = requireNationalId;
    }

    public boolean isRequireEconomicCode() {
        return requireEconomicCode;
    }

    public void setRequireEconomicCode(boolean requireEconomicCode) {
        this.requireEconomicCode = requireEconomicCode;
    }

    public boolean isRequireBirthDate() {
        return requireBirthDate;
    }

    public void setRequireBirthDate(boolean requireBirthDate) {
        this.requireBirthDate = requireBirthDate;
    }

    public boolean isRequireGender() {
        return requireGender;
    }

    public void setRequireGender(boolean requireGender) {
        this.requireGender = requireGender;
    }

    public boolean isRequireReferrerMobile() {
        return requireReferrerMobile;
    }

    public void setRequireReferrerMobile(boolean requireReferrerMobile) {
        this.requireReferrerMobile = requireReferrerMobile;
    }

    public boolean isValidateNationalId() {
        return validateNationalId;
    }

    public void setValidateNationalId(boolean validateNationalId) {
        this.validateNationalId = validateNationalId;
    }

    public void validateDetails() {
        if (this.requireFirstName) {
            this.showFirstName = true;
        }
        if (this.requireLastName) {
            this.showLastName = true;
        }
        if (this.requireEmail) {
            this.showEmail = true;
        }
        if (this.requireMobile) {
            this.showMobile = true;
        }
        if (this.requireNationalId || this.validateNationalId) {
            this.showNationalId = true;
        }
        if (this.requireEconomicCode) {
            this.showEconomicCode = true;
        }
        if (this.requireBirthDate) {
            this.showBirthDate = true;
        }
        if (this.requireGender) {
            this.showGender = true;
        }
        if (this.requireReferrerMobile) {
            this.showReferrerMobile = true;
        }
    }
}
