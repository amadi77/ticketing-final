package com.rajaee.ticketing.utility.config.property.payment.zarrinpal;

import com.rajaee.ticketing.utility.config.property.payment.PayGatewayConfigBase;

/**
 * @author imax on 7/3/19
 */
public class ZrConfig extends PayGatewayConfigBase {

    private String merchantID;
    private String callbackURL;
    private String gatewayUrl;
    private String gatewayUrlZr;
    private String priority;

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getCallbackURL() {
        return callbackURL;
    }

    public void setCallbackURL(String callbackURL) {
        this.callbackURL = callbackURL;
    }

    public String getGatewayUrl() {
        return gatewayUrl;
    }

    public void setGatewayUrl(String gatewayUrl) {
        this.gatewayUrl = gatewayUrl;
    }

    public String getGatewayUrlZr() {
        return gatewayUrlZr;
    }

    public void setGatewayUrlZr(String gatewayUrlZr) {
        this.gatewayUrlZr = gatewayUrlZr;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }
}
