package com.rajaee.ticketing.utility.config.property.payment.pasargad;

import java.util.List;

/**
 * @author imax on 7/15/19
 */
public class PasargadPayment {

    private List<PasargadConfig> configs;

    public List<PasargadConfig> getConfigs() {
        return configs;
    }

    public void setConfigs(List<PasargadConfig> configs) {
        this.configs = configs;
    }
}
