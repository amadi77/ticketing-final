package com.rajaee.ticketing.utility.config.property.email;

import com.rajaee.ticketing.utility.statics.enums.EmailProtocolType;

/**
 * @author imax on 8/29/19
 */
public class EmailConfig {

    private String userName;
    private String password;
    private String host;
    private String port;
    private EmailProtocolType protocol;
    private boolean auth;
    private boolean starttls;
    private int priority;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public EmailProtocolType getProtocol() {
        return protocol;
    }

    public void setProtocol(EmailProtocolType protocol) {
        this.protocol = protocol;
    }

    public boolean isAuth() {
        return auth;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }

    public boolean isStarttls() {
        return starttls;
    }

    public void setStarttls(boolean starttls) {
        this.starttls = starttls;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
