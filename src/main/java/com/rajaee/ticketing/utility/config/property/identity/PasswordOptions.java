package com.rajaee.ticketing.utility.config.property.identity;

public class PasswordOptions {
    private int requiredLength = 6;
    private int requiredUniqueChars = 0;
    private boolean requireNonAlphaNumeric = false;
    private boolean requireLowercase = false;
    private boolean requireUppercase = false;
    private boolean requireDigit = false;

    public int getRequiredLength() {
        return requiredLength;
    }

    public void setRequiredLength(int requiredLength) {
        this.requiredLength = requiredLength;
    }

    public int getRequiredUniqueChars() {
        return requiredUniqueChars;
    }

    public void setRequiredUniqueChars(int requiredUniqueChars) {
        this.requiredUniqueChars = requiredUniqueChars;
    }

    public boolean isRequireNonAlphaNumeric() {
        return requireNonAlphaNumeric;
    }

    public void setRequireNonAlphaNumeric(boolean requireNonAlphaNumeric) {
        this.requireNonAlphaNumeric = requireNonAlphaNumeric;
    }

    public boolean isRequireLowercase() {
        return requireLowercase;
    }

    public void setRequireLowercase(boolean requireLowercase) {
        this.requireLowercase = requireLowercase;
    }

    public boolean isRequireUppercase() {
        return requireUppercase;
    }

    public void setRequireUppercase(boolean requireUppercase) {
        this.requireUppercase = requireUppercase;
    }

    public boolean isRequireDigit() {
        return requireDigit;
    }

    public void setRequireDigit(boolean requireDigit) {
        this.requireDigit = requireDigit;
    }
}
