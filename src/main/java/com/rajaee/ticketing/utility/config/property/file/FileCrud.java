package com.rajaee.ticketing.utility.config.property.file;

/**
 * @author imax on 8/3/19
 */
public class FileCrud {
    private String root;
    private String baseUrl;
    private String baseFilePath;
    private String tempFilePath;
    private String tempFileUrl;

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getBaseFilePath() {
        return baseFilePath;
    }

    public void setBaseFilePath(String baseFilePath) {
        this.baseFilePath = baseFilePath;
    }

    public String getTempFilePath() {
        return tempFilePath;
    }

    public void setTempFilePath(String tempFilePath) {
        this.tempFilePath = tempFilePath;
    }

    public String getTempFileUrl() {
        return tempFileUrl;
    }

    public void setTempFileUrl(String tempFileUrl) {
        this.tempFileUrl = tempFileUrl;
    }
}
