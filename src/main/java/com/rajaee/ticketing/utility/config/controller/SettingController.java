package com.rajaee.ticketing.utility.config.controller;

import com.rajaee.ticketing.utility.model.dto.SettingDto;
import com.rajaee.ticketing.utility.repository.service.SettingService;
import com.rajaee.ticketing.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Api(tags = {"Setting"})
@RestController
@RequestMapping(value = RestApi.REST_PUBLIC)
@Validated
public class SettingController {

    private final SettingService settingService;

    @Autowired
    public SettingController(SettingService settingService) {
        this.settingService = settingService;
    }

    @ApiOperation(value = "Get all coupons matched by filter from the data source", response = SettingDto.class)
    @GetMapping(path = "/setting", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAll(@RequestParam(value = "include",required = false) String[] include, HttpServletRequest httpServletRequest){
        return new ResponseEntity<>(settingService.getApplicationSetting(include), HttpStatus.OK);
    }
}
