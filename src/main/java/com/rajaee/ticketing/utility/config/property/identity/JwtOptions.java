package com.rajaee.ticketing.utility.config.property.identity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtOptions {
    private long mobileAccessTokenExpireMin;
    private long browserAccessTokenExpireMin;
    private long mobileRefreshTokenExpireMin;
    private long browserRefreshTokenExpireMin;
    private String refreshPath;
    private String securityKey = "";

}
