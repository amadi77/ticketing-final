package com.rajaee.ticketing.utility.config.property.email;

import com.rajaee.ticketing.utility.model.dto.email.Amazon;
import com.rajaee.ticketing.utility.model.dto.email.Gmail;

/**
 * @author imax on 8/29/19
 */
public class EmailProvider {

    private Amazon amazon;
    private Gmail gmail;

    public Amazon getAmazon() {
        return amazon;
    }

    public void setAmazon(Amazon amazon) {
        this.amazon = amazon;
    }

    public Gmail getGmail() {
        return gmail;
    }

    public void setGmail(Gmail gmail) {
        this.gmail = gmail;
    }
}
