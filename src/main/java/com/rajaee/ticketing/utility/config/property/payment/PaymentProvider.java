package com.rajaee.ticketing.utility.config.property.payment;

import com.rajaee.ticketing.utility.config.property.payment.mellat.MellatPayment;
import com.rajaee.ticketing.utility.config.property.payment.parsian.ParsianPayment;
import com.rajaee.ticketing.utility.config.property.payment.pasargad.PasargadPayment;
import com.rajaee.ticketing.utility.config.property.payment.zarrinpal.ZarrinpalPayment;

/**
 * @author imax on 7/3/19
 */
public class PaymentProvider {

   private ZarrinpalPayment zarrinpalPayment;
   private MellatPayment mellatPayment;
   private PasargadPayment pasargadPayment;
   private ParsianPayment parsianPayment;

    public ZarrinpalPayment getZarrinpalPayment() {
        return zarrinpalPayment;
    }

    public void setZarrinpalPayment(ZarrinpalPayment zarrinpalPayment) {
        this.zarrinpalPayment = zarrinpalPayment;
    }

    public MellatPayment getMellatPayment() {
        return mellatPayment;
    }

    public void setMellatPayment(MellatPayment mellatPayment) {
        this.mellatPayment = mellatPayment;
    }

    public PasargadPayment getPasargadPayment() {
        return pasargadPayment;
    }

    public void setPasargadPayment(PasargadPayment pasargadPayment) {
        this.pasargadPayment = pasargadPayment;
    }

    public ParsianPayment getParsianPayment() {
        return parsianPayment;
    }

    public void setParsianPayment(ParsianPayment parsianPayment) {
        this.parsianPayment = parsianPayment;
    }
}
