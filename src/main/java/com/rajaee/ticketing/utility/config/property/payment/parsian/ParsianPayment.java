package com.rajaee.ticketing.utility.config.property.payment.parsian;

import java.util.List;

public class ParsianPayment {
    private List<ParsianConfig> configs;

    public List<ParsianConfig> getConfigs() {
        return configs;
    }

    public void setConfigs(List<ParsianConfig> configs) {
        this.configs = configs;
    }
}
