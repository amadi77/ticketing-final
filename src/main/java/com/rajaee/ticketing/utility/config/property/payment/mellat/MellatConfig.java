package com.rajaee.ticketing.utility.config.property.payment.mellat;

import com.rajaee.ticketing.utility.config.property.payment.PayGatewayConfigBase;
import org.springframework.stereotype.Component;

@Component
public class MellatConfig extends PayGatewayConfigBase {

    private Long terminalId;
    private String username;
    private String password;
    private String url;
    private String callbackUrl;
    private int priority;

    public Long getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Long terminalId) {
        this.terminalId = terminalId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
