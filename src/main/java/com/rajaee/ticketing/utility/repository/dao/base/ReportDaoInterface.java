package com.rajaee.ticketing.utility.repository.dao.base;

import com.rajaee.ticketing.utility.model.object.report.ReportAggregation;
import com.rajaee.ticketing.utility.model.object.report.ReportCondition;
import com.rajaee.ticketing.utility.model.object.report.ReportOption;
import com.rajaee.ticketing.utility.model.object.report.ReportQuery;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ReportDaoInterface {

    <Result> Integer countEntity(Class<Result> cls, ReportOption reportOption, ReportCondition reportCondition);

    <Result> List<Result> reportEntity(Class<Result> cls, ReportOption reportOption, ReportCondition reportCondition);

    <Result> List<Result> reportEntityWithInclude(Class<Result> cls, ReportOption reportOption, ReportCondition reportCondition, String[] include);

    <Result> Integer countComplex(Class<Result> cls, ReportOption reportOption, ReportQuery reportQuery);

    <Result> List<Result> reportComplex(Class<Result> cls, ReportOption reportOption, ReportQuery reportQuery);

    <Aggregation> Aggregation aggregateComplex(Class<Aggregation> cls, ReportOption reportOption, ReportQuery reportQuery,
                                               ReportAggregation reportAggregation);

    <Result> List<Result> reportEntityWithIncludeJoin(Class<Result> cls, ReportOption reportOption, ReportCondition reportCondition, String[] include);

    <Result> List<Result> reportHqlWithIncludeJoin(Class<Result> cls, String hql, ReportOption reportOption, Map<String, Object> parameters, String[] include);

}
