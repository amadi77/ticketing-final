package com.rajaee.ticketing.utility.repository.service;

import com.rajaee.ticketing.utility.repository.dao.base.BaseDaoInterface;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author imax on 9/30/19
 */
@Service
public class GenericService {

    private final ModelMapper modelMapper;
    private Integer id;
    private final BaseDaoInterface baseDaoInterface;

    @Autowired
    public GenericService(ModelMapper modelMapper, BaseDaoInterface baseDaoInterface) {
        this.modelMapper = modelMapper;
        this.baseDaoInterface = baseDaoInterface;
    }

    @Transactional
    public <S, T > void upsert(List<T> models, List<Integer> ids, Class<S> sClass,String param) throws NoSuchFieldException, IllegalAccessException {
        List<Integer> updateIds=new ArrayList<>(0);
        for(T model:models){
            Field field=model.getClass().getDeclaredField(param);
            field.setAccessible(true);
            if(field.get(model)!=null) {
                updateIds.add((Integer) field.get(model));
            }
        }
        List<S> entities = models.stream().filter(Objects::nonNull).map(source -> modelMapper.map(source, sClass))
                .collect(Collectors.toList());

            ids.removeAll(updateIds);
            if(!ids.isEmpty())
            baseDaoInterface.deleteByIdList(sClass, ids, "id");

        if(!entities.isEmpty())
        baseDaoInterface.saveOrUpdateCollection(entities);


    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
