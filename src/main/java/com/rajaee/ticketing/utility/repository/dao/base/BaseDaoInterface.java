package com.rajaee.ticketing.utility.repository.dao.base;

import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@Repository
public interface BaseDaoInterface {

    <T> Integer count(Class<T> cls);

    <T> T exist(Class<T> cls, Map<String, Object> conditions);
    <T> boolean existByCondition(Class<T> cls, Map<String, Object> conditions);

    /* ****************************************************************************************************************** */

    <T> T getById(Class<T> cls, Integer id);

    <T> T getByAndConditions(Class<T> cls, Map<String, Object> conditions);

    <T> T getByAndConditions(Class<T> cls, Map<String, Object> conditions, String[] include);

    <T> T getByOrConditions(Class<T> cls, Map<String, Object> conditions);

    /* ****************************************************************************************************************** */

    <T> List<T> list(Class<T> cls);

    <T> List<T> listByIds(Class<T> cls, Collection<Integer> ids);

    <T> List<T> listByAndConditions(Class<T> cls, Map<String, Object> conditions);

    <T> List<T> listByAndConditions(Class<T> cls, Map<String, Object> conditions, String[] includes);

    <T> List<T> listByOrConditions(Class<T> cls, Map<String, Object> conditions);

    <T> List<T> listByOrAndConditions(Class<T> cls, List<Map<String, Object>> conditions);

    <T> List<T> listByInConditions(Class<T> cls, Map<String, List> conditions);

    <T> List<T> queryHql(String hql, Class<T> cls);

    <T> List<T> queryHql(String hql, Map<String, Object> parameters);

    <T> List<T> queryHql(String hql, Map<String, Object> parameters, Class<T> cls, int maxResultNumber);

    <T> List<T> queryHql(String hql, Map<String, Object> parameters, Class<T> cls, String[] include);

    <T> List<T> queryHql(String hql, Map<String, Object> parameters, Integer pageSize, Integer pageNumber);

    <T> List<T> querySql(String sql, Map<String, Object> parameters);

    <T> List<T> querySql(String sql, Map<String, Object> parameters, Class<T> cls);

    /* ****************************************************************************************************************** */

    void updateHqlQuery(String hql, Map<String, Object> parameters);

    <T> void deleteById(Class<T> cls, Object id);

    <T> void deleteByIdList(Class<T> cls, List<Integer> ids, String idName);

    <T> Boolean deleteByAndConditions(Class<T> cls, Map<String, Object> conditions);

    <T> Boolean updateByAndConditions(Class<T> cls, Map<String, Object> conditions, Map<String, Object> parameters);
    /* ****************************************************************************************************************** */

    <T> void saveOrUpdate(T entity);

    <T> void update(T entity);

    <T> void delete(T entity);

    <T> T saveEntity(T entity);

    <T> T updateEntity(T entity);

    /* ****************************************************************************************************************** */

    <T> void saveCollection(Collection<T> entities);

    <T> void updateCollection(Collection<T> entities);

    <T> void saveOrUpdateCollection(Collection<T> entities);

    void flush();

    <T> void detach(T object);

}
