package com.rajaee.ticketing.utility.repository.dao.base;

import com.rajaee.ticketing.utility.bl.ValidationEngine;
import com.github.jknack.handlebars.internal.lang3.ArrayUtils;
import com.google.common.collect.Iterables;
import org.hibernate.IdentifierLoadAccess;
import org.hibernate.MultiIdentifierLoadAccess;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@Repository
@Transactional
public class BaseDaoImplementation implements BaseDaoInterface {

    /**
     * The Batch Size for Flush a Batch of Inserts and Release Memory
     * 20, Same as the JDBC Batch Size
     */
    private static final int BATCH_SIZE = 20;
    private static String FETCH_GRAPH = "javax.persistence.fetchgraph";

    /**
     * The {@link SessionFactory} Instance Representing Database Session Factory
     */
    @PersistenceContext
    private EntityManager entityManager;

    public Session getSession() {
        return entityManager.unwrap(Session.class);
    }
    /* ****************************************************************************************************************** */

    @Override
    public <T> Integer count(Class<T> cls) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<T> rootQuery = criteriaQuery.from(cls);

        criteriaQuery.select(criteriaBuilder.count(rootQuery));
        return entityManager.createQuery(criteriaQuery).getSingleResult().intValue();
    }

    @Override
    public <T> T exist(Class<T> cls, Map<String, Object> conditions) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : conditions.entrySet())
            predicates.add(criteriaBuilder.equal(rootQuery.get(pair.getKey()), pair.getValue()));

        criteriaQuery.select(rootQuery);
        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[]{})));
        List<T> result = entityManager.createQuery(criteriaQuery)
                .setFirstResult(0)
                .setMaxResults(1)
                .getResultList();
        return !result.isEmpty() ? result.get(0) : null;
    }

    @Override
    public <T> boolean existByCondition(Class<T> cls, Map<String, Object> conditions) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : conditions.entrySet())
            predicates.add(criteriaBuilder.equal(rootQuery.get(pair.getKey()), pair.getValue()));

        criteriaQuery.select(rootQuery);
        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[]{})));
        return entityManager.createQuery(criteriaQuery)
                .setFirstResult(0)
                .setMaxResults(1) != null;
    }
    /* ****************************************************************************************************************** */

    @Override
    public <T> T getById(Class<T> cls, Integer id) {
        IdentifierLoadAccess<T> identifierLoadAccess = getSession().byId(cls);
        return identifierLoadAccess.load(id);
    }

    @Override
    public <T> T getByAndConditions(Class<T> cls, Map<String, Object> conditions) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : conditions.entrySet())
            predicates.add(criteriaBuilder.equal(rootQuery.get(pair.getKey()), pair.getValue()));

        criteriaQuery.select(rootQuery);
        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[]{})));
        List<T> result = entityManager.createQuery(criteriaQuery).getResultList();
        return !result.isEmpty() ? result.get(0) : null;
    }

    @Override
    public <T> T getByAndConditions(Class<T> cls, Map<String, Object> conditions, String[] include) {
        CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : conditions.entrySet())
            predicates.add(criteriaBuilder.equal(rootQuery.get(pair.getKey()), pair.getValue()));

        EntityGraph eg = entityManager.createEntityGraph(cls);
        List<String> columns = ValidationEngine.fieldNames(cls);
        addAttributesToEntityGraph(eg, include, columns);

        criteriaQuery.select(rootQuery);
        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[]{})));
        List<T> result = entityManager
                .createQuery(criteriaQuery)
                .setHint(FETCH_GRAPH, eg)
                .getResultList();
        return !result.isEmpty() ? result.get(0) : null;
    }

    @Override
    public <T> T getByOrConditions(Class<T> cls, Map<String, Object> conditions) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : conditions.entrySet())
            predicates.add(criteriaBuilder.equal(rootQuery.get(pair.getKey()), pair.getValue()));

        criteriaQuery.select(rootQuery);
        criteriaQuery.where(criteriaBuilder.or(predicates.toArray(new Predicate[]{})));
        List<T> result = entityManager.createQuery(criteriaQuery).getResultList();
        return !result.isEmpty() ? result.get(0) : null;
    }

    /* ****************************************************************************************************************** */

    @Override
    public <T> List<T> list(Class<T> cls) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        criteriaQuery.select(rootQuery);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public <T> List<T> listByIds(Class<T> cls, Collection<Integer> ids) {
        MultiIdentifierLoadAccess<T> multiIdentifierLoadAccess = entityManager.unwrap(Session.class).byMultipleIds(cls);
        return multiIdentifierLoadAccess.multiLoad(Iterables.toArray(ids, Integer.class));
    }

    @Override
    public <T> List<T> listByAndConditions(Class<T> cls, Map<String, Object> conditions) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : conditions.entrySet())
            predicates.add(criteriaBuilder.equal(rootQuery.get(pair.getKey()), pair.getValue()));

        criteriaQuery.select(rootQuery);
        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[]{})));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public <T> List<T> listByAndConditions(Class<T> cls, Map<String, Object> conditions, String[] includes) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : conditions.entrySet())
            predicates.add(criteriaBuilder.equal(rootQuery.get(pair.getKey()), pair.getValue()));

        criteriaQuery.select(rootQuery);
        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[]{})));
        EntityGraph eg = entityManager.createEntityGraph(cls);
        List<String> columns = ValidationEngine.fieldNames(cls);
        List<String> finalIncludes = columns.stream().filter(item -> ArrayUtils.contains(includes, item)).collect(Collectors.toList());

        for (String property : finalIncludes) {
            eg.addAttributeNodes(property);
        }
        return entityManager
                .createQuery(criteriaQuery)
                .setHint(FETCH_GRAPH, eg)
                .getResultList();
    }

    @Override
    public <T> List<T> listByOrConditions(Class<T> cls, Map<String, Object> conditions) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : conditions.entrySet())
            predicates.add(criteriaBuilder.equal(rootQuery.get(pair.getKey()), pair.getValue()));

        criteriaQuery.select(rootQuery);
        criteriaQuery.where(criteriaBuilder.or(predicates.toArray(new Predicate[]{})));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public <T> List<T> listByOrAndConditions(Class<T> cls, List<Map<String, Object>> conditions) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> orPredicates = new ArrayList<>();

        for (Map<String, Object> eachCondition : conditions) {
            List<Predicate> predicates = new ArrayList<>();
            for (Map.Entry<String, Object> pair : eachCondition.entrySet())
                predicates.add(criteriaBuilder.equal(rootQuery.get(pair.getKey()), pair.getValue()));
            orPredicates.add(criteriaBuilder.and(predicates.toArray(new Predicate[]{})));
        }

        criteriaQuery.select(rootQuery);
        criteriaQuery.where(criteriaBuilder.or(orPredicates.toArray(new Predicate[]{})));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public <T> List<T> listByInConditions(Class<T> cls, Map<String, List> conditions) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, List> pair : conditions.entrySet()) {
            Expression<String> parentExpression = rootQuery.get(pair.getKey());
            predicates.add(parentExpression.in(pair.getValue()));
        }

        criteriaQuery.select(rootQuery);
        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[]{})));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }


    @Override
    public <T> List<T> queryHql(String hql, Class<T> cls) {
        TypedQuery<T> query = entityManager.createQuery(hql, cls);
        return query.getResultList();
    }

    @Override
    public <T> List<T> queryHql(String hql, Map<String, Object> parameters) {
        Query query = entityManager.createQuery(hql);

        for (Map.Entry<String, Object> pair : parameters.entrySet())
            query.setParameter(pair.getKey(), pair.getValue());
        return query.getResultList();
    }

    @Override
    public <T> List<T> queryHql(String hql, Map<String, Object> parameters, Class<T> cls, String[] include) {
        TypedQuery<T> query = entityManager.createQuery(hql, cls);

        EntityGraph eg = entityManager.createEntityGraph(cls);
        List<String> columns = ValidationEngine.fieldNames(cls);
        List<String> finalIncludes = columns.stream().filter(item -> ArrayUtils.contains(include, item)).collect(Collectors.toList());

        for (String property : finalIncludes) {
            eg.addAttributeNodes(property);
        }
        for (Map.Entry<String, Object> pair : parameters.entrySet())
            query.setParameter(pair.getKey(), pair.getValue());
        return query.setHint(FETCH_GRAPH, eg)
                .getResultList();
    }

    @Override
    public <T> List<T> queryHql(String hql, Map<String, Object> parameters, Class<T> cls, int maxResultNumber) {
        TypedQuery<T> query = entityManager.createQuery(hql, cls);

        for (Map.Entry<String, Object> pair : parameters.entrySet())
            query.setParameter(pair.getKey(), pair.getValue());
        query.setMaxResults(maxResultNumber);
        return query.getResultList();
    }

    @Override
    public <T> List<T> queryHql(String hql, Map<String, Object> parameters, Integer pageSize, Integer pageNumber) {
        if (pageNumber < 1) pageNumber = 1;
        Query query = entityManager.createQuery(hql);
        query.setFirstResult((pageNumber - 1) * pageSize);
        query.setMaxResults(pageSize);

        for (Map.Entry<String, Object> pair : parameters.entrySet())
            query.setParameter(pair.getKey(), pair.getValue());
        return query.getResultList();
    }

    @Override
    public <T> List<T> querySql(String sql, Map<String, Object> parameters) {
        Query query = entityManager.createNativeQuery(sql);
        for (Map.Entry<String, Object> pair : parameters.entrySet())
            query.setParameter(pair.getKey(), pair.getValue());
        return query.getResultList();
    }

    @Override
    public <T> List<T> querySql(String sql, Map<String, Object> parameters, Class<T> cls) {
        Query query = entityManager.createNativeQuery(sql, cls);

        for (Map.Entry<String, Object> pair : parameters.entrySet())
            query.setParameter(pair.getKey(), pair.getValue());
        return query.getResultList();
    }

    /* ****************************************************************************************************************** */
    @Override
    public void updateHqlQuery(String hql, Map<String, Object> parameters) {
        Query query = entityManager.createQuery(hql);

        for (Map.Entry<String, Object> pair : parameters.entrySet())
            query.setParameter(pair.getKey(), pair.getValue());
        query.executeUpdate();
    }


    @Override
    public <T> void deleteById(Class<T> cls, Object id) {
        IdentifierLoadAccess<T> identifierLoadAccess = getSession().byId(cls);
        T entity = identifierLoadAccess.load((Serializable) id);
        if (entity != null) {
            getSession().delete(entity);
        }
    }

    @Override
    public <T> void deleteByIdList(Class<T> cls, List<Integer> ids, String idName) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaDelete<T> criteriaDelete = criteriaBuilder.createCriteriaDelete(cls);
        Root<T> rootDelete = criteriaDelete.from(cls);

        criteriaDelete.where(rootDelete.get(idName).in(ids));
        entityManager.createQuery(criteriaDelete).executeUpdate();
    }

    @Override
    public <T> Boolean deleteByAndConditions(Class<T> cls, Map<String, Object> conditions) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaDelete<T> criteriaDelete = criteriaBuilder.createCriteriaDelete(cls);
        Root<T> rootDelete = criteriaDelete.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : conditions.entrySet())
            predicates.add(criteriaBuilder.equal(rootDelete.get(pair.getKey()), pair.getValue()));

        criteriaDelete.where(criteriaBuilder.and(predicates.toArray(new Predicate[]{})));
        int count = entityManager.createQuery(criteriaDelete).executeUpdate();
        return count > 0;
    }

    @Override
    public <T> Boolean updateByAndConditions(Class<T> cls, Map<String, Object> conditions, Map<String, Object> parameters) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaUpdate<T> criteriaUpdate = criteriaBuilder.createCriteriaUpdate(cls);
        Root<T> rootUpdate = criteriaUpdate.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : conditions.entrySet())
            predicates.add(criteriaBuilder.equal(rootUpdate.get(pair.getKey()), pair.getValue()));

        for (Map.Entry<String, Object> pair : parameters.entrySet())
            criteriaUpdate.set(pair.getKey(), pair.getValue());
        criteriaUpdate.where(criteriaBuilder.and(predicates.toArray(new Predicate[]{})));
        int count = entityManager.createQuery(criteriaUpdate).executeUpdate();
        return count > 0;
    }

    /* ****************************************************************************************************************** */

    @Override
    public <T> void saveOrUpdate(T entity) {
        getSession().saveOrUpdate(entity);
    }

    @Override
    public <T> void update(T entity) {
        getSession().update(entity);
    }

    @Override
    public <T> void delete(T entity) {
        getSession().delete(entity);
    }

    @Override
    public <T> T saveEntity(T entity) {
        getSession().save(entity);
        return entity;
    }

    @Override
    public <T> T updateEntity(T entity) {
        getSession().update(entity);
        return entity;
    }

    /* ****************************************************************************************************************** */

    @Override
    public <T> void saveCollection(Collection<T> entities) {
        int i = 0;
        for (T entity : entities) {
            getSession().save(entity);
            if (i % BATCH_SIZE == 0) {
                entityManager.flush();
                entityManager.clear();
            }
            i++;
        }
    }

    @Override
    public <T> void updateCollection(Collection<T> entities) {
        int i = 0;
        for (T entity : entities) {
            getSession().update(entity);
            if (i % BATCH_SIZE == 0) {
                entityManager.flush();
                entityManager.clear();
            }
            i++;
        }
    }

    @Override
    public <T> void saveOrUpdateCollection(Collection<T> entities) {
        int i = 0;
        for (T entity : entities) {
            getSession().saveOrUpdate(entity);
            if (i % BATCH_SIZE == 0) {
                entityManager.flush();
                entityManager.clear();
            }
            i++;
        }
    }


    @Override
    public void flush() {
        entityManager.flush();
        entityManager.clear();
    }

    @Override
    public <T> void detach(T object) {
        entityManager.detach(object);
    }

    static void addAttributesToEntityGraph(EntityGraph entityGraph, String[] include, List<String> columns) {
        Map<String, Subgraph> subGraphs = new HashMap<>();
        if (include != null) {
            for (String property : include) {
                if (property != null) {
                    if (columns.contains(property)) {
                        entityGraph.addAttributeNodes(property);
                    } else {
                        String[] subGraphSplit = property.split("\\.");
                        if (subGraphSplit.length > 1) {
                            Subgraph subGraph;
                            if (subGraphs.containsKey(subGraphSplit[0])) {
                                subGraph = subGraphs.get(subGraphSplit[0]);
                            } else {
                                subGraph = entityGraph.addSubgraph(subGraphSplit[0]);
                                subGraphs.put(subGraphSplit[0], subGraph);
                            }
                            for (int i = 1; i < subGraphSplit.length - 1; i++) {
                                subGraph = subGraph.addSubgraph(subGraphSplit[i]);
                            }
                            subGraph.addAttributeNodes(subGraphSplit[subGraphSplit.length - 1].split(","));
                        }
                    }
                }
            }
        }
    }


}
