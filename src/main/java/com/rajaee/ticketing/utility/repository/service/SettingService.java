package com.rajaee.ticketing.utility.repository.service;

import com.rajaee.ticketing.utility.config.property.ApplicationProperties;
import com.rajaee.ticketing.utility.config.property.identity.*;
import com.rajaee.ticketing.utility.model.dto.SettingDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SettingService {
    private final ModelMapper modelMapper;
    private final ApplicationProperties applicationProperties;

    @Autowired
    public SettingService(ModelMapper modelMapper, ApplicationProperties applicationProperties) {
        this.modelMapper = modelMapper;
        this.applicationProperties = applicationProperties;
    }

    public SettingDto getApplicationSetting(String[] include) {
        SettingDto settingDto = new SettingDto();
        if (include == null) {
            return settingDto;
        }
        for (String settingInclude : include) {
            switch (settingInclude) {
                case "signIn":
                    if (settingDto.getIdentitySettings() == null) {
                        settingDto.setIdentitySettings(new IdentitySettings());
                    }
                    settingDto.getIdentitySettings().setSignIn(modelMapper.map(applicationProperties.getIdentitySettings().getSignIn(), SignInOptions.class));
                    break;
                case "profile":
                    if (settingDto.getIdentitySettings() == null) {
                        settingDto.setIdentitySettings(new IdentitySettings());
                    }
                    settingDto.getIdentitySettings().setProfile(modelMapper.map(applicationProperties.getIdentitySettings().getProfile(), ProfileOptions.class));
                    settingDto.getIdentitySettings().getProfile().validateDetails();
                    break;
                case "registration":
                    if (settingDto.getIdentitySettings() == null) {
                        settingDto.setIdentitySettings(new IdentitySettings());
                    }
                    settingDto.getIdentitySettings().setRegistration(modelMapper.map(applicationProperties.getIdentitySettings().getRegistration(), RegistrationOptions.class));
                    settingDto.getIdentitySettings().getRegistration().validateDetails();
                    break;
                case "password":
                    if (settingDto.getIdentitySettings() == null) {
                        settingDto.setIdentitySettings(new IdentitySettings());
                    }
                    settingDto.getIdentitySettings().setPassword(modelMapper.map(applicationProperties.getIdentitySettings().getPassword(), PasswordOptions.class));
                    break;
                default:
                    break;
            }
        }
        return settingDto;
    }
}
