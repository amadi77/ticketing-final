package com.rajaee.ticketing.utility.repository.service;

import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.repository.sp.ResetSP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The Database Service Class,
 * Containing Methods about Database
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Service
public class DatabaseResetService {

    private final ResetSP resetSP;

    @Autowired
    public DatabaseResetService(ResetSP resetSP) {
        this.resetSP = resetSP;
    }

    /**
     * Reset Super User Permissions to All Permissions
     * <br />Possible {@link com.rajaee.ticketing.utility.model.object.SystemException} in Function Call of {@link ResetSP#superAdminPermission()}
     */
    public void superAdminPermission() throws SystemException {
        this.resetSP.superAdminPermission();
    }
}
