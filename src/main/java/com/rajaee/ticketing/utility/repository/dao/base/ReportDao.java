package com.rajaee.ticketing.utility.repository.dao.base;

import com.rajaee.ticketing.utility.model.object.report.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReportDao {
    private final ReportDaoInterface reportDaoInterface;

    @Autowired
    public ReportDao(ReportDaoInterface reportDaoInterface) {
        this.reportDaoInterface = reportDaoInterface;
    }

    /* ****************************************************************************************************************** */

    public <T> int countEntity(Class<T> resultCls, ReportOption reportOption, ReportCondition reportCondition){
        return this.reportDaoInterface.countEntity(resultCls, reportOption, reportCondition);
    }

    public <T> List<T> reportEntityWithInclude(Class<T> resultCls, ReportOption reportOption, ReportCondition reportCondition, String[] include) {
        return this.reportDaoInterface.reportEntityWithInclude(resultCls, reportOption, reportCondition, include);
    }

    public <T> List<T> reportHqlWithIncludeSortOptionsJoin(Class<T> resultCls, ReportOption reportOption, ReportCondition reportCondition, String[] include) {
        return this.reportDaoInterface.reportEntityWithIncludeJoin(resultCls, reportOption, reportCondition, include);
    }

    public <T> List<T> reportEntityWithIncludeSortOptionsJoin(Class<T> resultCls, ReportOption reportOption, ReportCondition reportCondition, String[] include) {
        return this.reportDaoInterface.reportEntityWithIncludeJoin(resultCls, reportOption, reportCondition, include);
    }

    public <T, T1> ReportResult<T, T1> reportWithCountEntity(Class<T> resultCls, ReportOption reportOption, ReportCondition reportCondition) {
        Integer countAll = this.reportDaoInterface.countEntity(resultCls, reportOption, reportCondition);
        List<T> result = this.reportDaoInterface.reportEntity(resultCls, reportOption, reportCondition);
        return new ReportResult<>(countAll, result);
    }

    public <T, T1> ReportResult<T, T1> reportComplex(Class<T> resultCls, ReportOption reportOption, ReportQuery reportQuery) {
        Integer countAll = this.reportDaoInterface.countComplex(resultCls, reportOption, reportQuery);
        List<T> result = this.reportDaoInterface.reportComplex(resultCls, reportOption, reportQuery);
        return new ReportResult<>(countAll, result);
    }

    public <T, T1> ReportResult<T, T1> reportComplex(Class<T> resultCls, ReportOption reportOption, ReportQuery reportQuery,
                                                                                 Class<T1> aggregationCls, ReportAggregation reportAggregation) {
        Integer countAll = this.reportDaoInterface.countComplex(resultCls, reportOption, reportQuery);
        List<T> result = this.reportDaoInterface.reportComplex(resultCls, reportOption, reportQuery);
        T1 aggregation = this.reportDaoInterface.aggregateComplex(aggregationCls, reportOption, reportQuery, reportAggregation);
        return new ReportResult<>(countAll, result, aggregation);
    }
}
