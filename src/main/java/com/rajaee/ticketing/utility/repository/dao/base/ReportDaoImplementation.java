package com.rajaee.ticketing.utility.repository.dao.base;

import com.rajaee.ticketing.utility.bl.ValidationEngine;
import com.rajaee.ticketing.utility.model.dto.ConditionParameters;
import com.rajaee.ticketing.utility.model.object.SortOption;
import com.rajaee.ticketing.utility.model.object.report.*;
import com.rajaee.ticketing.utility.statics.enums.SortType;
import com.github.jknack.handlebars.internal.lang3.ArrayUtils;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.rajaee.ticketing.utility.repository.dao.base.BaseDaoImplementation.addAttributesToEntityGraph;

@Repository
@Transactional
public class ReportDaoImplementation implements ReportDaoInterface {

    /**
     * The {@link SessionFactory} Instance Representing Database Session Factory
     */
    @PersistenceContext
    private EntityManager entityManager;

    private Integer stateCount = 1;
    private Integer stateAggregation = 2;
    private Integer stateSelect = 3;

    /* ****************************************************************************************************************** */

    @Override
    public <T> Integer countEntity(Class<T> cls, ReportOption reportOption, ReportCondition reportCondition) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<T> rootQuery = criteriaQuery.from(cls);

        if (reportOption.isDistinct()) {
            criteriaQuery.select(criteriaBuilder.countDistinct(rootQuery));
        } else {
            criteriaQuery.select(criteriaBuilder.count(rootQuery));
        }
        Predicate predicate = this.generateCriteriaPredicate(false, reportCondition, criteriaBuilder, rootQuery, true);
        if (predicate != null)
            criteriaQuery.where(predicate);

        return entityManager.createQuery(criteriaQuery).getSingleResult().intValue();
    }

    @Override
    public <T> List<T> reportEntity(Class<T> cls, ReportOption reportOption, ReportCondition reportCondition) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        criteriaQuery.select(rootQuery);
        Predicate predicate = this.generateCriteriaPredicate(true, reportCondition, criteriaBuilder, rootQuery, true);
        if (predicate != null)
            criteriaQuery.where(predicate);

        if (!reportOption.getSortOptions().isEmpty()) {
            List<Order> sorts = new ArrayList<>();
            for (SortOption item : reportOption.getSortOptions()) {
                if (item.getType() == SortType.ASCENDING)
                    sorts.add(criteriaBuilder.asc(rootQuery.get(item.getColumn())));
                else
                    sorts.add(criteriaBuilder.desc(rootQuery.get(item.getColumn())));
            }
            criteriaQuery.orderBy(sorts);
        }

        Query query = entityManager.createQuery(criteriaQuery);
        query.setFirstResult((reportOption.getPageNumber() - 1) * reportOption.getPageSize())
                .setMaxResults(reportOption.getPageSize());

        return query.getResultList();
    }

    @Override
    public <T> List<T> reportEntityWithInclude(Class<T> cls, ReportOption reportOption, ReportCondition reportCondition, String[] include) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        criteriaQuery.select(rootQuery);
        Predicate predicate = this.generateCriteriaPredicate(true, reportCondition, criteriaBuilder, rootQuery, true);
        if (predicate != null)
            criteriaQuery.where(predicate);

        EntityGraph eg = entityManager.createEntityGraph(cls);
        List<String> columns = ValidationEngine.fieldNames(cls);
        List<String> finalIncludes = columns.stream().filter(item -> ArrayUtils.contains(include, item)).collect(Collectors.toList());

        for (String property : finalIncludes) {
            eg.addAttributeNodes(property);
        }

        if (!reportOption.getSortOptions().isEmpty()) {
            List<Order> sorts = new ArrayList<>();
            for (SortOption item : reportOption.getSortOptions()) {
                if (item.getType() == SortType.ASCENDING)
                    sorts.add(criteriaBuilder.asc(rootQuery.get(item.getColumn())));
                else
                    sorts.add(criteriaBuilder.desc(rootQuery.get(item.getColumn())));
            }
            criteriaQuery.orderBy(sorts);
        }

        TypedQuery<T> query = entityManager.createQuery(criteriaQuery.distinct(reportOption.isDistinct()));
        query.setFirstResult((reportOption.getPageNumber() - 1) * reportOption.getPageSize())
                .setMaxResults(reportOption.getPageSize());

        return query.setHint("javax.persistence.fetchgraph", eg)
                .getResultList();
    }

    @Override
    public <T> Integer countComplex(Class<T> cls, ReportOption reportOption, ReportQuery reportQuery) {
        String sql = this.generateSqlQuery(reportQuery, reportOption.getSortOptions(), this.stateCount, null);

        Query query = entityManager.createNativeQuery(sql);

        for (Map.Entry<String, Object> pair : reportQuery.getWhereParameters().entrySet())
            query.setParameter(pair.getKey(), pair.getValue());
//        for (Map.Entry<String, Object> pair : reportQuery.getHavingParameters().entrySet())
//            query.setParameter(pair.getKey(), pair.getValue());

        return (Integer) query.getSingleResult();
    }

    @Override
    public <T> List<T> reportComplex(Class<T> cls, ReportOption reportOption, ReportQuery reportQuery) {
        String sql = this.generateSqlQuery(reportQuery, reportOption.getSortOptions(), this.stateSelect, null) + " ";

        Query query = entityManager.createNativeQuery(sql, cls);

        for (Map.Entry<String, Object> pair : reportQuery.getWhereParameters().entrySet())
            query.setParameter(pair.getKey(), pair.getValue());
//        for (Map.Entry<String, Object> pair : reportQuery.getHavingParameters().entrySet())
//            query.setParameter(pair.getKey(), pair.getValue());

        query.setFirstResult((reportOption.getPageNumber() - 1) * reportOption.getPageSize())
                .setMaxResults(reportOption.getPageSize());

        return query.getResultList();
    }

    @Override
    public <T> T aggregateComplex(Class<T> cls, ReportOption reportOption, ReportQuery reportQuery,
                                  ReportAggregation reportAggregation) {
        String sql = this.generateSqlQuery(reportQuery, reportOption.getSortOptions(), this.stateAggregation, reportAggregation) + " ";

        Query query = entityManager.createNativeQuery(sql, cls);

        for (Map.Entry<String, Object> pair : reportQuery.getWhereParameters().entrySet())
            query.setParameter(pair.getKey(), pair.getValue());
//        for (Map.Entry<String, Object> pair : reportQuery.getHavingParameters().entrySet())
//            query.setParameter(pair.getKey(), pair.getValue());

        query.setFirstResult((reportOption.getPageNumber() - 1) * reportOption.getPageSize())
                .setMaxResults(reportOption.getPageSize());

        List<T> result = query.getResultList();
        return !result.isEmpty() ? result.get(0) : null;
    }


    @Override
    public <T> List<T> reportEntityWithIncludeJoin(Class<T> cls, ReportOption reportOption, ReportCondition reportCondition, String[] include) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);
        criteriaQuery.select(rootQuery);
        Predicate predicate = this.generateCriteriaPredicate(true, reportCondition, criteriaBuilder, rootQuery, true);
        if (predicate != null) {
            criteriaQuery.where(predicate);
        }
        EntityGraph eg = entityManager.createEntityGraph(cls);
        List<String> columns = ValidationEngine.fieldNames(cls);

        addAttributesToEntityGraph(eg, include, columns);
        createSortExpression(criteriaQuery, rootQuery, criteriaBuilder, reportOption);
        TypedQuery<T> query = entityManager.createQuery(criteriaQuery.distinct(reportOption.isDistinct()));
        if (reportOption.getPageNumber() != null && reportOption.getPageNumber() > 1) {
            query.setFirstResult((reportOption.getPageNumber() - 1) * reportOption.getPageSize());
        }
        if (reportOption.getPageSize() != null && reportOption.getPageSize() > 1) {
            query.setMaxResults(reportOption.getPageSize());
        }
        return query
                .setHint("javax.persistence.fetchgraph", eg)
                .getResultList();
    }

    @Override
    public <T> List<T> reportHqlWithIncludeJoin(Class<T> cls, String hql, ReportOption reportOption, Map<String, Object> parameters, String[] include) {
        Query query = entityManager.createQuery(hql);
        EntityGraph eg = entityManager.createEntityGraph(cls);
        List<String> columns = ValidationEngine.fieldNames(cls);

        for (Map.Entry<String, Object> pair : parameters.entrySet())
            query.setParameter(pair.getKey(), pair.getValue());
        addAttributesToEntityGraph(eg, include, columns);
        query.setFirstResult((reportOption.getPageNumber() - 1) * reportOption.getPageSize())
                .setMaxResults(reportOption.getPageSize());
        return query.setHint("javax.persistence.fetchgraph", eg)
                .getResultList();
    }

    private void createSortExpression(CriteriaQuery criteriaQuery, From rootQuery, CriteriaBuilder criteriaBuilder, ReportOption reportOption) {
        List<Order> sorts = new ArrayList<>();
        List<SortOption> lstSortOptions = reportOption.getSortOptions();
        if (lstSortOptions != null && !lstSortOptions.isEmpty()) {
            criteriaQuery.orderBy(addSorts(rootQuery, criteriaBuilder, lstSortOptions, sorts));
        }
    }

    private List<Order> addSorts(From rootQuery, CriteriaBuilder criteriaBuilder, List<SortOption> lstSortOptions, List<Order> sorts) {

        for (SortOption sortOption : lstSortOptions) {
            String[] sortColumns = sortOption.getColumn().split("\\.");
            if (sortColumns.length > 1) {
                try {
                    int[] splitNumber = {0};
                    From chainJoin = setJoinPath(rootQuery, sortColumns, JoinType.LEFT, splitNumber);
                    Path pathJoin = chainJoin.get(sortColumns[splitNumber[0]]);

                    if (sortOption.getType() == SortType.ASCENDING) {
                        sorts.add(criteriaBuilder.asc(pathJoin));
                    } else {
                        sorts.add(criteriaBuilder.desc(pathJoin));
                    }
                } catch (IllegalArgumentException ignored) {
                    /** handle invalid entity and column name
                     * can't throw exception because exception must add to method
                     * and must add it to all getAllEntities in project
                     */
                }
            } else {
                if (sortOption.getType() == SortType.ASCENDING) {
                    sorts.add(criteriaBuilder.asc(rootQuery.get(sortOption.getColumn())));
                } else {
                    sorts.add(criteriaBuilder.desc(rootQuery.get(sortOption.getColumn())));
                }
            }
        }
        return sorts;
    }

    private From setJoinPath(From query, String[] sortColumns, JoinType joinType, int[] splitNumber) {
        From chainJoin = query;
        while (splitNumber[0] < sortColumns.length - 1) {
            chainJoin = getJoin(query, sortColumns, joinType, splitNumber);
            splitNumber[0] = splitNumber[0] + 1;
        }
        return chainJoin;
    }

    private From getJoin(From query, String[] sortColumns, JoinType joinType, int[] splitNumber) {
        List<Join> prevJoins = new ArrayList(query.getJoins());
        From chainJoin = null;
        for (Join currentJoin : prevJoins) {
            if (sortColumns[splitNumber[0]].equals(currentJoin.getAttribute().getName()) &&
                    joinType.equals(currentJoin.getJoinType())) {
                chainJoin = currentJoin;
                break;
            }
        }
        if (chainJoin == null) {
            chainJoin = query.join(sortColumns[splitNumber[0]], joinType);
        }
        return chainJoin;
    }

    /* ************************************************************************************************************** */

    private Predicate generateCriteriaPredicate(boolean fetch, ReportCondition reportCondition,
                                                CriteriaBuilder criteriaBuilder, Root<?> rootQuery, boolean generateAndPredicates) {
        List<Predicate> mainPredicates = new ArrayList<>();
        for (String item : reportCondition.getNullCondition())
            mainPredicates.add(criteriaBuilder.isNull(rootQuery.get(item)));
        for (String item : reportCondition.getNotNullCondition())
            mainPredicates.add(criteriaBuilder.isNotNull(rootQuery.get(item)));

        for (ConditionParameters pair : reportCondition.getEqualCondition()) {
            if (pair.getValue() != null) {
                mainPredicates.add(criteriaBuilder.equal(rootQuery.get(pair.getKey()), pair.getValue()));
            }
        }
        for (ConditionParameters pair : reportCondition.getNotEqualCondition()) {
            if (pair.getValue() != null) {
                mainPredicates.add(criteriaBuilder.notEqual(rootQuery.get(pair.getKey()), pair.getValue()));
            }
        }

        for (ConditionParameters parameters : reportCondition.getLikeCondition()) {
            if (parameters.getValue() != null) {
                mainPredicates.add(criteriaBuilder.like(rootQuery.get(parameters.getKey()), "%" + parameters.getValue() + "%"));
            }
        }

        for (ConditionParameters pair : reportCondition.getInCondition()) {
            if (pair.getValue() != null) {
                mainPredicates.add(rootQuery.get(pair.getKey()).in(pair.getValue()));
            }
        }

        for (ConditionParameters pair : reportCondition.getMinNumberCondition()) {
            if (pair.getValue() != null) {
                mainPredicates.add(criteriaBuilder.ge(rootQuery.get(pair.getKey()), (Number) pair.getValue()));
            }
        }
        for (ConditionParameters pair : reportCondition.getMaxNumberCondition()) {
            if (pair.getValue() != null) {
                mainPredicates.add(criteriaBuilder.le(rootQuery.get(pair.getKey()), (Number) pair.getValue()));
            }
        }

        for (ConditionParameters pair : reportCondition.getMinDateCondition()) {
            if (pair.getValue() != null) {
                mainPredicates.add(criteriaBuilder.greaterThanOrEqualTo(rootQuery.get(pair.getKey()), (Date) pair.getValue()));
            }
        }

        for (ConditionParameters pair : reportCondition.getMaxDateCondition()) {
            if (pair.getValue() != null) {
                mainPredicates.add(criteriaBuilder.lessThanOrEqualTo(rootQuery.get(pair.getKey()), (Date) pair.getValue()));
            }
        }

        for (ConditionParameters pair : reportCondition.getMinTimeCondition()) {
            if (pair.getValue() != null) {
                mainPredicates.add(criteriaBuilder.greaterThanOrEqualTo(rootQuery.get(pair.getKey()), (LocalTime) pair.getValue()));
            }
        }

        for (ConditionParameters pair : reportCondition.getMaxTimeCondition()) {
            if (pair.getValue() != null) {
                mainPredicates.add(criteriaBuilder.lessThanOrEqualTo(rootQuery.get(pair.getKey()), (LocalTime) pair.getValue()));
            }
        }

        for (ConditionParameters pair : reportCondition.getMinZonedTimeCondition()) {
            if (pair.getValue() != null) {
                mainPredicates.add(criteriaBuilder.greaterThanOrEqualTo(rootQuery.get(pair.getKey()), (ZonedDateTime) pair.getValue()));
            }
        }

        for (ConditionParameters pair : reportCondition.getMaxZonedTimeCondition()) {
            if (pair.getValue() != null) {
                mainPredicates.add(criteriaBuilder.lessThanOrEqualTo(rootQuery.get(pair.getKey()), (ZonedDateTime) pair.getValue()));
            }
        }

        if (reportCondition.getOrConditions() != null) {
            for (ReportCondition item : reportCondition.getOrConditions()) {
                mainPredicates.add(generateCriteriaPredicate(fetch, item, criteriaBuilder, rootQuery, false));
            }
        }

        List<Predicate> joinPredicates = this.generateJoinPredicate(fetch, reportCondition.getJoinCondition(),
                criteriaBuilder, rootQuery);
        mainPredicates.addAll(joinPredicates);

        Predicate main = null;
        if (!mainPredicates.isEmpty() && generateAndPredicates) {
            main = criteriaBuilder.and(mainPredicates.toArray(new Predicate[]{}));
        } else if (!mainPredicates.isEmpty()) {
            main = criteriaBuilder.or(mainPredicates.toArray(new Predicate[]{}));
        }

        return main;
    }

    private String generateSqlQuery(ReportQuery reportQuery, List<SortOption> sortOptions, Integer state, ReportAggregation reportAggregation) {
        StringBuilder sql = new StringBuilder("SELECT ");
        if (state.equals(this.stateCount))
            sql.append("count(*)");
        else if (state.equals(this.stateAggregation)) {
            String type;
            switch (reportAggregation.getType()) {
                case SUM:
                    type = "SUM(";
                    break;
                case AVG:
                    type = "AVG(";
                    break;
                case MAX:
                    type = "MAX(";
                    break;
                case MIN:
                    type = "MIN(";
                    break;
                default:
                    type = "SUM(";
                    break;
            }
            for (ReportColumn item : reportAggregation.getAggregationColumn()) {
                sql.append(type).append(item.getColumn()).append(") as ").append(item.getAlias()).append(", ");
            }
            sql.delete(sql.length() - 2, sql.length());
        } else
            sql.append(reportQuery.getSelect());
        sql.append(" FROM ").append(reportQuery.getFrom());
        if (!reportQuery.getWhere().equals(""))
            sql.append(" WHERE ").append(reportQuery.getWhere());

//        sql.append(" GROUP BY ");
//        if (reportQuery.getGroup() != null)
//            sql.append(reportQuery.getGroup());
//        if (sql.substring(sql.length() - 10).equals(" GROUP BY "))
//            sql.delete(sql.length() - 10, sql.length());

        if (state.equals(this.stateSelect)) {
            sql.append(" ORDER BY ");
            if (!sortOptions.isEmpty()) {
                for (SortOption item : sortOptions) {
                    if (item.getType() == SortType.ASCENDING)
                        sql.append(item.getColumn()).append(" ASC, ");
                    else
                        sql.append(item.getColumn()).append(" DESC, ");
                }
                sql.delete(sql.length() - 2, sql.length());
            }
//            else if (reportQuery.getOrder() != null)
//                sql.append(reportQuery.getOrder()).append(", ");

            if (sql.substring(sql.length() - 2).equals(", "))
                sql.delete(sql.length() - 2, sql.length());

            if (sql.substring(sql.length() - 10).equals(" ORDER BY "))
                sql.delete(sql.length() - 10, sql.length());
        }

        return sql.toString();
    }

    private List<Predicate> generateJoinPredicate(boolean fetch, List<ReportCriteriaJoinCondition> joins,
                                                  CriteriaBuilder criteriaBuilder, From root) {
        List<Predicate> predicates = new ArrayList<>();
        for (ReportCriteriaJoinCondition x : joins) {
            From join = (fetch && x.isFetch()) ? (Join) root.fetch(x.getName(), x.getJoinType()) : root.join(x.getName(), x.getJoinType());
            for (Map.Entry<String, List<? extends Number>> pair : x.getInCondition().entrySet())
                predicates.add(join.get(pair.getKey()).in(pair.getValue()));


            for (Map.Entry<String, Object> pair : x.getEqualCondition().entrySet()) {
                if (pair.getValue() != null) {
                    predicates.add(criteriaBuilder.equal(join.get(pair.getKey()), pair.getValue()));
                }
            }

            for (Map.Entry<String, Object> pair : x.getLikeCondition().entrySet()) {
                if (pair.getValue() != null) {
                    predicates.add(criteriaBuilder.like(join.get(pair.getKey()), "%" + pair.getValue() + "%"));
                }
            }

            for (Map.Entry<String, List<? extends Number>> pair : x.getInCondition().entrySet()) {
                if (pair.getValue() != null) {
                    predicates.add(join.get(pair.getKey()).in(pair.getValue()));
                }
            }

            for (Map.Entry<String, Date> pair : x.getMinDateCondition().entrySet()) {
                if (pair.getValue() != null) {
                    predicates.add(criteriaBuilder.greaterThanOrEqualTo(join.get(pair.getKey()), pair.getValue()));
                }
            }

            for (Map.Entry<String, Date> pair : x.getMaxDateCondition().entrySet()) {
                if (pair.getValue() != null) {
                    predicates.add(criteriaBuilder.lessThanOrEqualTo(join.get(pair.getKey()), pair.getValue()));
                }
            }

            for (Map.Entry<String, ZonedDateTime> pair : x.getMinZonedTimeCondition().entrySet()) {
                if (pair.getValue() != null) {
                    predicates.add(criteriaBuilder.greaterThanOrEqualTo(join.get(pair.getKey()), pair.getValue()));
                }
            }

            for (Map.Entry<String, ZonedDateTime> pair : x.getMaxZonedTimeCondition().entrySet()) {
                if (pair.getValue() != null) {
                    predicates.add(criteriaBuilder.lessThanOrEqualTo(join.get(pair.getKey()), pair.getValue()));
                }
            }
            List<Predicate> joinPredicates = this.generateJoinPredicate(fetch && x.isFetch(), x.getChildren(),
                    criteriaBuilder, join);
            predicates.addAll(joinPredicates);
        }
        return predicates;
    }

}