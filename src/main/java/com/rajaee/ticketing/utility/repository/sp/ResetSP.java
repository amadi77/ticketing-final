package com.rajaee.ticketing.utility.repository.sp;

import com.rajaee.ticketing.utility.model.object.SystemError;
import com.rajaee.ticketing.utility.model.object.SystemException;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.StoredProcedureQuery;

/**
 * The General Stored Procedure Class,
 * Containing Global Database Operation
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Repository
@Transactional
public class ResetSP {

    @PersistenceContext
    private EntityManager entityManager;

    public Session getSession() {
        Session session = entityManager.unwrap(Session.class);
        return session;
    }

    public void superAdminPermission() throws SystemException {
        StoredProcedureQuery storedProcedure = getSession().createStoredProcedureQuery("super_admin_permission");
        try {
            storedProcedure.execute();
        } catch (PersistenceException e) {
            throw new SystemException(SystemError.STORED_PROCEDURE_FAILED, "session", 1550);
        }
    }
}
