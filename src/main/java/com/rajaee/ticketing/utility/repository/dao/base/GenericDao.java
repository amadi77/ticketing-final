package com.rajaee.ticketing.utility.repository.dao.base;

import com.rajaee.ticketing.utility.model.dto.FilterBase;
import com.rajaee.ticketing.utility.model.object.Pair;
import com.rajaee.ticketing.utility.model.object.SystemError;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.model.object.report.ReportCondition;
import com.rajaee.ticketing.utility.model.object.report.ReportOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author imax on 5/25/19
 */

/**
 * Exceptions error code range: 1201-1250
 */

@Service
public abstract class GenericDao {

    @Autowired
    private ReportDao reportDao;
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    public <T> List<T> getAllEntities(Class<T> cls, FilterBase filterBase, String[] include) {
        return reportDao.reportEntityWithIncludeSortOptionsJoin(cls, filterChain(filterBase).getSecond(), filterChain(filterBase).getFirst(), include);
    }

    public <T> List<T> getAllEntitiesWithFilter(Class<T> cls, Pair<ReportCondition, ReportOption> reportFields, String[] include) {
        return reportDao.reportEntityWithIncludeSortOptionsJoin(cls, reportFields.getSecond(), reportFields.getFirst(), include);
    }

    public <T> List<T> getAllEntitiesJoin(Class<T> cls, FilterBase filterBase, String[] include) {
        return reportDao.reportEntityWithIncludeSortOptionsJoin(cls, filterChain(filterBase).getSecond(), filterChain(filterBase).getFirst(), include);
    }

    public <T> int countEntity(Class<T> cls, FilterBase filterBase) {
        return reportDao.countEntity(cls, filterChain(filterBase).getSecond(), filterChain(filterBase).getFirst());
    }

    public <T> T getEntityById(Class<T> cls, int id, String[] includes) throws SystemException {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        T result = baseDaoInterface.getByAndConditions(cls, map, includes);
        if (result == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, "id:" + id, 1201);
        }
        return result;
    }

    public <T> T getEntityById(Class<T> cls, Object id, String[] includes) throws SystemException {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        T result = baseDaoInterface.getByAndConditions(cls, map, includes);
        if (result == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, "id:" + id, 1201);
        }
        return result;
    }

    public <T> T createEntity(T entity) {
        return baseDaoInterface.saveEntity(entity);
    }

    public <T> T updateEntity(T entity) {
        return baseDaoInterface.updateEntity(entity);
    }

    public <T> void detach(T entity) {
        baseDaoInterface.detach(entity);
    }

    public void flush() {
        baseDaoInterface.flush();
    }

    public <T> void createOrUpdateEntity(T entity) {
        baseDaoInterface.saveOrUpdate(entity);
    }

    public <T> void saveOrUpdateEntityCollection(Collection<T> collection) {
        baseDaoInterface.saveOrUpdateCollection(collection);
    }
    public <T> void saveOrUpdateEntity(T entity) {
        baseDaoInterface.saveOrUpdate(entity);
    }

    public <T> boolean deleteById(Class<T> cls, int id) {
        baseDaoInterface.deleteById(cls, id);
        return true;
    }
    public <T> boolean deleteById(Class<T> cls, Object id) {
        baseDaoInterface.deleteById(cls, id);
        return true;
    }

    public <T> void deleteEntity(T entity) {
        baseDaoInterface.delete(entity);
    }


    public abstract Pair<ReportCondition, ReportOption> filterChain(FilterBase filterBase);

}
