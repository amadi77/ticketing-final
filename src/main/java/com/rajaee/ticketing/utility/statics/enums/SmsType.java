package com.rajaee.ticketing.utility.statics.enums;

public enum SmsType {
    SERVICE,
    ADVERTISING;
}
