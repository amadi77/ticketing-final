package com.rajaee.ticketing.utility.statics.constants;

public abstract class UtilityConstant {

    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static final String ZIP_CODE_PATTERN = "[0-9]{10}";
    public static final String[] DATE_PATTERNS = {"yyyy/MM/dd", "yyyy-MM-dd"};

    public static final String ENGLISH_STRING_PATTERN = "(?=.*[_A-Za-z0-9]).{1,}";
    public static final String PERSIAN_STRING_PATTERN = "(?=.*[\\u0600-\\u06FF\\uFB8A\\u067E\\u0686\\u06AF\\u200C\\u200F ]).{4,}";

    public static final String BASE_APP_FOLDER = "/opt/Beroozresan/app";
    public static final String BASE_APP_URL = "http://185.13.228.98:808/Beroozresan/app";
    public static final String APP_FOLDER_CUSTOMER = "customer";
    public static final String APP_FOLDER_DRIVER = "driver";

    public static final Integer DEFAULT_REPORT_PAGE_SIZE = 10;
    public static final Integer DEFAULT_REPORT_PAGE_NUMBER = 1;
    public static final Integer MINIMUM_REPORT_PAGE_SIZE = 1;
    public static final Integer MAXIMUM_REPORT_PAGE_SIZE = 100;
    public static final Integer MINIMUM_REPORT_PAGE_NUMBER = 1;

    public static final String HASH_FUNCTION = "SHA-256";
    public static final String GOOGLE_API_KEY = "AIzaSyDh3yzsorXDyP-gAlxEGUlzmfJ6nVxizUw";


}
