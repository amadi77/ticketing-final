package com.rajaee.ticketing.utility.statics.enums;

/**
 * @author imax on 8/29/19
 */
public enum  EmailProtocolType {

    SMTP,
    POP3
}
