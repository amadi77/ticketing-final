package com.rajaee.ticketing.utility.statics.enums;

public enum CertificateType {

    LICENSE(1),
    CERTIFICATE(2);

    private final int value;

    CertificateType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
