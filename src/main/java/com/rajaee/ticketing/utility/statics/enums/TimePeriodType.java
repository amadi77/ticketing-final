package com.rajaee.ticketing.utility.statics.enums;

public enum TimePeriodType {
    DAILY,
    WEEKLY,
    MONTHLY,
    YEARLY
}
