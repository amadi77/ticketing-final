package com.rajaee.ticketing.utility.statics.enums;

public enum ChangeStateType {
    ENABLE,
    DISABLE,
    DELETE
}
