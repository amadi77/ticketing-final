package com.rajaee.ticketing.utility.statics.enums;

public enum PrimaryWarehouseType {
    PRIMARY_WAREHOUSE(3),
    ANBARAK_WAREHOUSE(5),
    INTERFACE_WAREHOUSE(6);

    private final Integer value;

    PrimaryWarehouseType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
