package com.rajaee.ticketing.utility.bl;

import com.rajaee.ticketing.utility.model.object.SystemError;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.Arrays;

/**
 * The Normalize Service Class,
 * Containing Methods about Normalizing Input Data
 *
 * @author Omid Sohrabi
 * @version 1.0
 * @since 2019-06-22
 */

/**
 * Exceptions error code range: 1081-1100
 */

public final class NormalizeEngine {

    private static final String INT_ARRAY_STRING_REGEX = "\\[|]|\\s";

    public static boolean mobileCheker(String mobile) {
        String mobileRegex = "(0|\\+98)?([ ]|,|-|[()]){0,2}9[1|2|3|4]([ ]|,|-|[()]){0,2}(?:[0-9]([ ]|,|-|[()]){0,2}){8}";
        return (mobile.matches(mobileRegex));
    }

    public static String getSlug(String value) {
        if (value != null) {
            value = value.trim().toLowerCase();
            String result = value.replaceAll("\\s+\\u200c*|\\u200c+\\s*", "-");
            return normalizePersianString(result);
        } else
            return null;
    }

    public static String trimAndNormalizePersianString(String input) {
        if (input != null) {
            return normalizePersianString(input.trim());
        } else {
            return null;
        }
    }

    public static String convertIntegerArrayToString(Integer[] array) {
        return Arrays.toString(array).replaceAll(INT_ARRAY_STRING_REGEX, "");
    }

    public static boolean nationalCodeChecker(String nationalCode) throws SystemException {
        if (nationalCode == null) {
            throw new SystemException(SystemError.VALIDATION_EXCEPTION, "null", 1081);
        }
        int c, n, r;
        char[] a = nationalCode.toCharArray();
        if (nationalCode.length() == 10) {
            boolean sameChars = nationalCode.chars().allMatch(ch -> ch == nationalCode.charAt(0));
            if (sameChars) {
                throw new SystemException(SystemError.VALIDATION_EXCEPTION, "nationalCode:" + nationalCode, 1082);
            } else {
                c = Integer.parseInt(String.valueOf(a[9]));
                n = Integer.parseInt(String.valueOf(a[0])) * 10 + Integer.parseInt(String.valueOf(a[1])) * 9 + Integer.parseInt(String.valueOf(a[2])) * 8 + Integer.parseInt(String.valueOf(a[3])) * 7 + Integer.parseInt(String.valueOf(a[4])) * 6 + Integer.parseInt(String.valueOf(a[5])) * 5 + Integer.parseInt(String.valueOf(a[6])) * 4 + Integer.parseInt(String.valueOf(a[7])) * 3 + Integer.parseInt(String.valueOf(a[8])) * 2;
                r = n - (n / 11) * 11;
                if ((r == 0 && r == c) || (r == 1 && c == 1) || (r > 1 && c == 11 - r)) {
                    return true;
                } else {
                    throw new SystemException(SystemError.VALIDATION_EXCEPTION, "nationalCode:" + nationalCode, 1083);
                }
            }
        } else {
            throw new SystemException(SystemError.VALIDATION_EXCEPTION, "nationalCode:" + nationalCode, 1084);
        }
    }

    public static String normalizePersianDigits(String number) {
        if (number != null) {
            char[] chars = new char[number.length()];
            for (int i = 0; i < number.length(); i++) {
                char ch = number.charAt(i);
                if (ch >= 0x0660 && ch <= 0x0669)
                    ch -= 0x0660 - '0';
                else if (ch >= 0x06f0 && ch <= 0x06F9)
                    ch -= 0x06f0 - '0';
                chars[i] = ch;
            }
            return new String(chars);
        } else {
            return null;
        }
    }

    public static String getNormalizedPhoneNumber(String mobile) throws SystemException {
        try {
            if (mobile == null) {
                return null;
            }
            mobile = mobile.trim();
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber phoneProto = phoneUtil.parse(mobile, "IR");
            if (phoneUtil.isValidNumber(phoneProto)
                    && phoneUtil.isPossibleNumberForType(phoneProto, PhoneNumberUtil.PhoneNumberType.MOBILE)) {
                return phoneUtil.format(phoneProto, PhoneNumberUtil.PhoneNumberFormat.E164);
            }
            throw new SystemException(SystemError.VALIDATION_EXCEPTION, "mobile:" + mobile, 1085);
        } catch (NumberParseException e) {
            throw new SystemException(SystemError.VALIDATION_EXCEPTION, "mobile:" + mobile, 1086);
        }
    }

    public static String normalizePersianString(String value) {
        if (value == null || value.equals(""))
            return value;
        return value.trim().replace("۰", "0").replace("٠", "0")
                .replace("۱", "1").replace("١", "1")
                .replace("۲", "2").replace("٢", "2")
                .replace("۳", "3").replace("٣", "3")
                .replace("۴", "4").replace("٤", "4")
                .replace("۵", "5").replace("٥", "5")
                .replace("۶", "6").replace("٦", "6")
                .replace("۷", "7").replace("٧", "7")
                .replace("۸", "8").replace("٨", "8")
                .replace("۹", "9").replace("٩", "9")
                .replace("بِ", "ب")
                .replace("زِ", "ز")
                .replace("دِ", "د")
                .replace("ذِِ", "ذ")
                .replace("سِ", "س")
                .replace("شِ", "ش")
                .replace("ك", "ک")
                .replace("ى", "ی")
                .replace("ي", "ی");
    }

}
