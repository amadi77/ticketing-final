package com.rajaee.ticketing.utility.bl.template;

import com.rajaee.ticketing.utility.model.object.SystemException;
import org.springframework.stereotype.Service;

@Service
public interface ITemplateService {
    String render(String templateName, Object model) throws SystemException;
}
