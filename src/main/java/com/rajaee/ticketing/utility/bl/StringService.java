package com.rajaee.ticketing.utility.bl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class StringService {
    private final ObjectMapper objectMapper;

    @Autowired
    private StringService(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public static String toJsonString(Object obj) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    public String toJsonStringAdvanced(Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    public <T> T convertJsonToObject(String json, Class<T> cls) {
        try {
            return objectMapper.readValue(json, cls);
        } catch (IOException e) {
            return null;
        }
    }

    public <T> List<T> convertJsonToObjectList(String json, Class<T> tClass) {
        try {
            CollectionType listType = objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, tClass);
            return objectMapper.readValue(json, listType);
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    public static <T> String convertObjectListToString(List<T> objectList) {
        if (objectList != null && !objectList.isEmpty())
            return objectList.stream().filter(Objects::nonNull).map(Object::toString).collect(Collectors.joining(","));
        return null;
    }

    public static List<Integer> convertStringToNumericArray(String list) {
        if (list != null && !list.equals("")) {
            return Stream.of(list.split(","))
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    public static List<String> convertCommaSeparatedStringToList(String commaSeparated) {
        if (commaSeparated != null && !commaSeparated.equals("")) {
            return Stream.of(commaSeparated.split(","))
                    .collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    public Map<String, Object> objectToMap(Object obj) {
        return objectMapper.convertValue(obj, Map.class);
    }

    public Map<String, Object> stringToMap(String json) {
        try {
            return objectMapper.readValue(json, Map.class);
        } catch (IOException e) {
            return null;
        }
    }

    public <T> T mapToObject(Map<String, Object> map, Class<T> cls) {
        return objectMapper.convertValue(map, cls);
    }


    public static String getAlphaNumericString(int n) {

        // lower limit for LowerCase Letters
        int lowerLimit = 97;

        // lower limit for LowerCase Letters
        int upperLimit = 122;

        Random random = new Random();

        // Create a StringBuffer to store the result
        StringBuffer r = new StringBuffer(n);

        for (int i = 0; i < n; i++) {

            // take a random value between 97 and 122
            int nextRandomChar = lowerLimit
                    + (int) (random.nextFloat()
                    * (upperLimit - lowerLimit + 1));

            // append a character at the end of bs
            r.append((char) nextRandomChar);
        }

        // return the resultant string
        return r.toString();
    }


}
