package com.rajaee.ticketing.utility.file.bl;

import com.rajaee.ticketing.utility.file.model.object.BucketInfo;
import com.rajaee.ticketing.utility.file.model.object.ContainerInfo;
import com.rajaee.ticketing.utility.file.model.object.DownloadResult;
import com.rajaee.ticketing.utility.file.model.object.FileInfo;
import com.rajaee.ticketing.utility.file.statics.enums.FileServiceStatus;
import com.rajaee.ticketing.utility.model.object.SystemException;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;

public interface IFileService {
    Collection<ContainerInfo> getContainers() throws SystemException;

    ContainerInfo getContainer(String containerName) throws SystemException;

    BucketInfo getBucket(String containerName, String bucketName) throws SystemException;

    Collection<FileInfo> getFiles(String containerName, String bucketName) throws SystemException;

    FileServiceStatus createContainer(String containerName);

    FileServiceStatus createBucket(String containerName, String bucketName);

    FileServiceStatus deleteContainer(String containerName);

    FileServiceStatus deleteBucket(String containerName, String bucketName);

    FileServiceStatus deleteFiles(String filePaths);

    FileServiceStatus deleteTempFiles(String filePaths);

    FileServiceStatus deleteFiles(Collection<String> filePaths);

    Collection<String> upload(Collection<MultipartFile> files) throws SystemException;

    String manipulateAttachments(String oldFilePaths, String newFilePaths, String containerName, String bucketName) throws SystemException;

    <T> void manipulateAttachments(T oldModel, T newModel) throws SystemException;

    DownloadResult download(String containerName, String bucketName, String fileName) throws SystemException;

}
