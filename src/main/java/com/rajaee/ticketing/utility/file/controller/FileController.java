package com.rajaee.ticketing.utility.file.controller;

import com.rajaee.ticketing.utility.file.bl.IFileService;
import com.rajaee.ticketing.utility.file.model.object.BucketInfo;
import com.rajaee.ticketing.utility.file.model.object.ContainerInfo;
import com.rajaee.ticketing.utility.file.model.object.FileInfo;
import com.rajaee.ticketing.utility.file.statics.constants.FileRestApi;
import com.rajaee.ticketing.utility.file.statics.enums.FileServiceStatus;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

@Api(tags = {"File"})
@RestController
@RequestMapping(value = RestApi.REST_PUBLIC)
@Validated
public class FileController {

    private final IFileService fileService;

    @Autowired
    public FileController(IFileService fileService) {
        this.fileService = fileService;
    }

    @ApiOperation(value = "Get all containers", response = ContainerInfo.class, responseContainer = "List")
    @GetMapping(path = FileRestApi.FILE_CONTAINERS, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getContainers(HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(fileService.getContainers(), HttpStatus.OK);
    }

    @ApiOperation(value = "Get a container by {name}", response = ContainerInfo.class)
    @GetMapping(path = FileRestApi.FILE_CONTAINERS_NAME, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getContainerByName(@PathVariable("name") String name, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(fileService.getContainer(name), HttpStatus.OK);
    }

    @ApiOperation(value = "Get a bucket by {bucketName} inside a container named {containerName}", response = BucketInfo.class)
    @GetMapping(path = FileRestApi.FILE_BUCKETS, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getBucket(@PathVariable("containerName") String containerName, @PathVariable("bucketName") String bucketName, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(fileService.getBucket(containerName, bucketName), HttpStatus.OK);
    }

    @ApiOperation(value = "Get files inside a bucket named {bucketName} inside a container named {containerName}", response = FileInfo.class, responseContainer = "List")
    @GetMapping(path = FileRestApi.FILE_FILES, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getFiles(@PathVariable("containerName") String containerName, @PathVariable("bucketName") String bucketName, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(fileService.getFiles(containerName, bucketName), HttpStatus.OK);
    }

    @ApiOperation(value = "Delete a temp file", response = FileServiceStatus.class)
    @DeleteMapping(path = FileRestApi.FILE_TEMP_FILES, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deleteFile(@PathVariable("fileName") String fileName, HttpServletRequest request) {
        return new ResponseEntity<>(fileService.deleteTempFiles(fileName), HttpStatus.OK);
    }

    @ApiOperation(value = "Upload  temp files", response = String.class, responseContainer = "List")
    @PostMapping(path = FileRestApi.FILE_TEMP, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity uploadFiles(@RequestBody List<MultipartFile> files, HttpServletRequest request) throws SystemException {
        return new ResponseEntity<>(fileService.upload(files), HttpStatus.OK);
    }

}
