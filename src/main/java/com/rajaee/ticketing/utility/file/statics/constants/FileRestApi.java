package com.rajaee.ticketing.utility.file.statics.constants;

public abstract class FileRestApi {

    //Product Category Controller
    public static final String FILE_CONTAINERS = "/files/containers";
    public static final String FILE_CONTAINERS_NAME = "/files/containers/{name}";
    public static final String FILE_BUCKETS = "/files/containers/{containerName}/buckets/{bucketName}";
    public static final String FILE_FILES = "/files/containers/{containerName}/buckets/{bucketName}/files";
    public static final String FILE_TEMP_FILES = "/files/temp/{fileName:.+}";
    public static final String FILE_TEMP = "/files/temp";

}
