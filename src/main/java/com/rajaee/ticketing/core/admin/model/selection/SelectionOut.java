package com.rajaee.ticketing.core.admin.model.selection;

import com.rajaee.ticketing.core.admin.model.candidate.CandidateOut;
import com.rajaee.ticketing.databse.core.entity.SelectionEntity;
import com.rajaee.ticketing.databse.core.statics.Gender;
import com.rajaee.ticketing.databse.core.statics.Grade;
import com.rajaee.ticketing.utility.config.StaticApplicationContext;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.modelmapper.ModelMapper;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
public class SelectionOut extends SelectionIn {
    private Integer id;
    @Setter(AccessLevel.PRIVATE)
    private List<CandidateOut> candidates;

    public SelectionOut(SelectionEntity entity) {
        if (entity != null) {
            setId(entity.getId());
            if (entity.getCollegeIds() != null)
                setCollegeId(Arrays.stream(entity.getCollegeIds().split(",")).map(Integer::parseInt).collect(Collectors.toList()));
            if (entity.getDormIds() != null)
                setDormId(Arrays.stream(entity.getDormIds().split(",")).map(Integer::parseInt).collect(Collectors.toList()));
            if (entity.getFieldIds() != null)
                setFieldId(Arrays.stream(entity.getFieldIds().split(",")).map(Integer::parseInt).collect(Collectors.toList()));
            if (entity.getInputYears() != null)
                setInputYear(Arrays.stream(entity.getInputYears().split(",")).map(Integer::parseInt).collect(Collectors.toList()));
            if (entity.getDormIds() != null)
                setGrads(Arrays.stream(entity.getDormIds().split(",")).map(Integer::parseInt).map(Grade::getGrade).collect(Collectors.toList()));
            if (entity.getGender() != null)
                setGenders(Arrays.stream(entity.getGender().split(",")).map(Integer::parseInt).map(Gender::getGender).collect(Collectors.toList()));
            if (Hibernate.isInitialized(entity.getCandidates())) {
                candidates = entity.getCandidates().stream().map(CandidateOut::new).collect(Collectors.toList());
            }
        }
    }
}
