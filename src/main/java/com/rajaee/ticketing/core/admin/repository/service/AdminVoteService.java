package com.rajaee.ticketing.core.admin.repository.service;

import com.rajaee.ticketing.core.admin.model.vote.VoteFilter;
import com.rajaee.ticketing.core.admin.model.vote.VoteIn;
import com.rajaee.ticketing.core.admin.repository.dao.VoteDao;
import com.rajaee.ticketing.databse.core.entity.SelectionEntity;
import com.rajaee.ticketing.databse.core.entity.VoteEntity;
import com.rajaee.ticketing.utility.model.dto.FilterBase;
import com.rajaee.ticketing.utility.model.object.Pair;
import com.rajaee.ticketing.utility.model.object.SystemError;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.model.object.report.ReportCondition;
import com.rajaee.ticketing.utility.model.object.report.ReportOption;
import com.rajaee.ticketing.utility.repository.dao.base.GenericDao;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;

@Repository
public class AdminVoteService extends GenericDao {
    private final ModelMapper modelMapper;
    private final VoteDao voteDao;
    private final AdminSelectionService selectionService;

    @Autowired
    public AdminVoteService(ModelMapper modelMapper, VoteDao voteDao, AdminSelectionService selectionService) {
        this.modelMapper = modelMapper;
        this.voteDao = voteDao;
        this.selectionService = selectionService;
    }

    public int count(VoteFilter voteFilter) {
        return countEntity(VoteEntity.class, voteFilter);
    }

    //    todo check this step completely
    @Transactional(rollbackOn = SystemException.class)
    public boolean create(VoteIn voteIn) throws SystemException {
        if (voteIn == null || voteIn.getCandidateIds() == null || voteIn.getCandidateIds().length == 0)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, "vote must have candidate", 1911);
        voteDao.checkValid(voteIn.getStudentId(), voteIn.getSelectionId());

        SelectionEntity selectionEntity = selectionService.getEntityById(voteIn.getSelectionId(), null);
        if (selectionEntity.getFromDate().compareTo(ZonedDateTime.now()) < 0 || selectionEntity.getToDate().compareTo(ZonedDateTime.now()) > 0)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, "this selection is finished!", 1912);
        if (voteIn.getCandidateIds().length > selectionEntity.getLimitVote())
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, "vote number( " + voteIn.getCandidateIds().length + " ) more that selection vote limit " + selectionEntity.getLimitVote(), 1913);

        VoteEntity entity = modelMapper.map(voteIn, VoteEntity.class);
        for (Integer candidate : voteIn.getCandidateIds()) {
            entity.setCandidateId(candidate);
            checkDuplicate(entity);
            createEntity(entity);
        }
        return true;
    }

    public boolean checkDuplicate(VoteEntity entity) throws SystemException {
        return voteDao.checkDuplicate(entity);
    }

    @Override
    public Pair<ReportCondition, ReportOption> filterChain(FilterBase filterBase) {
        ReportOption reportOption = new ReportOption();

        ReportCondition condition = new ReportCondition();
        VoteFilter voteFilter = (VoteFilter) filterBase;
        condition.addEqualCondition("candidateId", voteFilter.getCandidateId());
        condition.addEqualCondition("studentId", voteFilter.getStudentId());
        condition.addEqualCondition("selectionId", voteFilter.getSelectionId());
        return new Pair<>(condition, reportOption);
    }
}
