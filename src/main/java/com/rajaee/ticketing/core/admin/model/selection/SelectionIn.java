package com.rajaee.ticketing.core.admin.model.selection;

import com.rajaee.ticketing.databse.core.statics.Gender;
import com.rajaee.ticketing.databse.core.statics.Grade;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.List;

@Setter
@Getter
public class SelectionIn {
    @NotNull
    private String name;
    private List<Integer> dormId;
    private List<Integer> fieldId;
    private List<Integer> collegeId;
    private List<Integer> inputYear;
    private List<Grade> grads;
    private List<Gender> genders;
    private Integer limitVote;
    private ZonedDateTime fromDate;
    private ZonedDateTime toDate;
}
