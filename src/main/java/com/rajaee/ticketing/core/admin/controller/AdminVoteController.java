package com.rajaee.ticketing.core.admin.controller;

import com.rajaee.ticketing.core.admin.model.vote.VoteFilter;
import com.rajaee.ticketing.core.admin.model.vote.VoteIn;
import com.rajaee.ticketing.core.admin.repository.service.AdminVoteService;
import com.rajaee.ticketing.core.admin.statics.CoreRestApi;
import com.rajaee.ticketing.utility.model.dto.FilterBase;
import com.rajaee.ticketing.utility.model.object.Pair;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "Vote Controller")
@Controller
@RequestMapping(RestApi.REST_IDENTIFIED)
@Validated
public class AdminVoteController {

    private AdminVoteService voteService;

    @Autowired
    public AdminVoteController(AdminVoteService voteService) {
        this.voteService = voteService;
    }

    @ApiOperation(value = "get vote count", response = Integer.class)
    @GetMapping(path = CoreRestApi.VOTE_COUNT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity count(@Valid @RequestParam VoteFilter voteFilter) {
        return new ResponseEntity<>(voteService.count(voteFilter), HttpStatus.OK);
    }

    @ApiOperation(value = "create a vote", response = Boolean.class)
    @PostMapping(path = CoreRestApi.VOTE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> create(@Valid @RequestBody VoteIn voteIn) throws SystemException {
        return new ResponseEntity<Boolean>(voteService.create(voteIn), HttpStatus.OK);
    }

    @ApiOperation(value = "unknown", response = Pair.class)
    @GetMapping(path = CoreRestApi.VOTE_FILTERCHAIN, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity filterChain(@RequestParam FilterBase filterBase) {
        return new ResponseEntity<>(voteService.filterChain(filterBase), HttpStatus.OK);
    }

    //note : baraye checkDuplicate rest nazashtam

}
