package com.rajaee.ticketing.core.admin.model.dorm;

import com.rajaee.ticketing.databse.core.statics.Gender;
import com.rajaee.ticketing.utility.model.dto.FilterBase;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DormFilter extends FilterBase {
    private Integer id;
    private String name;
    private Gender gender;
}
