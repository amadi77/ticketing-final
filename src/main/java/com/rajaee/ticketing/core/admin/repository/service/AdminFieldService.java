package com.rajaee.ticketing.core.admin.repository.service;

import com.rajaee.ticketing.core.admin.model.field.FieldFilter;
import com.rajaee.ticketing.core.admin.model.field.FieldIn;
import com.rajaee.ticketing.core.admin.model.field.FieldOut;
import com.rajaee.ticketing.core.admin.model.field.FieldPageableFilter;
import com.rajaee.ticketing.databse.core.entity.FieldEntity;
import com.rajaee.ticketing.utility.model.dto.FilterBase;
import com.rajaee.ticketing.utility.model.object.Pair;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.model.object.report.ReportCondition;
import com.rajaee.ticketing.utility.model.object.report.ReportOption;
import com.rajaee.ticketing.utility.repository.dao.base.GenericDao;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdminFieldService extends GenericDao {
    private final ModelMapper modelMapper;

    @Autowired
    public AdminFieldService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public List<FieldEntity> getAllEntities(FieldPageableFilter pageableFilter, String[] include) {
        return getAllEntities(FieldEntity.class, pageableFilter, include);
    }

    public FieldEntity getEntityById(int id, String[] include) throws SystemException {
        return getEntityById(FieldEntity.class, id, include);
    }

    public int count(FieldFilter fieldFilter) {
        return countEntity(FieldEntity.class, fieldFilter);
    }

    public boolean delete(int id) {
        return deleteById(FieldEntity.class, id);
    }

    public List<FieldOut> getAll(FieldPageableFilter filter, String[] include) {
        return getAllEntities(filter, include).stream().map(FieldOut::new).collect(Collectors.toList());
    }

    public FieldOut getById(int id, String[] include) throws SystemException {
        return new FieldOut(getEntityById(id, include));
    }

    public FieldOut create(FieldIn fieldIn) {
        FieldEntity entity = modelMapper.map(fieldIn, FieldEntity.class);
        createEntity(entity);
        return new FieldOut(entity);
    }

    public FieldOut update(Integer id, FieldIn fieldIn) {
        FieldEntity entity = modelMapper.map(fieldIn, FieldEntity.class);
        entity.setId(id);
        updateEntity(entity);
        return new FieldOut(entity);
    }


    @Override
    public Pair<ReportCondition, ReportOption> filterChain(FilterBase filterBase) {
        ReportOption reportOption = new ReportOption();

        if (filterBase instanceof FieldPageableFilter) {
            FieldPageableFilter pageableFilter = (FieldPageableFilter) filterBase;
            reportOption.setSortOptions(pageableFilter.getSort());
            reportOption.setPageSize(pageableFilter.getSize());
            reportOption.setPageNumber(pageableFilter.getPage());
        }

        FieldFilter filter = (FieldFilter) filterBase;
        ReportCondition condition = new ReportCondition();
        condition.addLikeCondition("fieldName", filter.getFieldName());
        condition.addLikeCondition("orientation", filter.getOrientation());
        condition.addEqualCondition("id", filter.getId());
        condition.addEqualCondition("collegeId", filter.getCollegeId());

        return new Pair<>(condition, reportOption);
    }
}
