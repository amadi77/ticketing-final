package com.rajaee.ticketing.core.admin.model.college;

import com.rajaee.ticketing.utility.model.dto.FilterBase;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CollegeFilter  extends FilterBase {
    private Integer id;
    private String collegeEngName;
    private String collegeName;

}
