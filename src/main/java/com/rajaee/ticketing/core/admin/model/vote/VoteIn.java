package com.rajaee.ticketing.core.admin.model.vote;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class VoteIn {
    @NotNull
    private Integer studentId;
    @NotNull
    private Integer[] candidateIds;
    @NotNull
    private Integer selectionId;
}
