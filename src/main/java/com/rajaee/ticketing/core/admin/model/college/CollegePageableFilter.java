package com.rajaee.ticketing.core.admin.model.college;

import com.rajaee.ticketing.utility.model.object.SortOption;
import com.rajaee.ticketing.utility.statics.enums.SortType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class CollegePageableFilter extends CollegeFilter {
    @Min(1)
    private int size;
    @Min(1)
    private int page;
    private List<SortOption> sort;
    public CollegePageableFilter() {
        this.size = 10;
        this.page = 1;
        this.sort = new ArrayList<>();
        this.sort.add(new SortOption("id", SortType.DESCENDING));
    }
}
