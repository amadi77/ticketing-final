package com.rajaee.ticketing.core.admin.model.selection;

import com.rajaee.ticketing.databse.core.statics.Gender;
import com.rajaee.ticketing.databse.core.statics.Grade;
import com.rajaee.ticketing.utility.model.dto.FilterBase;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;
import java.util.List;

@Setter
@Getter
public class SelectionFilter extends FilterBase {
    private Integer id;
    private Integer limitVote;
    private String name;
    private ZonedDateTime fromDateMax;
    private ZonedDateTime fromDateMin;
    private ZonedDateTime toDateMax = ZonedDateTime.now();
    private ZonedDateTime toDateMin;
    private List<Integer> dormId;
    private List<Integer> fieldId;
    private List<Integer> collegeId;
    private List<Integer> inputYear;
    private List<Grade> grads;
    private List<Gender> genders;
    private String studentId;
    private String nationalId;

    public void setToDateMax(ZonedDateTime toDateMax) {
        if (toDateMax == null)
            this.toDateMax = ZonedDateTime.now();
        else if (toDateMax.compareTo(ZonedDateTime.now()) > 0)
            this.toDateMax = ZonedDateTime.now();
        else
            this.toDateMax = toDateMax;

    }
}
