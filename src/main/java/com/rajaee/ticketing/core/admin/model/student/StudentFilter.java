package com.rajaee.ticketing.core.admin.model.student;

import com.rajaee.ticketing.databse.core.statics.Gender;
import com.rajaee.ticketing.databse.core.statics.Grade;
import com.rajaee.ticketing.utility.model.dto.FilterBase;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class StudentFilter extends FilterBase {
    private Integer id;
    private String studentId;
    private String name;
    private String family;
    private String nationalCode;
    private Integer inputYear;
    private Integer fieldId;
    private Integer dormId;
    private Integer collegeId;
    private Gender gender;
    private Grade grade;
}
