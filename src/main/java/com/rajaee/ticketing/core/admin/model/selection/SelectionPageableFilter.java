package com.rajaee.ticketing.core.admin.model.selection;

import com.rajaee.ticketing.utility.model.object.SortOption;
import com.rajaee.ticketing.utility.statics.enums.SortType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class SelectionPageableFilter extends SelectionFilter {
    @Min(1)
    private Integer size;
    @Min(1)
    private Integer page;
    private List<SortOption> sort;

    public SelectionPageableFilter(){
        this.size = 10;
        this.page = 1;
        this.sort = new ArrayList<>();
        this.sort.add(new SortOption("id", SortType.DESCENDING));

    }
}
