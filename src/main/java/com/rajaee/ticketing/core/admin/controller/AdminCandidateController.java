package com.rajaee.ticketing.core.admin.controller;


import com.rajaee.ticketing.core.admin.model.candidate.CandidateIn;
import com.rajaee.ticketing.core.admin.model.candidate.CandidateOut;
import com.rajaee.ticketing.core.admin.model.candidate.CandidatePageableFilter;
import com.rajaee.ticketing.core.admin.repository.service.AdminCandidateService;
import com.rajaee.ticketing.core.admin.statics.CoreRestApi;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "Candidate controller")
@Controller
@RequestMapping(path = RestApi.REST_IDENTIFIED)
@Valid
public class AdminCandidateController {

    private AdminCandidateService candidateService;

    @Autowired
    public AdminCandidateController(AdminCandidateService candidateService) {
        this.candidateService = candidateService;
    }

    @ApiOperation(value = "get all candidate by filter", response = CandidateOut.class, responseContainer = "List")
    @GetMapping(path = CoreRestApi.CANDIDATE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAll(@Valid @RequestParam CandidatePageableFilter pageableFilter, @RequestParam(required = false) String[] include) {
        return new ResponseEntity<>(candidateService.getAll(pageableFilter, include), HttpStatus.OK);
    }

    @ApiOperation(value = "get candidate by id", response = CandidateOut.class)
    @GetMapping(path = CoreRestApi.CANDIDATE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getById(@PathVariable Integer id, @RequestParam(required = false) String[] include) throws SystemException {
        return new ResponseEntity<>(candidateService.getById(id, include), HttpStatus.OK);
    }

    @ApiOperation(value = "get count candidate math by filter", response = Integer.class)
    @GetMapping(path = CoreRestApi.CANDIDATE_COUNT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> count(@Valid @RequestParam CandidatePageableFilter pageableFilter) {
        return new ResponseEntity<>(candidateService.count(pageableFilter), HttpStatus.OK);
    }

    @ApiOperation(value = "create candidate entity", response = CandidateOut.class)
    @PostMapping(path = CoreRestApi.CANDIDATE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@Valid @RequestBody CandidateIn candidateIn) {
        return new ResponseEntity<>(candidateService.create(candidateIn), HttpStatus.OK);
    }

    @ApiOperation(value = "update candidate entity", response = CandidateOut.class)
    @PutMapping(path = CoreRestApi.CANDIDATE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@PathVariable("id") Integer id, @RequestBody CandidateIn candidateIn) {
        return new ResponseEntity<>(candidateService.update(id, candidateIn), HttpStatus.OK);
    }

    @ApiOperation(value = "delete candidate entity ", response = Boolean.class)
    @DeleteMapping(path = CoreRestApi.CANDIDATE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@PathVariable("id") Integer id) {
        return new ResponseEntity<>(candidateService.delete(id), HttpStatus.OK);
    }

}
