package com.rajaee.ticketing.core.admin.controller;


import com.rajaee.ticketing.core.admin.model.selection.SelectionFilter;
import com.rajaee.ticketing.core.admin.model.selection.SelectionIn;
import com.rajaee.ticketing.core.admin.model.selection.SelectionOut;
import com.rajaee.ticketing.core.admin.model.selection.SelectionPageableFilter;
import com.rajaee.ticketing.core.admin.repository.service.AdminSelectionService;
import com.rajaee.ticketing.core.admin.statics.CoreRestApi;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "Dorm controller")
@Controller
@RequestMapping(path = RestApi.REST_IDENTIFIED)
@Valid
public class AdminSelectionController {

    private AdminSelectionService selectionService;

    @Autowired
    public AdminSelectionController(AdminSelectionService selectionService) {
        this.selectionService = selectionService;
    }

    @ApiOperation(value = "get all selections by filter", response = SelectionOut.class, responseContainer = "List")
    @GetMapping(path = CoreRestApi.SELECTION, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAll(@Valid @RequestParam SelectionPageableFilter selectionPageableFilter, @RequestParam(required = false) String[] include) {
        return new ResponseEntity<>(selectionService.getAll(selectionPageableFilter, include), HttpStatus.OK);
    }

    @ApiOperation(value = "get selection by id", response = SelectionOut.class)
    @GetMapping(path = CoreRestApi.SELECTION_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getById(@PathVariable Integer id, @RequestParam(required = false) String[] include) throws SystemException {
        return new ResponseEntity<>(selectionService.getById(id, include), HttpStatus.OK);
    }

    @ApiOperation(value = "create selection", response = SelectionOut.class)
    @PostMapping(path = CoreRestApi.SELECTION, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@Valid @RequestBody SelectionIn selectionIn) {
        return new ResponseEntity<>(selectionService.create(selectionIn), HttpStatus.OK);
    }

    @ApiOperation(value = "update selection", response = SelectionOut.class)
    @PutMapping(path = CoreRestApi.SELECTION_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@PathVariable Integer id, @Valid @RequestBody SelectionIn selectionIn) {
        return new ResponseEntity<>(selectionService.update(id, selectionIn), HttpStatus.OK);
    }

    @ApiOperation(value = "delete selection", response = Boolean.class)
    @DeleteMapping(path = CoreRestApi.SELECTION_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@PathVariable Integer id) {
        return new ResponseEntity<>(selectionService.delete(id), HttpStatus.OK);
    }

    @ApiOperation(value = "get list of valid selection for student", response = SelectionOut.class, responseContainer = "List")
    @GetMapping(path = CoreRestApi.SELECTION_VALID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity listValidForStudent(@PathVariable Integer id) throws SystemException {
        return new ResponseEntity<>(selectionService.listValidForStudent(id), HttpStatus.OK);
    }

    @ApiOperation(value = "get selection count by filter", response = Integer.class)
    @GetMapping(path = CoreRestApi.SELECTION_COUNT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity count(@Valid @RequestParam SelectionFilter selectionFilter) {
        return new ResponseEntity<>(selectionService.count(selectionFilter), HttpStatus.OK);
    }

}
