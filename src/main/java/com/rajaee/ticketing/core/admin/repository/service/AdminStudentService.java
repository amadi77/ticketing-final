package com.rajaee.ticketing.core.admin.repository.service;

import com.rajaee.ticketing.core.admin.model.student.*;
import com.rajaee.ticketing.databse.core.entity.StudentEntity;
import com.rajaee.ticketing.utility.model.dto.FilterBase;
import com.rajaee.ticketing.utility.model.object.Pair;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.model.object.report.ReportCondition;
import com.rajaee.ticketing.utility.model.object.report.ReportOption;
import com.rajaee.ticketing.utility.repository.dao.base.GenericDao;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdminStudentService extends GenericDao {
    private final ModelMapper modelMapper;

    @Autowired
    public AdminStudentService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public List<StudentEntity> getAllEntities(StudentPageableFilter pageableFilter, String[] include) {
        return getAllEntities(StudentEntity.class, pageableFilter, include);
    }

    public StudentEntity getEntityById(int id, String[] include) throws SystemException {
        return getEntityById(StudentEntity.class, id, include);
    }

    public int count(StudentFilter studentFilter) {
        return countEntity(StudentEntity.class, studentFilter);
    }

    public boolean delete(int id) {
        return deleteById(StudentEntity.class, id);
    }

    public List<StudentOut> getAll(StudentPageableFilter filter, String[] include) {
        return getAllEntities(filter, include).stream().map(item -> modelMapper.map(item, StudentOut.class)).collect(Collectors.toList());
    }

    public StudentOut getById(int id, String[] include) throws SystemException {
        return modelMapper.map(getEntityById(id, include), StudentOut.class);
    }

    public StudentOut create(StudentIn studentIn) {
        StudentEntity entity = modelMapper.map(studentIn, StudentEntity.class);
        createEntity(entity);
        return modelMapper.map(entity, StudentOut.class);
    }

    public StudentOut update(Integer id, StudentEditIn studentIn) {
        StudentEntity entity = modelMapper.map(studentIn, StudentEntity.class);
        entity.setId(id);
        updateEntity(entity);
        return modelMapper.map(entity, StudentOut.class);
    }

    @Override
    public Pair<ReportCondition, ReportOption> filterChain(FilterBase filterBase) {
        ReportOption reportOption = new ReportOption();

        if (filterBase instanceof StudentPageableFilter) {
            StudentPageableFilter pageableFilter = (StudentPageableFilter) filterBase;
            reportOption.setSortOptions(pageableFilter.getSort());
            reportOption.setPageSize(pageableFilter.getSize());
            reportOption.setPageNumber(pageableFilter.getPage());
        }

        StudentFilter filter = (StudentFilter) filterBase;
        ReportCondition condition = new ReportCondition();
        condition.addLikeCondition("name", filter.getName());
        condition.addLikeCondition("studentId", filter.getStudentId());
        condition.addLikeCondition("family", filter.getFamily());
        condition.addEqualCondition("id", filter.getId());
        condition.addEqualCondition("inputYear", filter.getInputYear());
        condition.addEqualCondition("fieldId", filter.getFieldId());
        condition.addEqualCondition("dormId", filter.getDormId());
        condition.addEqualCondition("collegeId", filter.getCollegeId());
        condition.addEqualCondition("gender", filter.getGender());
        condition.addEqualCondition("grade", filter.getGrade());

        return new Pair<>(condition, reportOption);
    }
}
