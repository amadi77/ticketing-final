package com.rajaee.ticketing.core.admin.model.student;

import com.rajaee.ticketing.databse.core.statics.Gender;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class StudentIn extends StudentEditIn {
    private Integer id;
    private String studentId;
    private Gender gender;
    private Integer collegeId;
}
