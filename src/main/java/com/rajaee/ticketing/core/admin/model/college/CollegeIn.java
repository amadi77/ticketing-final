package com.rajaee.ticketing.core.admin.model.college;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class CollegeIn {
    @NotNull
    private String collegeName;
    @NotNull
    private String collegeEngName;
}
