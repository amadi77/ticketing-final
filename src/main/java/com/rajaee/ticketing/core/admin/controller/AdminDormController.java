package com.rajaee.ticketing.core.admin.controller;

import com.rajaee.ticketing.core.admin.model.dorm.DormFilter;
import com.rajaee.ticketing.core.admin.model.dorm.DormIn;
import com.rajaee.ticketing.core.admin.model.dorm.DormOut;
import com.rajaee.ticketing.core.admin.model.dorm.DormPageableFilter;
import com.rajaee.ticketing.core.admin.repository.service.AdminDormService;
import com.rajaee.ticketing.core.admin.statics.CoreRestApi;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.validation.Valid;

@Api(tags = "Dorm controller")
@Controller
@RequestMapping(path = RestApi.REST_IDENTIFIED)
@Valid
public class AdminDormController {
    private final AdminDormService dormService;

    @Autowired
    public AdminDormController(AdminDormService dormService) {
        this.dormService = dormService;
    }

    @ApiOperation(value = "get all dorm math by filter", response = DormOut.class, responseContainer = "List")
    @GetMapping(path = CoreRestApi.DORM, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAll(@Valid @RequestParam DormPageableFilter pageableFilter, BindingResult bindingResult, @RequestParam(required = false) String[] include, ServletRequest servletRequest) {
        return new ResponseEntity<>(dormService.getAll(pageableFilter, include), HttpStatus.OK);
    }

    @ApiOperation(value = "get all dorm math by filter", response = DormOut.class)
    @GetMapping(path = CoreRestApi.DORM_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getById(@PathVariable(name = "id") Integer id, @RequestParam(required = false) String[] include, ServletRequest servletRequest) throws SystemException {
        return new ResponseEntity<>(dormService.getById(id, include), HttpStatus.OK);
    }

    @ApiOperation(value = "create dorm entity", response = DormOut.class)
    @PostMapping(path = CoreRestApi.DORM, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@Valid @RequestBody DormIn dormIn, BindingResult bindingResult, ServletRequest servletRequest) throws SystemException {
        return new ResponseEntity<>(dormService.create(dormIn), HttpStatus.OK);
    }

    @ApiOperation(value = "update dorm entity", response = DormOut.class)
    @PutMapping(path = CoreRestApi.DORM_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@PathVariable("id") Integer id,
                                 @RequestBody DormIn dormIn,
                                 BindingResult bindingResult,
                                 ServletRequest servletRequest) throws SystemException {
        return new ResponseEntity<>(dormService.update(id, dormIn), HttpStatus.OK);
    }

    @ApiOperation(value = "delete dorm ", response = DormOut.class)
    @DeleteMapping(path = CoreRestApi.DORM_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@PathVariable("id") Integer id,
                                 ServletRequest servletRequest) throws SystemException {
        return new ResponseEntity<>(dormService.delete(id), HttpStatus.OK);
    }

    @ApiOperation(value = "get count dorm math by filter", response = Integer.class)
    @GetMapping(path = CoreRestApi.DORM_COUNT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> count(@Valid @RequestParam DormFilter pageableFilter, BindingResult bindingResult, ServletRequest servletRequest) {
        return new ResponseEntity<>(dormService.count(pageableFilter), HttpStatus.OK);
    }
}
