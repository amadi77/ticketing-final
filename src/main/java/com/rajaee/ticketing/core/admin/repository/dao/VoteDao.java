package com.rajaee.ticketing.core.admin.repository.dao;

import com.rajaee.ticketing.core.admin.model.vote.VoteIn;
import com.rajaee.ticketing.databse.core.entity.VoteEntity;
import com.rajaee.ticketing.utility.model.object.SystemError;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.repository.dao.base.BaseDaoInterface;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class VoteDao {
    private final BaseDaoInterface baseDaoInterface;

    public VoteDao(BaseDaoInterface baseDaoInterface) {
        this.baseDaoInterface = baseDaoInterface;
    }

    public boolean checkDuplicate(VoteEntity entity) throws SystemException {
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("studentId", entity.getStudentId());
        objectMap.put("selectionId", entity.getSelectionId());
        objectMap.put("candidateId", entity.getCandidateId());

        if (baseDaoInterface.getByAndConditions(VoteEntity.class, objectMap) != null)
            throw new SystemException(SystemError.DUPLICATE_REQUEST,
                    "duplicate vote for" +
                            " student " + entity.getStudentId() +
                            " for selection " + entity.getSelectionId() +
                            " and candidate " + entity.getCandidateId()
                    , 1902);
        return true;
    }

    public boolean checkValid(Integer studentId, Integer selectionId) throws SystemException {
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("studentId", studentId);
        objectMap.put("selectionId", selectionId);
        if (baseDaoInterface.getByAndConditions(VoteEntity.class, objectMap) != null)
            throw new SystemException(SystemError.DUPLICATE_REQUEST,
                    "duplicate vote for student " + studentId + " for selection " + selectionId
                    , 1901);
        return true;
    }
}
