package com.rajaee.ticketing.core.admin.model.student;

import com.rajaee.ticketing.core.admin.model.college.CollegeOut;
import com.rajaee.ticketing.core.admin.model.field.FieldOut;
import com.rajaee.ticketing.core.admin.model.user.UserInfo;
import com.rajaee.ticketing.core.pub.model.dorm.DormOut;
import com.rajaee.ticketing.databse.core.entity.StudentEntity;
import com.rajaee.ticketing.utility.config.StaticApplicationContext;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.modelmapper.ModelMapper;

@Setter(AccessLevel.PRIVATE)
@Getter
public class StudentOut extends StudentIn {
    private DormOut dorm;
    private FieldOut field;
    private CollegeOut college;
    private UserInfo user;

    public StudentOut(StudentEntity entity) {
        if (entity != null) {

            ModelMapper mapper = StaticApplicationContext.getContext().getBean(ModelMapper.class);
            mapper.map(entity, this);

            if (Hibernate.isInitialized(entity.getField()))
                field = new FieldOut(entity.getField());
            if (Hibernate.isInitialized(entity.getCollege()))
                college = new CollegeOut(entity.getCollege());
            if (Hibernate.isInitialized(entity.getDorm()))
                dorm = mapper.map(entity.getDorm(), DormOut.class);
            if (Hibernate.isInitialized(entity.getUser()))
                user = new UserInfo(entity.getUser());
        }
    }
}
