package com.rajaee.ticketing.core.admin.repository.service;

import com.google.common.base.Joiner;
import com.rajaee.ticketing.core.admin.model.selection.SelectionFilter;
import com.rajaee.ticketing.core.admin.model.selection.SelectionIn;
import com.rajaee.ticketing.core.admin.model.selection.SelectionOut;
import com.rajaee.ticketing.core.admin.model.selection.SelectionPageableFilter;
import com.rajaee.ticketing.core.admin.repository.dao.SelectionDao;
import com.rajaee.ticketing.databse.core.entity.SelectionEntity;
import com.rajaee.ticketing.databse.core.entity.StudentEntity;
import com.rajaee.ticketing.databse.core.statics.Grade;
import com.rajaee.ticketing.utility.model.dto.FilterBase;
import com.rajaee.ticketing.utility.model.object.Pair;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.model.object.report.ReportCondition;
import com.rajaee.ticketing.utility.model.object.report.ReportOption;
import com.rajaee.ticketing.utility.repository.dao.base.GenericDao;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdminSelectionService extends GenericDao {
    private final ModelMapper modelMapper;
    private final SelectionDao selectionDao;
    private final AdminStudentService studentService;

    @Autowired
    public AdminSelectionService(ModelMapper modelMapper, SelectionDao selectionDao, AdminStudentService studentService) {
        this.modelMapper = modelMapper;
        this.selectionDao = selectionDao;
        this.studentService = studentService;
    }

    public List<SelectionEntity> getAllEntities(SelectionPageableFilter pageableFilter, String[] include) {
        return getAllEntities(SelectionEntity.class, pageableFilter, include);
    }

    public SelectionEntity getEntityById(int id, String[] include) throws SystemException {
        return getEntityById(SelectionEntity.class, id, include);
    }

    public int count(SelectionFilter selectionFilter) {
        return countEntity(SelectionEntity.class, selectionFilter);
    }

    public boolean delete(int id) {
        return deleteById(SelectionEntity.class, id);
    }

    public List<SelectionOut> getAll(SelectionPageableFilter filter, String[] include) {
        return getAllEntities(filter, include).stream().map(SelectionOut::new).collect(Collectors.toList());
    }

    public SelectionOut getById(int id, String[] include) throws SystemException {
        return new SelectionOut(getEntityById(id, include));
    }

    public SelectionOut create(SelectionIn selectionIn) {
        SelectionEntity entity = entityFromIn(selectionIn);
        createEntity(entity);
        return new SelectionOut(entity);
    }

    public SelectionOut update(Integer id, SelectionIn selectionIn) {
        SelectionEntity entity = entityFromIn(selectionIn);
        entity.setId(id);
        updateEntity(entity);
        return new SelectionOut(entity);
    }

    public List<SelectionOut> listValidForStudent(Integer id) throws SystemException {
        StudentEntity studentEntity = studentService.getEntityById(id, null);
        return selectionDao.getByStudent(studentEntity)
                .stream().map(item -> modelMapper.map(item, SelectionOut.class))
                .collect(Collectors.toList());
    }

    private SelectionEntity entityFromIn(SelectionIn selectionIn) {
        SelectionEntity entity = new SelectionEntity();
        entity.setName(selectionIn.getName());
        entity.setFromDate(selectionIn.getFromDate());
        entity.setToDate(selectionIn.getToDate());
        entity.setLimitVote(selectionIn.getLimitVote());

        if (selectionIn.getDormId() != null)
            entity.setDormIds(Joiner.on("*,").join(selectionIn.getDormId()));
        if (selectionIn.getCollegeId() != null)
            entity.setCollegeIds(Joiner.on("*,").join(selectionIn.getCollegeId()));
        if (selectionIn.getInputYear() != null)
            entity.setInputYears(Joiner.on("*,").join(selectionIn.getInputYear()));
        if (selectionIn.getFieldId() != null)
            entity.setFieldIds(Joiner.on("*,").join(selectionIn.getFieldId()));
        if (selectionIn.getGenders() != null)
            entity.setGender(Joiner.on("*,").join(selectionIn.getGenders()));
        if (selectionIn.getGrads() != null)
            entity.setGrades(Joiner.on("*,").join(selectionIn.getGrads()));
        return entity;
    }

    @Override
    public Pair<ReportCondition, ReportOption> filterChain(FilterBase filterBase) {
        ReportOption reportOption = new ReportOption();

        if (filterBase instanceof SelectionPageableFilter) {
            SelectionPageableFilter pageableFilter = (SelectionPageableFilter) filterBase;
            reportOption.setSortOptions(pageableFilter.getSort());
            reportOption.setPageSize(pageableFilter.getSize());
            reportOption.setPageNumber(pageableFilter.getPage());
        }

        SelectionFilter filter = (SelectionFilter) filterBase;
        ReportCondition condition = new ReportCondition();
        ReportCondition orCondition = new ReportCondition();
        condition.addLikeCondition("name", filter.getName());
        condition.addEqualCondition("id", filter.getId());
        condition.addEqualCondition("limitVote", filter.getLimitVote());
        if (filter.getCollegeId() != null && !filter.getCollegeId().isEmpty()) {
            for (Integer item : filter.getCollegeId()) {
                orCondition.addLikeCondition("collegeIds", item + "*,");
            }
        }
        if (filter.getDormId() != null && !filter.getDormId().isEmpty()) {
            for (Integer item : filter.getDormId()) {
                orCondition.addLikeCondition("dormIds", item + "*,");
            }
        }
        if (filter.getFieldId() != null && !filter.getFieldId().isEmpty()) {
            for (Integer item : filter.getFieldId()) {
                orCondition.addLikeCondition("fieldIds", item + "*,");
            }
            condition.addOrCondition(orCondition);
        }
        if (filter.getGenders() != null && !filter.getGenders().isEmpty()) {
            for (Integer item : filter.getFieldId()) {
                orCondition.addLikeCondition("gender", item + "*,");
            }
        }
        if (filter.getGrads() != null && !filter.getGrads().isEmpty()) {
            for (Grade item : filter.getGrads()) {
                orCondition.addLikeCondition("grades", item + "*,");
            }
        }
        if (filter.getInputYear() != null && !filter.getInputYear().isEmpty()) {
            for (Integer item : filter.getInputYear()) {
                orCondition.addLikeCondition("inputYears", item + "*,");
            }
        }
        condition.addOrCondition(orCondition);
        condition.addMaxZonedTimeCondition("fromDate", filter.getFromDateMax());
        condition.addMinZonedTimeCondition("fromDate", filter.getFromDateMin());
        condition.addMaxZonedTimeCondition("toDate", filter.getToDateMax());
        condition.addMinZonedTimeCondition("toDate", filter.getToDateMin());

        return new Pair<>(condition, reportOption);
    }

}
