package com.rajaee.ticketing.core.admin.controller;

import com.rajaee.ticketing.core.admin.model.college.CollegeIn;
import com.rajaee.ticketing.core.admin.model.college.CollegeOut;
import com.rajaee.ticketing.core.admin.model.college.CollegePageableFilter;
import com.rajaee.ticketing.core.admin.repository.service.AdminCollegeService;
import com.rajaee.ticketing.core.admin.statics.CoreRestApi;
import com.rajaee.ticketing.utility.model.object.SystemException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

public class AdminCollegeController {
    
    private AdminCollegeService collegeService;

    @Autowired
    public AdminCollegeController(AdminCollegeService collegeService) {
        this.collegeService = collegeService;
    }

    @ApiOperation(value = "get all college by filter", response = CollegeOut.class, responseContainer = "List")
    @GetMapping(path = CoreRestApi.COLLEGE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAll(@Valid @RequestParam CollegePageableFilter pageableFilter, @RequestParam(required = false) String[] include) {
        return new ResponseEntity<>(collegeService.getAll(pageableFilter, include), HttpStatus.OK);
    }

    @ApiOperation(value = "get college by id", response = CollegeOut.class)
    @GetMapping(path = CoreRestApi.COLLEGE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getById(@PathVariable Integer id, @RequestParam(required = false) String[] include) throws SystemException {
        return new ResponseEntity<>(collegeService.getById(id, include), HttpStatus.OK);
    }

    @ApiOperation(value = "get count college math by filter", response = Integer.class)
    @GetMapping(path = CoreRestApi.COLLEGE_COUNT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> count(@Valid @RequestParam CollegePageableFilter pageableFilter) {
        return new ResponseEntity<>(collegeService.count(pageableFilter), HttpStatus.OK);
    }

    @ApiOperation(value = "create college entity", response = CollegeOut.class)
    @PostMapping(path = CoreRestApi.COLLEGE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@Valid @RequestBody CollegeIn collegeIn) {
        return new ResponseEntity<>(collegeService.create(collegeIn), HttpStatus.OK);
    }

    @ApiOperation(value = "update college entity", response = CollegeOut.class)
    @PutMapping(path = CoreRestApi.COLLEGE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@PathVariable("id") Integer id, @RequestBody CollegeIn collegeIn) {
        return new ResponseEntity<>(collegeService.update(id, collegeIn), HttpStatus.OK);
    }

    @ApiOperation(value = "delete college entity ", response = Boolean.class)
    @DeleteMapping(path = CoreRestApi.COLLEGE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@PathVariable("id") Integer id) {
        return new ResponseEntity<>(collegeService.delete(id), HttpStatus.OK);
    }

}
