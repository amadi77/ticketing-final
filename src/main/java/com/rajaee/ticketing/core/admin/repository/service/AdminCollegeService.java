package com.rajaee.ticketing.core.admin.repository.service;

import com.rajaee.ticketing.core.admin.model.college.CollegeFilter;
import com.rajaee.ticketing.core.admin.model.college.CollegeIn;
import com.rajaee.ticketing.core.admin.model.college.CollegeOut;
import com.rajaee.ticketing.core.admin.model.college.CollegePageableFilter;
import com.rajaee.ticketing.databse.core.entity.CollegeEntity;
import com.rajaee.ticketing.utility.model.dto.FilterBase;
import com.rajaee.ticketing.utility.model.object.Pair;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.model.object.report.ReportCondition;
import com.rajaee.ticketing.utility.model.object.report.ReportOption;
import com.rajaee.ticketing.utility.repository.dao.base.GenericDao;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdminCollegeService extends GenericDao {
    private final ModelMapper modelMapper;

    @Autowired
    public AdminCollegeService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public List<CollegeEntity> getAllEntities(CollegePageableFilter pageableFilter, String[] include) {
        return getAllEntities(CollegeEntity.class, pageableFilter, include);
    }

    public CollegeEntity getEntityById(int id, String[] include) throws SystemException {
        return getEntityById(CollegeEntity.class, id, include);
    }

    public int count(CollegeFilter collegeFilter) {
        return countEntity(CollegeEntity.class, collegeFilter);
    }

    public boolean delete(int id) {
        return deleteById(CollegeEntity.class, id);
    }

    public List<CollegeOut> getAll(CollegePageableFilter filter, String[] include) {
        return getAllEntities(filter, include).stream().map(item-> modelMapper.map(item,CollegeOut.class)).collect(Collectors.toList());
    }

    public CollegeOut getById(int id, String[] include) throws SystemException {
        return modelMapper.map(getEntityById(id, include),CollegeOut.class);
    }

    public CollegeOut create(CollegeIn collegeIn) {
        CollegeEntity entity = modelMapper.map(collegeIn, CollegeEntity.class);
        createEntity(entity);
        return modelMapper.map(entity,CollegeOut.class);
    }

    public CollegeOut update(Integer id, CollegeIn collegeIn) {
        CollegeEntity entity = modelMapper.map(collegeIn, CollegeEntity.class);
        entity.setId(id);
        updateEntity(entity);
        return modelMapper.map(entity,CollegeOut.class);
    }


    @Override
    public Pair<ReportCondition, ReportOption> filterChain(FilterBase filterBase) {
        ReportOption reportOption = new ReportOption();

        if (filterBase instanceof CollegePageableFilter) {
            CollegePageableFilter pageableFilter = (CollegePageableFilter) filterBase;
            reportOption.setSortOptions(pageableFilter.getSort());
            reportOption.setPageSize(pageableFilter.getSize());
            reportOption.setPageNumber(pageableFilter.getPage());
        }

        CollegeFilter filter = (CollegeFilter) filterBase;
        ReportCondition condition = new ReportCondition();
        condition.addLikeCondition("collegeName", filter.getCollegeName());
        condition.addLikeCondition("collegeEngName", filter.getCollegeEngName());
        condition.addEqualCondition("id", filter.getId());

        return new Pair<>(condition, reportOption);
    }

}
