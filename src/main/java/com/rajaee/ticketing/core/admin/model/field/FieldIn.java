package com.rajaee.ticketing.core.admin.model.field;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FieldIn {
    private String fieldName;
    private String orientation;
    private Integer collegeId;
}
