package com.rajaee.ticketing.core.admin.model.candidate;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CandidateIn  extends CandidateEditIn{
    private Integer studentId;
    private Integer selectionId;
}
