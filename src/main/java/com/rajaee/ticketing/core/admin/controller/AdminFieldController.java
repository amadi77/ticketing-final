package com.rajaee.ticketing.core.admin.controller;

import com.rajaee.ticketing.core.admin.model.field.FieldFilter;
import com.rajaee.ticketing.core.admin.model.field.FieldIn;
import com.rajaee.ticketing.core.admin.model.field.FieldOut;
import com.rajaee.ticketing.core.admin.model.field.FieldPageableFilter;
import com.rajaee.ticketing.core.admin.repository.service.AdminFieldService;
import com.rajaee.ticketing.core.admin.statics.CoreRestApi;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.validation.Valid;

@Api(tags = "Field controller")
@Controller
@RequestMapping(path = RestApi.REST_IDENTIFIED)
@Valid
public class AdminFieldController {
    private final AdminFieldService adminFieldService;

    @Autowired
    public AdminFieldController(AdminFieldService adminFieldService) {
        this.adminFieldService = adminFieldService;
    }

    @ApiOperation(value = "get all field math by filter", response = FieldOut.class, responseContainer = "List")
    @GetMapping(path = CoreRestApi.FIELD, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAll(@Valid @RequestParam FieldPageableFilter pageableFilter, BindingResult bindingResult, @RequestParam(required = false) String[] include, ServletRequest servletRequest) {
        return new ResponseEntity<>(adminFieldService.getAll(pageableFilter, include), HttpStatus.OK);
    }

    @ApiOperation(value = "get all field math by filter", response = FieldOut.class)
    @GetMapping(path = CoreRestApi.FIELD_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getById(@PathVariable(name = "id") Integer id, @RequestParam(required = false) String[] include, ServletRequest servletRequest) throws SystemException {
        return new ResponseEntity<>(adminFieldService.getById(id, include), HttpStatus.OK);
    }

    @ApiOperation(value = "create field entity", response = FieldOut.class)
    @PostMapping(path = CoreRestApi.FIELD, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@Valid @RequestBody FieldIn fieldIn, BindingResult bindingResult, ServletRequest servletRequest) throws SystemException {
        return new ResponseEntity<>(adminFieldService.create(fieldIn), HttpStatus.OK);
    }

    @ApiOperation(value = "update field entity", response = FieldOut.class)
    @PutMapping(path = CoreRestApi.FIELD_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@PathVariable("id") Integer id,
                                 @RequestBody FieldIn fieldIn,
                                 BindingResult bindingResult,
                                 ServletRequest servletRequest) throws SystemException {
        return new ResponseEntity<>(adminFieldService.update(id, fieldIn), HttpStatus.OK);
    }

    @ApiOperation(value = "delete field ", response = FieldOut.class)
    @DeleteMapping(path = CoreRestApi.FIELD_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@PathVariable("id") Integer id,
                                 ServletRequest servletRequest) throws SystemException {
        return new ResponseEntity<>(adminFieldService.delete(id), HttpStatus.OK);
    }

    @ApiOperation(value = "get count field math by filter", response = Integer.class)
    @GetMapping(path = CoreRestApi.FIELD_COUNT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> count(@Valid @RequestParam FieldFilter pageableFilter, BindingResult bindingResult, ServletRequest servletRequest) {
        return new ResponseEntity<>(adminFieldService.count(pageableFilter), HttpStatus.OK);
    }
}
