package com.rajaee.ticketing.core.admin.model.dorm;

import com.rajaee.ticketing.utility.model.object.SortOption;
import com.rajaee.ticketing.utility.statics.enums.SortType;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class DormPageableFilter extends DormFilter {
    private Integer size;
    private Integer page;
    private List<SortOption> sort;

    public DormPageableFilter() {
        this.size = 10;
        this.page = 1;
        this.sort = new ArrayList<>();
        this.sort.add(new SortOption("id", SortType.DESCENDING));
    }
}

