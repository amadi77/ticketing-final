package com.rajaee.ticketing.core.admin.model.vote;

import com.rajaee.ticketing.utility.model.dto.FilterBase;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class VoteFilter extends FilterBase {
    private Integer id;
    private Integer studentId;
    private Integer candidateId;
    private Integer selectionId;
}
