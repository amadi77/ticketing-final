package com.rajaee.ticketing.core.admin.model.candidate;

import com.rajaee.ticketing.utility.model.dto.FilterBase;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CandidateFilter extends FilterBase {
    private Integer studentId;
    private Integer selectionId;

}
