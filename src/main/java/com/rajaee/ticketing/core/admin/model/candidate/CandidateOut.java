package com.rajaee.ticketing.core.admin.model.candidate;

import com.rajaee.ticketing.core.admin.model.selection.SelectionInfo;
import com.rajaee.ticketing.core.admin.model.student.StudentOut;
import com.rajaee.ticketing.databse.core.entity.CandidateEntity;
import com.rajaee.ticketing.utility.config.StaticApplicationContext;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.modelmapper.ModelMapper;

@Setter(AccessLevel.PRIVATE)
@Getter
public class CandidateOut extends CandidateIn {
    private SelectionInfo selection;
    private StudentOut student;

    public CandidateOut(CandidateEntity entity) {
        if (entity != null) {
            ModelMapper mapper = StaticApplicationContext.getContext().getBean(ModelMapper.class);
            mapper.map(entity, this);
            if (Hibernate.isInitialized(entity.getSelection())) {
                selection = new SelectionInfo(entity.getSelection());
            }
            if (Hibernate.isInitialized(entity.getStudent())) {
                student = new StudentOut(entity.getStudent());
            }
        }
    }
}
