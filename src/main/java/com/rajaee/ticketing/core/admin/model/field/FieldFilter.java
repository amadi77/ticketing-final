package com.rajaee.ticketing.core.admin.model.field;

import com.rajaee.ticketing.utility.model.dto.FilterBase;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FieldFilter extends FilterBase {
    private Integer id;
    private Integer collegeId;
    private String FieldName;
    private String orientation;
}
