package com.rajaee.ticketing.core.admin.repository.service;

import com.rajaee.ticketing.core.admin.model.candidate.CandidateFilter;
import com.rajaee.ticketing.core.admin.model.candidate.CandidateIn;
import com.rajaee.ticketing.core.admin.model.candidate.CandidateOut;
import com.rajaee.ticketing.core.admin.model.candidate.CandidatePageableFilter;
import com.rajaee.ticketing.databse.core.entity.CandidateEntity;
import com.rajaee.ticketing.utility.model.dto.FilterBase;
import com.rajaee.ticketing.utility.model.object.Pair;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.model.object.report.ReportCondition;
import com.rajaee.ticketing.utility.model.object.report.ReportOption;
import com.rajaee.ticketing.utility.repository.dao.base.GenericDao;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdminCandidateService extends GenericDao {
    private final ModelMapper modelMapper;

    @Autowired
    public AdminCandidateService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public List<CandidateEntity> getAllEntities(CandidatePageableFilter pageableFilter, String[] include) {
        return getAllEntities(CandidateEntity.class, pageableFilter, include);
    }

    public CandidateEntity getEntityById(int id, String[] include) throws SystemException {
        return getEntityById(CandidateEntity.class, id, include);
    }

    public int count(CandidateFilter candidateFilter) {
        return countEntity(CandidateEntity.class, candidateFilter);
    }

    public boolean delete(int id) {
        return deleteById(CandidateEntity.class, id);
    }

    public List<CandidateOut> getAll(CandidatePageableFilter filter, String[] include) {
        return getAllEntities(filter, include).stream().map(item-> modelMapper.map(item,CandidateOut.class)).collect(Collectors.toList());
    }

    public CandidateOut getById(int id, String[] include) throws SystemException {
        return modelMapper.map(getEntityById(id, include),CandidateOut.class);
    }

    public CandidateOut create(CandidateIn candidateIn) {
        CandidateEntity entity = modelMapper.map(candidateIn, CandidateEntity.class);
        createEntity(entity);
        return modelMapper.map(entity,CandidateOut.class);
    }

    public CandidateOut update(Integer id, CandidateIn candidateIn) {
        CandidateEntity entity = modelMapper.map(candidateIn, CandidateEntity.class);
        entity.setId(id);
        updateEntity(entity);
        return modelMapper.map(entity,CandidateOut.class);
    }

    @Override
    public Pair<ReportCondition, ReportOption> filterChain(FilterBase filterBase) {
        ReportOption reportOption = new ReportOption();

        if (filterBase instanceof CandidatePageableFilter) {
            CandidatePageableFilter pageableFilter = (CandidatePageableFilter) filterBase;
            reportOption.setSortOptions(pageableFilter.getSort());
            reportOption.setPageSize(pageableFilter.getSize());
            reportOption.setPageNumber(pageableFilter.getPage());
        }

        CandidateFilter filter = (CandidateFilter) filterBase;
        ReportCondition condition = new ReportCondition();
        condition.addEqualCondition("studentId", filter.getStudentId());
        condition.addEqualCondition("selectionId", filter.getSelectionId());
        return new Pair<>(condition, reportOption);
    }

}
