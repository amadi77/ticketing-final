package com.rajaee.ticketing.core.admin.controller;


import com.rajaee.ticketing.core.admin.model.student.StudentFilter;
import com.rajaee.ticketing.core.admin.model.student.StudentIn;
import com.rajaee.ticketing.core.admin.model.student.StudentOut;
import com.rajaee.ticketing.core.admin.model.student.StudentPageableFilter;
import com.rajaee.ticketing.core.admin.repository.service.AdminStudentService;
import com.rajaee.ticketing.core.admin.statics.CoreRestApi;
import com.rajaee.ticketing.databse.core.entity.StudentEntity;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "Student controller")
@Controller
@RequestMapping(path = RestApi.REST_IDENTIFIED)
@Valid
public class AdminStudentController {

    private AdminStudentService studentService;

    @Autowired
    public AdminStudentController(AdminStudentService studentService) {
        this.studentService = studentService;
    }

    @ApiOperation(value = "get all student entities", response = StudentEntity.class, responseContainer = "List")
    @GetMapping(path = CoreRestApi.STUDENT_ENTITY, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllEntities(@Valid @RequestParam StudentPageableFilter studentPageableFilter, @RequestParam(required = false) String[] include) {
        return new ResponseEntity<>(studentService.getAllEntities(studentPageableFilter, include), HttpStatus.OK);
    }

    @ApiOperation(value = "get student entity by id", response = StudentEntity.class)
    @GetMapping(path = CoreRestApi.STUDENT_ENTITY_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getEntityById(@PathVariable Integer id, @RequestParam(required = false) String[] include) throws SystemException {
        return new ResponseEntity<>(studentService.getEntityById(id, include), HttpStatus.OK);
    }

    @ApiOperation(value = "get filtered student count", response = Integer.class)
    @GetMapping(path = CoreRestApi.STUDENT_COUNT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> count(@Valid @RequestParam StudentFilter studentFilter) {
        return new ResponseEntity<>(studentService.count(studentFilter), HttpStatus.OK);
    }

    @ApiOperation(value = "delete student", response = Boolean.class)
    @DeleteMapping(path = CoreRestApi.STUDENT_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@PathVariable Integer id) {
        return new ResponseEntity<>(studentService.delete(id), HttpStatus.OK);
    }

    @ApiOperation(value = "get all students", response = StudentOut.class, responseContainer = "List")
    @GetMapping(path = CoreRestApi.STUDENT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAll(@Valid @RequestParam StudentPageableFilter studentPageableFilter, @RequestParam(required = false) String[] include) {
        return new ResponseEntity<>(studentService.getAll(studentPageableFilter, include), HttpStatus.OK);
    }

    @ApiOperation(value = "get student by id", response = StudentOut.class)
    @GetMapping(path = CoreRestApi.STUDENT_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getById(@PathVariable Integer id, @RequestParam(required = false) String[] include) throws SystemException {
        return new ResponseEntity<>(studentService.getById(id, include), HttpStatus.OK);
    }

    @ApiOperation(value = "create student entity", response = StudentOut.class)
    @PostMapping(path = CoreRestApi.STUDENT_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@RequestBody StudentIn studentIn) throws SystemException {
        return new ResponseEntity<>(studentService.create(studentIn), HttpStatus.OK);
    }

    @ApiOperation(value = "update student entity", response = StudentOut.class)
    @PutMapping(path = CoreRestApi.STUDENT_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody StudentIn studentIn) throws SystemException {
        return new ResponseEntity<>(studentService.create(studentIn), HttpStatus.OK);
    }

}
