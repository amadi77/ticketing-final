package com.rajaee.ticketing.core.admin.repository.dao;

import com.rajaee.ticketing.databse.core.entity.SelectionEntity;
import com.rajaee.ticketing.databse.core.entity.StudentEntity;
import com.rajaee.ticketing.utility.repository.dao.base.BaseDaoInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SelectionDao {
    private final BaseDaoInterface baseDaoInterface;

    @Autowired
    public SelectionDao(BaseDaoInterface baseDaoInterface) {
        this.baseDaoInterface = baseDaoInterface;
    }

    public List<SelectionEntity> getByListId(List<Integer> ids) {
        return baseDaoInterface.listByIds(SelectionEntity.class, ids);
    }

    public List<SelectionEntity> getByStudent(StudentEntity entity) {
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("collegeId", entity.getCollegeId());
        objectMap.put("dormId", entity.getDormId());
        objectMap.put("inputYear", entity.getInputYear());
        objectMap.put("fieldId", entity.getFieldId());
        objectMap.put("gender", entity.getGender());
        objectMap.put("grade", entity.getGrade());
        objectMap.put("now", ZonedDateTime.now());
        String query = "select selection from Selection selection " +
                "where collegeIds like %:collegeId% " +
                "or selection.dormIds like %:dormId% " +
                "or selection.inputYears like %:inputYear%" +
                "or selection.fieldIds like %:inputYear%" +
                "or selection.grades like %:grade:%" +
                "or selection.gender like %:gender%" +
                "and selection.fromDate <= :now";
        return baseDaoInterface.queryHql(query, objectMap);
    }
}
