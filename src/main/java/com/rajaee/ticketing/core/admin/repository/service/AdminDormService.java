package com.rajaee.ticketing.core.admin.repository.service;

import com.rajaee.ticketing.core.admin.model.dorm.DormFilter;
import com.rajaee.ticketing.core.admin.model.dorm.DormIn;
import com.rajaee.ticketing.core.admin.model.dorm.DormOut;
import com.rajaee.ticketing.core.admin.model.dorm.DormPageableFilter;
import com.rajaee.ticketing.databse.core.entity.DormEntity;
import com.rajaee.ticketing.utility.model.dto.FilterBase;
import com.rajaee.ticketing.utility.model.object.Pair;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.model.object.report.ReportCondition;
import com.rajaee.ticketing.utility.model.object.report.ReportOption;
import com.rajaee.ticketing.utility.repository.dao.base.GenericDao;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdminDormService extends GenericDao {
    private final ModelMapper modelMapper;

    @Autowired
    public AdminDormService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public List<DormEntity> getAllEntities(DormPageableFilter pageableFilter, String[] include) {
        return getAllEntities(DormEntity.class, pageableFilter, include);
    }

    public DormEntity getEntityById(int id, String[] include) throws SystemException {
        return getEntityById(DormEntity.class, id, include);
    }

    public int count(DormFilter dormFilter) {
        return countEntity(DormEntity.class, dormFilter);
    }

    public boolean delete(int id) {
        return deleteById(DormEntity.class, id);
    }

    public List<DormOut> getAll(DormPageableFilter filter, String[] include) {
        return getAllEntities(filter, include).stream().map(item-> modelMapper.map(item,DormOut.class)).collect(Collectors.toList());
    }

    public DormOut getById(int id, String[] include) throws SystemException {
        return modelMapper.map(getEntityById(id, include),DormOut.class);
    }

    public DormOut create(DormIn dormIn) {
        DormEntity entity = modelMapper.map(dormIn, DormEntity.class);
        createEntity(entity);
        return modelMapper.map(entity,DormOut.class);
    }

    public DormOut update(Integer id, DormIn dormIn) {
        DormEntity entity = modelMapper.map(dormIn, DormEntity.class);
        entity.setId(id);
        updateEntity(entity);
        return modelMapper.map(entity,DormOut.class);
    }


    @Override
    public Pair<ReportCondition, ReportOption> filterChain(FilterBase filterBase) {
        ReportOption reportOption = new ReportOption();

        if (filterBase instanceof DormPageableFilter) {
            DormPageableFilter pageableFilter = (DormPageableFilter) filterBase;
            reportOption.setSortOptions(pageableFilter.getSort());
            reportOption.setPageSize(pageableFilter.getSize());
            reportOption.setPageNumber(pageableFilter.getPage());
        }

        DormFilter filter = (DormFilter) filterBase;
        ReportCondition condition = new ReportCondition();
        condition.addLikeCondition("name", filter.getName());
        condition.addEqualCondition("id", filter.getId());
        condition.addEqualCondition("gender", filter.getGender());

        return new Pair<>(condition, reportOption);
    }

}
