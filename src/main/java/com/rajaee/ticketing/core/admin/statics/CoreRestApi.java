package com.rajaee.ticketing.core.admin.statics;

public abstract class CoreRestApi {
    //    candidate rest url
    public static final String CANDIDATE = "/candidate";
    public static final String CANDIDATE_COUNT = "/candidate/{selectionId}/count";
    public static final String CANDIDATE_ID = "/candidate/{id}";

    //    selection rest url
    public static final String SELECTION = "/selection";
    public static final String SELECTION_COUNT = "/selection/count";
    public static final String SELECTION_ID = "/selection/{id}";
    public static final String SELECTION_VALID = "/selection/valid";

    //    dorm rest url
    public static final String DORM = "/dorm";
    public static final String DORM_COUNT = "/dorm/count";
    public static final String DORM_ID = "/dorm/{id}";

    //    field rest url
    public static final String FIELD = "/field";
    public static final String FIELD_COUNT = "/field/count";
    public static final String FIELD_ID = "/field/{id}";

    //    vote rest url
    public static final String VOTE = "/vote";
    public static final String VOTE_COUNT = "/vote/count";
    public static final String VOTE_FILTERCHAIN = "/vote/filterChain";

    //    student rest url
    public static final String STUDENT = "/student";
    public static final String STUDENT_ENTITY = "/student/entity";
    public static final String STUDENT_ID = "/student/{id}";
    public static final String STUDENT_ENTITY_ID = "/student/entity/{id}";
    public static final String STUDENT_COUNT = "/student/count";

    //    college rest url
    public static final String COLLEGE = "/college";
    public static final String COLLEGE_ID = "/college/{id}";
    public static final String COLLEGE_COUNT = "/college/count";

}
