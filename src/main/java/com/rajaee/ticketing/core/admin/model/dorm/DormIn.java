package com.rajaee.ticketing.core.admin.model.dorm;

import com.rajaee.ticketing.databse.core.statics.Gender;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class DormIn {
    @NotNull
    private String name;
    @NotNull
    private Gender gender;
}
