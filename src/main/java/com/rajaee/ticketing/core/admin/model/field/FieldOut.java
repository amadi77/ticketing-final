package com.rajaee.ticketing.core.admin.model.field;

import com.rajaee.ticketing.core.admin.model.college.CollegeOut;
import com.rajaee.ticketing.databse.core.entity.FieldEntity;
import com.rajaee.ticketing.utility.config.StaticApplicationContext;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.modelmapper.ModelMapper;

@Setter
@Getter
public class FieldOut extends FieldIn {
    private int id;
    @Setter(AccessLevel.PRIVATE)
    private CollegeOut college;

    public FieldOut(FieldEntity entity) {
        if (entity != null) {
            ModelMapper modelMapper = StaticApplicationContext.getContext().getBean(ModelMapper.class);
            modelMapper.map(entity, this);
            if (Hibernate.isInitialized(entity.getCollege())) {
                college = new CollegeOut(entity.getCollege());
            }
        }
    }
}
