package com.rajaee.ticketing.core.admin.model.college;

import com.rajaee.ticketing.core.admin.model.field.FieldOut;
import com.rajaee.ticketing.databse.core.entity.CollegeEntity;
import com.rajaee.ticketing.utility.config.StaticApplicationContext;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
public class CollegeOut {
    private int id;
    @Setter(AccessLevel.PRIVATE)
    private List<FieldOut> fields;

    public CollegeOut(CollegeEntity entity) {
        if (entity != null) {
            ModelMapper modelMapper = StaticApplicationContext.getContext().getBean(ModelMapper.class);
            modelMapper.map(entity, this);
            if (Hibernate.isInitialized(entity.getFields())) {
                fields = entity.getFields().stream().map(FieldOut::new).collect(Collectors.toList());
            }
        }
    }
}
