package com.rajaee.ticketing.core.admin.model.user;

import com.rajaee.ticketing.databse.security.entity.UserEntity;
import com.rajaee.ticketing.utility.config.StaticApplicationContext;
import lombok.Getter;
import lombok.Setter;
import org.modelmapper.ModelMapper;

@Setter
@Getter
public class UserInfo {
    private String username;
    private String firstName;
    private String lastName;
    private String fullName;
    private String nationalCode;

    public UserInfo(UserEntity entity){
        if (entity!=null){
            ModelMapper modelMapper = StaticApplicationContext.getContext().getBean(ModelMapper.class);
            modelMapper.map(entity,this);
        }
    }
}
