package com.rajaee.ticketing.core.admin.model.candidate;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CandidateEditIn {
    private String description;
}
