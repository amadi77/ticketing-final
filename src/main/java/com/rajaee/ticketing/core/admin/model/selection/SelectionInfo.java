package com.rajaee.ticketing.core.admin.model.selection;

import com.rajaee.ticketing.databse.core.entity.SelectionEntity;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SelectionInfo {
    private Integer id;
    private String name;

    public SelectionInfo(SelectionEntity entity) {
        if (entity != null) {
            this.id = entity.getId();
            this.name = entity.getName();
        }
    }
}
