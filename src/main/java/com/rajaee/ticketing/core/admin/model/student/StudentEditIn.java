package com.rajaee.ticketing.core.admin.model.student;

import com.rajaee.ticketing.databse.core.statics.Grade;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class StudentEditIn {
    private Integer inputYear;
    private boolean active;
    private Grade grade;
    private Integer dormId;
    private Integer fieldId;
}
