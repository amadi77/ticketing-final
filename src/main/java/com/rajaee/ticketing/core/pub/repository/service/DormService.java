package com.rajaee.ticketing.core.pub.repository.service;

import com.rajaee.ticketing.core.pub.model.dorm.DormFilter;
import com.rajaee.ticketing.core.pub.model.dorm.DormOut;
import com.rajaee.ticketing.core.pub.model.dorm.DormPageableFilter;
import com.rajaee.ticketing.databse.core.entity.DormEntity;
import com.rajaee.ticketing.utility.model.dto.FilterBase;
import com.rajaee.ticketing.utility.model.object.Pair;
import com.rajaee.ticketing.utility.model.object.report.ReportCondition;
import com.rajaee.ticketing.utility.model.object.report.ReportOption;
import com.rajaee.ticketing.utility.repository.dao.base.GenericDao;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DormService extends GenericDao {
    private final ModelMapper modelMapper;

    @Autowired
    public DormService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public List<DormEntity> getAllEntities(DormPageableFilter pageableFilter, String[] include) {
        return getAllEntities(DormEntity.class, pageableFilter, include);
    }


    public List<DormOut> getAll(DormPageableFilter pageableFilter, String[] include) {
        return getAllEntities(pageableFilter, include).stream().map(item -> modelMapper.map(item, DormOut.class)).collect(Collectors.toList());
    }

    @Override
    public Pair<ReportCondition, ReportOption> filterChain(FilterBase filterBase) {
        ReportOption reportOption = new ReportOption();
        if (filterBase instanceof DormPageableFilter) {
            DormPageableFilter pageableFilter = (DormPageableFilter) filterBase;
            reportOption.setPageNumber(pageableFilter.getPage());
            reportOption.setPageSize(pageableFilter.getSize());
            reportOption.setSortOptions(pageableFilter.getSort());
        }
        DormFilter dormFilter = (DormFilter) filterBase;
        ReportCondition condition = new ReportCondition();

        return new Pair<>(condition, reportOption);
    }
}
