package com.rajaee.ticketing.core.pub.model.student;

import com.rajaee.ticketing.databse.core.statics.Gender;
import com.rajaee.ticketing.databse.core.statics.Grade;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class StudentIn {
    private String studentId;
    private Gender gender;
    private String inputYear;
    private Integer collegeId;
    private Boolean active;
    private Grade grade;
    private Integer fieldId;
    private Integer dormId;
}
