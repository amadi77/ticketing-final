package com.rajaee.ticketing.core.pub.model.dorm;

import com.rajaee.ticketing.utility.model.object.SortOption;
import com.rajaee.ticketing.utility.statics.enums.SortType;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class DormPageableFilter extends DormFilter {
    private int size;
    private int page;
    private List<SortOption> sort;

    public DormPageableFilter() {
        size = 10;
        page = 1;
        sort = new ArrayList<>();
        this.sort.add(new SortOption("id", SortType.DESCENDING));
    }
}
