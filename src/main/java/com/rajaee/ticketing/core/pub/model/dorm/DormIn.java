package com.rajaee.ticketing.core.pub.model.dorm;

import com.rajaee.ticketing.databse.core.statics.Gender;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DormIn {
    private Gender gender;
    private String name;
}
