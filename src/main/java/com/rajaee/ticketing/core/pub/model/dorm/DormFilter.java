package com.rajaee.ticketing.core.pub.model.dorm;

import com.rajaee.ticketing.utility.model.dto.FilterBase;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DormFilter extends FilterBase {
}
