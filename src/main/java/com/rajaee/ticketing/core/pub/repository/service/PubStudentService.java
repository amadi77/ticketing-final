package com.rajaee.ticketing.core.pub.repository.service;

import com.rajaee.ticketing.core.pub.model.student.StudentFilter;
import com.rajaee.ticketing.core.pub.model.student.StudentIn;
import com.rajaee.ticketing.core.pub.model.student.StudentOut;
import com.rajaee.ticketing.core.pub.model.student.StudentPageableFilter;
import com.rajaee.ticketing.databse.core.entity.StudentEntity;
import com.rajaee.ticketing.utility.model.dto.FilterBase;
import com.rajaee.ticketing.utility.model.object.Pair;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.model.object.report.ReportCondition;
import com.rajaee.ticketing.utility.model.object.report.ReportOption;
import com.rajaee.ticketing.utility.repository.dao.base.GenericDao;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PubStudentService extends GenericDao {
    private final ModelMapper modelMapper;

    @Autowired
    public PubStudentService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public StudentEntity getEntityById(int id, String[] include) throws SystemException {
        return getEntityById(StudentEntity.class, id, include);
    }

    public int countEntity(StudentFilter filter) {
        return countEntity(StudentEntity.class, filter);
    }

    public List<StudentEntity> getAllEntities(StudentPageableFilter filterBase, String[] include) {
        return super.getAllEntities(StudentEntity.class, filterBase, include);
    }

    public List<StudentOut> getAll(StudentPageableFilter filter, String[] include) {
        return getAllEntities(filter, include)
                .stream()
                .map(item -> modelMapper.map(item, StudentOut.class))
                .collect(Collectors.toList());
    }

    public boolean studentExist(StudentIn student) {
        StudentFilter filter = modelMapper.map(student, StudentFilter.class);
        return countEntity(filter) == 0;

    }

    public StudentOut create(StudentIn student,Integer userId){
        StudentEntity entity = modelMapper.map(student,StudentEntity.class);
        entity.setId(userId);
        createEntity(entity);
        return modelMapper.map(entity,StudentOut.class);
    }
    @Override
    public Pair<ReportCondition, ReportOption> filterChain(FilterBase filterBase) {
        ReportOption reportOption = new ReportOption();

        if (filterBase instanceof StudentPageableFilter) {
            StudentPageableFilter pageableFilter = (StudentPageableFilter) filterBase;
            reportOption.setPageNumber(pageableFilter.getPage());
            reportOption.setSortOptions(pageableFilter.getSorts());
            reportOption.setPageSize(pageableFilter.getSize());
        }

        StudentFilter filter = (StudentFilter) filterBase;
        ReportCondition condition = new ReportCondition();
        condition.addEqualCondition("id", filter.getId());
        condition.addEqualCondition("collegeId", filter.getCollegeId());
        condition.addEqualCondition("active", filter.getActive());
        condition.addEqualCondition("gender", filter.getGender());
        condition.addMinNumberCondition("inputYear", filter.getInputYearFrom());
        condition.addMaxNumberCondition("inputYear", filter.getInputYearTo());
        condition.addEqualCondition("grade", filter.getGrade());
        condition.addEqualCondition("dormId", filter.getDormId());
        condition.addEqualCondition("fieldId", filter.getFieldId());
        condition.addEqualCondition("studentId", filter.getStudentId());
        return new Pair<>(condition, reportOption);
    }
}
