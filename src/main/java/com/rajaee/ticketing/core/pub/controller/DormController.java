package com.rajaee.ticketing.core.pub.controller;

import com.rajaee.ticketing.core.pub.model.dorm.DormOut;
import com.rajaee.ticketing.core.pub.model.dorm.DormPageableFilter;
import com.rajaee.ticketing.core.pub.repository.service.DormService;
import com.rajaee.ticketing.core.pub.statics.RestTicket;
import com.rajaee.ticketing.utility.model.object.SystemException;
import com.rajaee.ticketing.utility.statics.constants.RestApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Api(tags = "Dorm Controller")
@Controller
@RequestMapping(RestApi.REST_PUBLIC)
@Validated
public class DormController {
    private final DormService service;

    @Autowired
    public DormController(DormService service) {
        this.service = service;
    }

    @ApiOperation(value = "get all dorm match filter", response = DormOut.class, responseContainer = "List")
    @GetMapping(path = RestTicket.DORM, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAll(@Valid DormPageableFilter pageableFilter, @RequestParam(value = "include", required = false) String[] include, HttpServletRequest servletRequest) throws SystemException {
        return new ResponseEntity<>(service.getAll(pageableFilter, include), HttpStatus.OK);
    }
}

