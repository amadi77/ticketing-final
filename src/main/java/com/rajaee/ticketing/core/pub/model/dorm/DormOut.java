package com.rajaee.ticketing.core.pub.model.dorm;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DormOut extends DormIn{
    private Integer id;
}
