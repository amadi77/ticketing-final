package com.rajaee.ticketing.core.pub.model.student;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class StudentOut extends StudentIn {
    private Integer id;
}
