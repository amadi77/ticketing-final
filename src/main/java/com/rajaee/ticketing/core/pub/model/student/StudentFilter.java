package com.rajaee.ticketing.core.pub.model.student;

import com.rajaee.ticketing.databse.core.statics.Gender;
import com.rajaee.ticketing.databse.core.statics.Grade;
import com.rajaee.ticketing.utility.model.dto.FilterBase;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class StudentFilter extends FilterBase {
    private Integer id;
    private String studentId;
    private Gender gender;
    private Integer inputYearFrom;
    private Integer inputYearTo;
    private Integer collegeId;
    private Boolean active;
    private Grade grade;
    private Integer fieldId;
    private Integer dormId;
}
