package com.rajaee.ticketing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
//@EntityScan(basePackages = {"com.rajaee.*"})
//@ComponentScan(basePackages = {"com.rajaee.*"})
//@ConfigurationPropertiesScan("com.rajaee")
@SpringBootApplication
public class TicketingApplication {

	public static void main(String[] args) {
		SpringApplication.run(TicketingApplication.class, args);
	}

}
